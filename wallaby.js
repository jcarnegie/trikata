module.exports = function wallabyConfig(wallaby) {
  return {
    files: [
      'src/**',
      'test/unit/jest.setup.js',
      'test/unit/fixtures/**',
      'features.json',
    ],
    tests: [
      'test/unit/**/*.test.ts',
    ],
    env: {
      type: 'node',
      runner: 'node',
      params: {
        env: 'NODE_ENV=test',
      },
    },
    compilers: {
      '**/*.ts': wallaby.compilers.babel(),
    },
    testFramework: 'jest',
  };
};
