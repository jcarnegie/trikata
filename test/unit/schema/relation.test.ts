// @flow
import { tableize } from 'inflection';
import { Type } from '../../../src/schema/type';
import { createRelationType } from '../../../src/schema/relation';

const minimalType = (name: string): Type => ({
  name,
  tableName: tableize(name),
  fields: [],
  indexes: [],
  directives: [],
});

describe('Relation', () => {
  describe('createRelationType', () => {
    
    const t1: Type = minimalType('User');
    const t2: Type = minimalType('Post');
    const relation = {
      name: '_PostToUser',
      tableName: tableize('_PostToUser'),
      types: [t2, t1],
    };
    const parentType = {
      name: relation.name,
      tableName: relation.tableName,
      indexes: [],
      directives: [],
      fields: [],
    };

    it('should create a type for a relation', () => {
      const relationType: Type = createRelationType(relation);

      expect(relationType).toEqual({
        name: '_PostToUser',
        tableName: '_post_to_users',
        fields: [{
          parentType,
          name: 'id',
          columnName: 'id',
          type: 'ID',
          isList: false,
          listItemsNullable: false,
          sqlType: 'varchar',
          size: 36,
          primaryKey: true,
          nullable: false,
          unique: false,
          default: null,
          isRelation: false,
          directives: [],
        }, {
          parentType,
          name: 'postId',
          columnName: 'post_id',
          type: 'String',
          isList: false,
          listItemsNullable: false,
          sqlType: 'varchar',
          size: 36,
          primaryKey: false,
          nullable: false,
          unique: false,
          default: null,
          isRelation: false,
          directives: [],
        }, {
          parentType,
          name: 'userId',
          columnName: 'user_id',
          type: 'String',
          isList: false,
          listItemsNullable: false,
          sqlType: 'varchar',
          size: 36,
          primaryKey: false,
          nullable: false,
          unique: false,
          default: null,
          isRelation: false,
          directives: [],
        }],
        indexes: [{
          parentType,
          name: '_post_to_users_post_id',
          columns: ['post_id'],
          primary: false,
          type: 'btree',
          unique: false,
        }, {
          parentType,
          name: '_post_to_users_user_id',
          columns: ['user_id'],
          primary: false,
          type: 'btree',
          unique: false,
        }],
        directives: [],
      });
    });
  });
});
