import { keys } from 'ramda';
import { promise } from 'fluture';
import { loadCustomResolvers } from '../../../src/schema/resolvers';

describe('resolvers', () => {
  it('should load custom resolvers', async () => {
    const dirs = ['./test/unit/fixtures/resolvers'];
    const resolvers = await promise(loadCustomResolvers(dirs));
    expect(typeof resolvers.Mutation.createUser).toEqual('function');
    expect(typeof resolvers.Query.findUser).toEqual('function');
    expect(keys(resolvers.Mutation)).toHaveLength(1);
    expect(keys(resolvers.Query)).toHaveLength(1);
  });
});