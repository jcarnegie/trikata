// @flow
import { underscore } from 'inflection';
import {
  fromWhereInput,
  formatSql,
  formatSqlWithBindParams,
} from '../../../../src/db/where';
import { Type } from '../../../../src/schema/type';
import { Field } from '../../../../src/schema/field';

describe('From Where Input', () => {

  const type: Type = {
    name: 'User',
    tableName: 'users',
    fields: [],
    indexes: [],
    directives: [],
  };

  const field: Field = {
    parentType: type,
    name: 'firstName',
    columnName: underscore('firstName'),
    type: 'String',
    isList: false,
    listItemsNullable: false,
    sqlType: 'varchar',
    size: 36,
    nullable: false,
    primaryKey: false,
    default: null,
    unique: false,
    isRelation: false,
    arguments: [],
    directives: [],
  };

  type.fields = [field];

  it('should handle simple filter case', () => {
    const whereInput = { email: 'a@b.c' };
    expect(fromWhereInput(type, whereInput)).toEqual([{
      column: 'email',
      operator: 'eq',
      value: 'a@b.c',
    }]);
  });

  it('should handle simple filter case #2', () => {
    const whereInput = { email_in: ['a@b.c'] };
    expect(fromWhereInput(type, whereInput)).toEqual([{
      column: 'email',
      operator: 'in',
      value: ['a@b.c'],
    }]);
  });

  it('should handle simple filter case with camelcase GQL field name', () => {
    const whereInput = { firstName_in: ['Jeff', 'John'] };
    expect(fromWhereInput(type, whereInput)).toEqual([{
      column: 'first_name',
      operator: 'in',
      value: ['Jeff', 'John'],
    }]);
  });

  it('should handle simple AND case', () => {
    const whereInput = {
      AND: [
        { email: 'a@b.c' },
        { email_in: ['a@b.c'] },
      ],
    };
    const whereAst = fromWhereInput(type, whereInput);
    expect(whereAst).toEqual([
      {
        operator: 'AND',
        filters: [
          { column: 'email', operator: 'eq', value: 'a@b.c' },
          { column: 'email', operator: 'in', value: ['a@b.c'] },
        ],
      },
    ]);
  });

  it('should handle complex where input', () => {
    const whereInput = {
      OR: [{
        AND: [{
          title_in: ['My biggest Adventure', 'My latest Hobbies'],
        }, {
          published: true,
        }],
      }, {
        id: 'cixnen24p33lo0143bexvr52n',
      }],
    };
    const whereAst = fromWhereInput(type, whereInput);
    expect(whereAst).toEqual([{
      operator: 'OR',
      filters: [{
        operator: 'AND',
        filters: [{
          column: 'title',
          operator: 'in',
          value: ['My biggest Adventure', 'My latest Hobbies'],
        }, {
          column: 'published',
          operator: 'eq',
          value: true,
        }],
      }, {
        column: 'id',
        operator: 'eq',
        value: 'cixnen24p33lo0143bexvr52n',
      }],
    }]);
  });
});

describe('formatSql', () => {
  it('should convert WhereAST to sql string', () => {
    const whereAst = [{
      operator: 'OR',
      filters: [{
        operator: 'AND',
        filters: [{
          column: 'title',
          operator: 'in',
          value: ['My biggest Adventure', 'My latest Hobbies'],
        }, {
          column: 'published',
          operator: 'eq',
          value: true,
        }],
      }, {
        column: 'id',
        operator: 'eq',
        value: 'cixnen24p33lo0143bexvr52n',
      }],
    }];

    const type = {
      fields: [
        { name: 'title', type: 'String' },
        { name: 'published', type: 'Boolean' },
        { name: 'id', type: 'String' },
      ],
    };

    expect(formatSql(type, whereAst))
      .toEqual('(((title in (\'My biggest Adventure\', \'My latest Hobbies\')) and (published = true)) or (id = \'cixnen24p33lo0143bexvr52n\'))');
  });
});

describe('formatSqlWithBindParams', () => {
  it('should handle the simple case', () => {
    const whereAST = [{
      column: 'id',
      operator: 'eq',
      value: 'cixnen24p33lo0143bexvr52n',
    }];
    const [sql, values] = formatSqlWithBindParams(whereAST);
    expect(sql).toEqual('(id = ?)');
    expect(values).toEqual(['cixnen24p33lo0143bexvr52n']);
  });

  it('should handle the multiple filters', () => {
    const whereAST = [{
      column: 'id',
      operator: 'eq',
      value: 'cixnen24p33lo0143bexvr52n',
    }, {
      column: 'title',
      operator: 'in',
      value: ['My biggest Adventure', 'My latest Hobbies'],
    }];
    const [sql, values] = formatSqlWithBindParams(whereAST);
    expect(sql).toEqual('(id = ?) and (title in (?, ?))');
    expect(values).toEqual(['cixnen24p33lo0143bexvr52n', 'My biggest Adventure', 'My latest Hobbies']);
  });

  it('should handle complex case', () => {
    const whereAST = [{
      operator: 'OR',
      filters: [{
        operator: 'AND',
        filters: [{
          column: 'title',
          operator: 'in',
          value: ['My biggest Adventure', 'My latest Hobbies'],
        }, {
          column: 'published',
          operator: 'eq',
          value: true,
        }],
      }, {
        column: 'id',
        operator: 'eq',
        value: 'cixnen24p33lo0143bexvr52n',
      }],
    }];

    const [sql, values] = formatSqlWithBindParams(whereAST);
    expect(sql).toEqual('(((title in (?, ?)) and (published = ?)) or (id = ?))');
    expect(values).toEqual(['My biggest Adventure', 'My latest Hobbies', true, 'cixnen24p33lo0143bexvr52n']);
  });
});
