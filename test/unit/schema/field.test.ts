import { readdir } from 'fs';
import { FutureInstance, map, promise } from 'fluture';
import { parseTypedefs } from '../../../src/schema';
import { ParseSchemaContext, createParseSchemaContext } from '../../../src/schema/context';
import { createIdField, hasDirective } from '../../../src/schema/field';

describe('Field', () => {
    let typedefs = `
      type User {
        id: ID!
        email: String!
        password: String! @writeOnly
      }
    `;

    let parseContext = null;
    let schema = null;

    beforeEach(async () => {
        parseContext = await promise(<FutureInstance<Error, ParseSchemaContext>>createParseSchemaContext());
        schema = parseTypedefs(parseContext, typedefs);
    });

    it('should test if field has a directive', () => {
        const field = schema.types[0].fields[2];
        expect(hasDirective('writeOnly', field)).toBe(true);
    });

    it('should detect when a field doesn\'t have a directive', async () => {
        let typedefs = `
            type User {
                id: ID!
                email: String!
                password: String!
            }
        `;

        schema = parseTypedefs(parseContext, typedefs);
        const field = schema.types[0].fields[2];
        expect(hasDirective("writeOnly", field)).toBe(false);
    });
})