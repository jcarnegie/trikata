// @flow
import { identity, sort } from 'ramda';
import { ascendString, lowerFirstLetter } from '../../../src/util/string';

describe('String', () => {
  describe('lowerFirstLetter', () => {
    it('should change first letter of string to lower case', () => {
      expect(lowerFirstLetter('Hello World!')).toEqual('hello World!');
      expect(lowerFirstLetter('HelloWorld!')).toEqual('helloWorld!');
    });

    it('should handle case of first letter already lower case', () => {
      expect(lowerFirstLetter('hello world!')).toEqual('hello world!');
    });

    it('should handle first letter being non-alpha', () => {
      expect(lowerFirstLetter('0xHelloworld!')).toEqual('0xHelloworld!');
    });
  });

  describe('ascendString', () => {
    it('should sort array of strings ascending', () => {
      const a = ['z', 'a', 'm'];
      const byString = ascendString(identity);
      const b = sort(byString, a);
      expect(b).toEqual(['a', 'm', 'z']);
    });
  });
});
