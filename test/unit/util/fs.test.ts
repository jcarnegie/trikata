import { fork, done } from 'fluture';
import { readdir, readFile } from '../../../src/util/fs';


describe('FS', () => {
    it('should read directory entries', (done) => {
        const dir = 'test/unit/fixtures/fs';
        const future = readdir(dir);
        const reject = done;
        const resolve = (files) => {
            expect(files).toEqual(['a.txt', 'b.txt']);
            done();
        };
        fork(reject)(resolve)(future);
    });

    it('should read file contents', (done) => {
        const path = "test/unit/fixtures/fs/a.txt";
        const reject = done;
        const resolve = contents => {
          expect(contents).toEqual("This is a file with some text in it.");
          done();
        };
        fork(reject)(resolve)(readFile(path))
    });
});