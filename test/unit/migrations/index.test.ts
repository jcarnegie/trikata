import { format as f } from 'sql-formatter';
import { FutureInstance, promise } from 'fluture';
import { stringify } from '../../../src/util/string';
import { create as createMigrationPlan } from '../../../src/migrations/plan';
import { generate } from '../../../src/migrations';
import { createParseSchemaContext, ParseSchemaContext } from '../../../src/schema/context';
import { parseTypedefs } from '../../../src/schema';

const EMPTY_SCHEMA = {
  types: [],
  statelessTypes: [],
  queryType: null,
  mutationType: null,
  enums: [],
  relations: []
};

const BASIC_USER_SDL = `
type User {
  id: ID!
  email: String!
}
`;

const USER_WITH_NAME_SDL = `
type User {
  id: ID!
  email: String!
  name: String
}
`;

const USER_WITH_NAME_AND_PHONE_SDL = `
type User {
  id: ID!
  email: String!
  name: String
  phone: String
}
`;

describe('Generate New Migration File', () => {
  let parseSchemaContext = null;
  const dbSchema = EMPTY_SCHEMA;
  const context = <PGDBContext>{ schemaName: 'trikata' };

  beforeEach(async () => {
    const ctxFuture = <FutureInstance<Error, ParseSchemaContext>>createParseSchemaContext();
    parseSchemaContext = await promise(ctxFuture);
  });

  it('should generate a new migration file', () => {
    const schema = parseTypedefs(parseSchemaContext, BASIC_USER_SDL);
    const plan = createMigrationPlan(schema, dbSchema);
    const fsMigrations = [];
    const dbMigrations = [];
    const migrations = generate(context, BASIC_USER_SDL, plan, fsMigrations, dbMigrations);
    const newMigration = migrations[0];
    expect(migrations).toHaveLength(1);
    expect(newMigration.createdAt.getTime()).toBeLessThanOrEqual(new Date().getTime());
    expect(newMigration.typedefs).toEqual(BASIC_USER_SDL);
    expect(newMigration.typedefsHash).toBeDefined();
    expect(newMigration.sql).toContain('create table trikata.users');
    expect(newMigration.sqlHash).toBeDefined();
    expect(newMigration.beforeFiles).toBeDefined();
    expect(newMigration.afterFiles).toBeDefined();
  });

  it('should add a new migration to existing applied migrations', () => {
    const migration = {
      createdAt: new Date(),
      typedefs: '\ntype User {\n  id: ID!\n  email: String!\n}\n',
      typedefsHash: '86b7a6d5ad9456b85a204befdf308cd39f6d18b6',
      sql: '\n    create table trikata.users (\n      id varchar (36) not null  primary key ,\nemail varchar (255) not null   \n    )\n  ',
      sqlHash: '56645222d85ce30f6ff2dead02f40c916ba41f89',
      beforeFiles: [],
      afterFiles: []
    };
    const schema = parseTypedefs(parseSchemaContext, USER_WITH_NAME_SDL);
    const dbSchema = parseTypedefs(parseSchemaContext, BASIC_USER_SDL);
    const plan = createMigrationPlan(schema, dbSchema);
    const fsMigrations = [migration];
    const dbMigrations = [migration];
    const migrations = generate(context, USER_WITH_NAME_SDL, plan, fsMigrations, dbMigrations);
    const newMigration = migrations[1];
    expect(migrations).toHaveLength(2);
    expect(newMigration.typedefs).toEqual(USER_WITH_NAME_SDL);
    expect(newMigration.sql).toContain(f('alter table trikata.users add column name'));
  });

  it('should add a new migration to existing applied migrations (replace unapplied migration)', () => {
    const migration1 = {
      createdAt: new Date(),
      typedefs: '\ntype User {\n  id: ID!\n  email: String!\n}\n',
      typedefsHash: '86b7a6d5ad9456b85a204befdf308cd39f6d18b6',
      sql: '\n    create table trikata.users (\n      id varchar (36) not null  primary key ,\nemail varchar (255) not null   \n    )\n  ',
      sqlHash: '56645222d85ce30f6ff2dead02f40c916ba41f89',
      beforeFiles: [],
      afterFiles: []
    };
    const migration2 = {
      createdAt: new Date(migration1.createdAt.getTime() + 30000),
      typedefs: '\ntype User {\n  id: ID!\n  email: String!\n  name: String\n}\n',
      typedefsHash: '7fac62a55b853c4a919fb7257b840208620bcff8',
      sql: 'alter table trikata.users add column name varchar (255)    ',
      sqlHash: 'b09f045cb49acaab508209ef31d163ce8b6903cb',
      beforeFiles: [],
      afterFiles: [],
    };
    const schema = parseTypedefs(parseSchemaContext, USER_WITH_NAME_AND_PHONE_SDL);
    const dbSchema = parseTypedefs(parseSchemaContext, BASIC_USER_SDL);
    const plan = createMigrationPlan(schema, dbSchema);
    const fsMigrations = [migration1, migration2];
    const dbMigrations = [migration1];
    const migrations = generate(context, USER_WITH_NAME_AND_PHONE_SDL, plan, fsMigrations, dbMigrations);
    const newMigration = migrations[1];
    expect(migrations).toHaveLength(2);
    expect(newMigration.typedefs).toEqual(USER_WITH_NAME_AND_PHONE_SDL);
    expect(newMigration.sql).toContain(f('alter table trikata.users add column name'));
    expect(newMigration.sql).toContain(f('alter table trikata.users add column phone'));
  });
});
