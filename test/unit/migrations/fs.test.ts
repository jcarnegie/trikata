import { FutureInstance, promise } from 'fluture';
import { Migration } from "../../../src/migrations";
import { loadMigrations } from '../../../src/migrations/fs';

describe('File System Migrations', () => {
  it('should load migrations from the file system', async () => {
    const migrations = await promise(<FutureInstance<Error, Migration[]>>loadMigrations('./test/unit/fixtures/migrations'));
    expect(migrations[0].typedefs).toEqual('foo');
    expect(migrations[0].typedefsHash).toEqual('0beec7b5ea3f0fdbc95d0dd47f3c5bc275da8a33');
    expect(migrations[0].sql).toEqual('bar');
    expect(migrations[0].sqlHash).toEqual('62cdb7020ff920e5aa642c3d4066950dd1f01f4d');
    expect(migrations[0].createdAt).toBeDefined();
  });
});