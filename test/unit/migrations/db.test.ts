import { FutureInstance, promise } from 'fluture';
import { Migration } from '../../../src/migrations';
import { loadMigrations } from '../../../src/migrations/db';
import { PGDBContext, connect } from '../../../src/db/pg';

describe('Database Migrations', () => {
  const dbMigrations = {
    rows: [
      {
        created_at: new Date('2019-06-18T18:34:39.424Z'),
        typedefs: 'foo',
        typedefs_hash: '0beec7b5ea3f0fdbc95d0dd47f3c5bc275da8a33',
        sql: 'bar',
        sql_hash: '62cdb7020ff920e5aa642c3d4066950dd1f01f4d'
      }
    ]
  };

  const client = {
    query: jest.fn(() => Promise.resolve(dbMigrations)),
  };

  it('should load migrations from the database', async () => {
    const ctx: PGDBContext = {
      client,
      schemaName: 'trikata',
    };
    const migrations = await promise(<FutureInstance<Error, Migration[]>>loadMigrations(ctx));
    expect(client.query.mock.calls[0][0]).toContain('select * from trikata.__migrations order by created_at');
    expect(migrations[0]).toEqual({
      createdAt: dbMigrations.rows[0].created_at,
      typedefs: dbMigrations.rows[0].typedefs,
      typedefsHash: dbMigrations.rows[0].typedefs_hash,
      sql: dbMigrations.rows[0].sql,
      sqlHash: dbMigrations.rows[0].sql_hash,
      beforeFiles: [],
      afterFiles: [],
    });
  });
});