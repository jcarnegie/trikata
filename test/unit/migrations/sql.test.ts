import { format as f } from 'sql-formatter';
import { FutureInstance, promise } from "fluture";
import { clone } from "ramda";
import { create } from "../../../src/migrations/plan";
import { migrationSql } from "../../../src/migrations/sql";
import { PGDBContext } from '../../../src/db/pg';
import { createParseSchemaContext, ParseSchemaContext } from "../../../src/schema/context";
import { parseTypedefs } from "../../../src/schema";

const EMPTY_SCHEMA = {
  types: [],
  statelessTypes: [],
  queryType: null,
  mutationType: null,
  enums: [],
  relations: []
};

const BASIC_USER_SDL = `
type User {
  id: ID!
  email: String!
}
`;

const RENAMABLE_RELATION_SDL_1 = `
type User {
  id: ID!
  posts: [Post!]! @relation(name: "Test")
}
type Post { id: ID! }
`;

const RENAMABLE_RELATION_SDL_2 = `
type User {
  id: ID!
  posts: [Post!]!
}
type Post { id: ID! }
`;

const DB_MISSING_RELATION_SDL = `
type User {
  id: ID!
}
`;

const GQL_WITH_RELATION_SDL = RENAMABLE_RELATION_SDL_2;

const GQL_ADD_NAME_TO_USER = `
type User {
  id: ID!
  name: String!
  email: String!
}
`;

const GQL_ADD_EMAIL_INDEX_TO_USER = `
type User {
  id: ID!
  email: String! @index
}
`;

const GQL_MAKE_EMAIL_NULLABLE = `
type User {
  id: ID!
  email: String
}
`;

const GQL_SET_EMAIL_DEFAULT = `
type User {
  id: ID!
  email: String! @default(value: "test@acme.com")
}
`;

const GQL_SET_EMAIL_DEFAULT_AND_MAKE_NULLABLE = `
type User {
  id: ID!
  email: String @default(value: "test@acme.com")
}
`;

const GQL_REMOVE_EMAIL = `type User { id: ID! }`;

const USER_AND_POST_SDL = `
type User { id: ID! }
type Post { id: ID! }
`;

const GQL_USER = `type User { id: ID! }`;

describe("Migration SQL", () => {
  const context = <PGDBContext>{ schemaName: "trikata" };
  let parseSchemaContext = null;
  let schema = null;
  let dbSchema = null;

  beforeEach(async () => {
    parseSchemaContext = await promise(<
      FutureInstance<Error, ParseSchemaContext>
    >createParseSchemaContext());
    schema = clone(EMPTY_SCHEMA);
    dbSchema = clone(EMPTY_SCHEMA);
  });

  it("should add a create table statement to the sql", () => {
    const schema = parseTypedefs(parseSchemaContext, BASIC_USER_SDL);
    const plan = create(schema, dbSchema);
    const sql = migrationSql(context, plan);
    expect(sql).toContain('create table');
    expect(sql).toContain(f('id varchar (36) not null primary key'));
    expect(sql).toContain(f('email varchar (255) not null'));
  });

  it("should add an alter table statement to sql for rename relation tables action", () => {
    const dbSchema = parseTypedefs(parseSchemaContext, RENAMABLE_RELATION_SDL_1);
    const schema = parseTypedefs(parseSchemaContext, RENAMABLE_RELATION_SDL_2);
    const plan = create(schema, dbSchema);
    const sql = migrationSql(context, plan);
    expect(sql).toContain(f('alter table trikata._tests rename to _post_to_users'));
    expect(sql).toContain(f('alter table trikata._post_to_users rename constraint "_tests_user_id_fkey" to "_post_to_users_user_id_fkey"'));
  });

  it("should add create table and create index statements for add relation tables action", () => {
    const dbSchema = parseTypedefs(parseSchemaContext, DB_MISSING_RELATION_SDL);
    const schema = parseTypedefs(parseSchemaContext, GQL_WITH_RELATION_SDL);
    const plan = create(schema, dbSchema);
    const sql = migrationSql(context, plan);
    expect(sql).toContain(f('create table trikata.posts'));
    expect(sql).toContain(f('create table trikata._post_to_users'));
    expect(sql).toContain(f('id varchar(36) primary key'));
    expect(sql).toContain(f('post_id varchar(36) not null references trikata.posts(id) on delete cascade'));
    expect(sql).toContain(f('user_id varchar(36) not null references trikata.users(id) on delete cascade'));
    expect(sql).toContain(f('unique (post_id, user_id)'));
    expect(sql).toContain(f('create  index _post_to_users_post_id on trikata._post_to_users using btree (post_id)'));
    expect(sql).toContain(f('create  index _post_to_users_user_id on trikata._post_to_users using btree (user_id)'));
  });

  it("should add alter table statement to sql for add new columns action", () => {
    const dbSchema = parseTypedefs(parseSchemaContext, BASIC_USER_SDL);
    const schema = parseTypedefs(parseSchemaContext, GQL_ADD_NAME_TO_USER);
    const plan = create(schema, dbSchema);
    const sql = migrationSql(context, plan);
    expect(sql).toContain(f('alter table trikata.users add column name varchar (255) not null'));
  });

  it("should add create index statement to sql for add new indexes action", () => {
    const dbSchema = parseTypedefs(parseSchemaContext, BASIC_USER_SDL);
    const schema = parseTypedefs(parseSchemaContext, GQL_ADD_EMAIL_INDEX_TO_USER);
    const plan = create(schema, dbSchema);
    const sql = migrationSql(context, plan);
    expect(sql).toContain(f('create index users_email_key on trikata.users using btree (email)'));
  });

  it("should add alter column statement for update columns (nullability) action", () => {
    const dbSchema = parseTypedefs(parseSchemaContext, BASIC_USER_SDL);
    const schema = parseTypedefs(parseSchemaContext, GQL_MAKE_EMAIL_NULLABLE);
    const plan = create(schema, dbSchema);
    const sql = migrationSql(context, plan);
    expect(sql).toContain(f('alter table trikata.users alter email drop not null'));
  });

  it("should add alter column statement for update columns (default) action", () => {
    const dbSchema = parseTypedefs(parseSchemaContext, BASIC_USER_SDL);
    const schema = parseTypedefs(parseSchemaContext, GQL_SET_EMAIL_DEFAULT);
    const plan = create(schema, dbSchema);
    const sql = migrationSql(context, plan);
    expect(sql).toContain(f('alter table trikata.users alter column email set default \'test@acme.com\''));
  });

  it("should add alter column statement for update columns (default + nullability) action", () => {
    const dbSchema = parseTypedefs(parseSchemaContext, BASIC_USER_SDL);
    const schema = parseTypedefs(parseSchemaContext, GQL_SET_EMAIL_DEFAULT_AND_MAKE_NULLABLE);
    const plan = create(schema, dbSchema);
    const sql = migrationSql(context, plan);
    expect(sql).toContain(f('alter table trikata.users alter email drop not null'));
    expect(sql).toContain(f('alter table trikata.users alter column email set default \'test@acme.com\''));
  });

  it("should add a remove columns action to the plan", () => {
    const dbSchema = parseTypedefs(parseSchemaContext, BASIC_USER_SDL);
    const schema = parseTypedefs(parseSchemaContext, GQL_REMOVE_EMAIL);
    const plan = create(schema, dbSchema);
    const sql = migrationSql(context, plan);
    expect(sql).toContain(f('alter table trikata.users drop column email'));
  });

  it("should add a remove tables action to the plan", () => {
    const dbSchema = parseTypedefs(parseSchemaContext, USER_AND_POST_SDL);
    const schema = parseTypedefs(parseSchemaContext, GQL_USER);
    const plan = create(schema, dbSchema);
    const sql = migrationSql(context, plan);
    expect(sql).toContain(f('drop table trikata.posts cascade'));
  });

  it("should add a remove indexes action to the plan", () => {
    const dbSchema = parseTypedefs(parseSchemaContext, GQL_ADD_EMAIL_INDEX_TO_USER);
    const schema = parseTypedefs(parseSchemaContext, BASIC_USER_SDL);
    const plan = create(schema, dbSchema);
    const sql = migrationSql(context, plan);
    expect(sql).toContain(f('drop index trikata.users_email_key cascade'));
  });
});
