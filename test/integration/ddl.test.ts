// @flow
/* eslint-disable prefer-arrow-callback */
import '@babel/polyfill';
import { Type } from '../../src/schema/type';
import { relationsFromDBMetadata } from '../../src/db/pg/ddl';

beforeEach(async () => {
  await global.pg.query('drop schema if exists trikata cascade');
});

describe('DDL', () => {
  const types: Type[] = [{
    name: 'User',
  }, {
    name: 'Post',
  }];

  it('should return information about join tables', async () => {
    await global.pg.query('create schema trikata');

    await global.pg.query(`
      create table trikata.users (
        id varchar (36) primary key,
        email varchar (255) not null unique
      );
    `);
    await global.pg.query(`
      create table trikata.posts(
        id varchar (36) primary key,
        title varchar (255),
        contents varchar (255)
      )
    `);
    await global.pg.query(`
      create table trikata._post_to_users(
        id varchar (36) primary key,
        user_id varchar (36),
        post_id varchar (36),
        foreign key (user_id) references trikata.users (id),
        foreign key (post_id) references trikata.posts (id)
      )
    `);
    const context = {
      schemaName: 'trikata',
      client: global.pg,
    };
    const relations = await relationsFromDBMetadata(context, types);
    expect(relations).toEqual([{
      name: 'PostToUser',
      tableName: '_post_to_users',
      types: [{ name: 'Post' }, { name: 'User' }],
    }]);
  });
});
