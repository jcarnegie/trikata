/* eslint-disable no-console,no-underscore-dangle */
console.log('db-jest-environment.js');
const NodeEnvironment = require('jest-environment-node');
const Promise = require('bluebird');
const { PG_URL } = require('./db');
const { Client } = require('pg');

const sleep = timeout =>
  new Promise(resolve => setTimeout(resolve, timeout));

const connect = async (
  connectionString = PG_URL,
  attempts = 30,
  interval = 1000,
) => {
  if (attempts === 0) throw new Error('Failed to connect');
  try {
    const client = new Client({ connectionString });
    await client.connect();
    return client;
  } catch (e) {
    await sleep(interval);
    return connect(connectionString, attempts - 1, interval);
  }
};

class DBEnvironment extends NodeEnvironment {
  // eslint-disable-next-line no-useless-constructor
  constructor(config) {
    super(config);
  }

  async setup() {
    this.global.pg = await connect(PG_URL);
    await super.setup();
  }

  async teardown() {
    try {
      await this.global.pg.end();
    } catch (e) {
      console.log('teardown error:', e.message);
    }

    await super.teardown();
  }

  runScript(script) {
    return super.runScript(script);
  }
}

module.exports = DBEnvironment;
