const shell = require('shelljs');
const Docker = require('dockerode');
const { promisify } = require('bluebird');
const {
  contains,
  curry,
  find,
  isNil,
  propSatisfies,
} = require('ramda');

const CONTAINER_NAME = 'integration-testing-pg-db';
const PG_USER = 'postgres';
const PG_PASSWORD = 'pgpassword';
const PG_HOST = '127.0.0.1';
const PG_PORT = 6543;
const PG_DB = 'postgres';
const PG_URL = process.env.CI
  ? 'postgresql://postgres@127.0.0.1:5432/postgres'
  : `postgresql://${PG_USER}:${PG_PASSWORD}@${PG_HOST}:${PG_PORT}/${PG_DB}`;

console.log(`Running jest integration tests against ${PG_URL}`);

const silent = { silent: true };

const propContains = curry((property, value, object) =>
  propSatisfies(p => contains(value, p), property, object));

const docker = new Docker();
const listContainers = promisify(docker.listContainers.bind(docker));

const containerExists = containers =>
  !isNil(find(propContains('Names', `/${CONTAINER_NAME}`), containers));

const containerRunning = containers => {
  const container = find(propContains('Names', `/${CONTAINER_NAME}`), containers);
  return container && container.State === 'running';
};

const runContainer = () =>
  `docker run -d -p ${PG_PORT}:5432 -e POSTGRES_USER=${PG_USER} -e POSTGRES_PASSWORD=${PG_PASSWORD} --name ${CONTAINER_NAME} postgres`;

const startContainer = () => `docker start ${CONTAINER_NAME}`;

// const stopContainer = () =>
//   `docker ps | grep ${CONTAINER_NAME} && docker stop ${CONTAINER_NAME} && docker rm ${CONTAINER_NAME}`;

module.exports.start = async () => {
  try {
    if (process.env.CI) return;
    const containers = await listContainers({ all: true });
    if (containerRunning(containers)) return;
    if (containerExists(containers)) return shell.exec(startContainer(), silent);
    return shell.exec(runContainer(), silent);
  } catch (e) {
    console.log(e);
  }
};

module.exports.stop = async () => {
  // const result = shell.exec(stopContainer(), silent);
  // if (result.code !== 0) {
  //   throw new Error(`Unable to stop postgres DB container:\n${result.stderr}${result.stdout}`);
  // }
};

module.exports.PG_URL = PG_URL;
