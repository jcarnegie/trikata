Feature: Query

  Dynamically generated queries

  Scenario: Find one entity
    Given a GraphQL server with typedefs
      """
      type User implements Node {
        id: ID! @unique
        name: String!
        email: String! @unique
        active: Boolean!
      }
      """
    And rows in the users table
      | id | name     | email       | active |
      | 1  | John Doe | jd@acme.com | false  |
    When a GraphQL query is executed
      """
        query {
          user(where: { email: "jd@acme.com" }) {
            id name email active
          }
        }
      """
    Then the GraphQL query should return json like
      """
      { "data": { "user": { "name": "John Doe", "email": "jd@acme.com", "active": false } } }
      """

  Scenario: Find many entities
    Given a GraphQL server with typedefs
      """
      type User implements Node {
        id: ID! @unique
        name: String!
        email: String! @unique
        active: Boolean!
      }
      """
    And rows in the users table
      | id | name     | email        | active |
      | 1  | John Doe | jd@acme.com  | false  |
      | 2  | Jane Doe | jad@acme.com | false  |
      | 3  | Joe Doe  | jod@acme.com | true   |
    When a GraphQL query is executed
      """
        query {
          users(where: { active: false }) {
            id name email active
          }
        }
      """
    Then the GraphQL query should return
      """
      { "data":
        {
          "users": [
            { "id": "1", "name": "John Doe", "email": "jd@acme.com", "active": false },
            { "id": "2", "name": "Jane Doe", "email": "jad@acme.com", "active": false }
          ]
        }
      }
      """