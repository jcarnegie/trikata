Feature: Mutate with has many relation

  Make sure mutations support has many relations.

  Scenario: Create has many relation on create
    Given a GraphQL server with typedefs
      """
      type User implements Node {
        id: ID! @unique
        name: String!
        email: String!
        active: Boolean!
        addresses: [Address!]!
      }

      type Address implements Node {
        id: ID! @unique
        name: String!
        street: String!
        city: String!
        state: String!
        zip: String!
      }
      """
    When a GraphQL query is executed
      """
      mutation {
        createUser(data: {
          name: "John Doe",
          email: "jd@acme.com",
          active: true,
          addresses: {
            create: [{
              name: "Office",
              street: "123 Main St",
              city: "New York",
              state: "NY",
              zip: "11354"
            }, {
              name: "Home",
              street: "244 Colgate Ave",
              city: "Kensington",
              state: "CA",
              zip: "94708"
            }]
          }
        }) {
          id name email active addresses {
            id name street city state zip
          }
        }
      }
      """
    Then the GraphQL query should return json like
      """
      {
        "data": {
          "createUser": {
            "name": "John Doe",
            "email": "jd@acme.com",
            "active": true,
            "addresses": [{
              "name": "Office",
              "street": "123 Main St",
              "city": "New York",
              "state": "NY",
              "zip": "11354"
            }, {
              "name": "Home",
              "street": "244 Colgate Ave",
              "city": "Kensington",
              "state": "CA",
              "zip": "94708"
            }]
          }
        }
      }
      """
    And the users table should have rows that match:
      | id | name     | email       | active |
      | *  | John Doe | jd@acme.com | true   |
    And the addresses table should have rows that match:
      | id | street          | city       | state | zip   | 
      | *  | 123 Main St     | New York   | NY    | 11354 |
      | *  | 244 Colgate Ave | Kensington | CA    | 94708 |
    And the _address_to_users table should have 2 rows

  Scenario: Connect has many relation on create
    Given a GraphQL server with typedefs
      """
      type User implements Node {
        id: ID! @unique
        name: String!
        email: String!
        active: Boolean!
        addresses: [Address!]!
      }

      type Address implements Node {
        id: ID! @unique
        name: String!
        street: String!
        city: String!
        state: String!
        zip: String!
      }
      """
    And rows in the addresses table
      | id | name   | street          | city       | state | zip   |
      | 1  | Home   | 244 Colgate Ave | Kensington | CA    | 94708 |
      | 2  | Office | 123 Main St     | New York   | NY    | 11354 |
    When a GraphQL query is executed
      """
      mutation {
        createUser(data: {
          name: "John Doe",
          email: "jd@acme.com",
          active: true,
          addresses: {
            connect: [{ id: 1 }, { id: 2 }]
          }
        }) {
          id name email active addresses {
            id name street city state zip
          }
        }
      }
      """
    Then the GraphQL query should return json like
      """
      {
        "data": {
          "createUser": {
            "name": "John Doe",
            "email": "jd@acme.com",
            "active": true,
            "addresses": [{
              "name": "Home",
              "street": "244 Colgate Ave",
              "city": "Kensington",
              "state": "CA",
              "zip": "94708"
            }, {
              "name": "Office",
              "street": "123 Main St",
              "city": "New York",
              "state": "NY",
              "zip": "11354"
            }]
          }
        }
      }
      """
    And the users table should have rows that match:
      | id | name     | email       | active |
      | *  | John Doe | jd@acme.com | true   |
    And the addresses table should have rows that match:
      | id | street          | city       | state | zip   | 
      | *  | 244 Colgate Ave | Kensington | CA    | 94708 |
      | *  | 123 Main St     | New York   | NY    | 11354 |
    And the _address_to_users table should have 2 rows

  Scenario: Create has many relation on update
    Given a GraphQL server with typedefs
      """
      type User implements Node {
        id: ID! @unique
        name: String!
        email: String!
        active: Boolean!
        addresses: [Address!]!
      }

      type Address implements Node {
        id: ID! @unique
        name: String!
        street: String!
        city: String!
        state: String!
        zip: String!
      }
      """
    And rows in the users table
      | id | name     | email        | active |
      | 1  | John Doe | jd@acme.com  | true   |
    And rows in the addresses table
      | id | name   | street      | city     | state | zip   |
      | 1  | Office | 123 Main St | New York | NY    | 11354 |
    And rows in the _address_to_users table
      | id | address_id | user_id |
      | 1  | 1          | 1       |
    When a GraphQL query is executed
      """
        mutation {
          updateUser(data: {
            addresses: {
              create: [{
                name: "Home",
                street: "244 Colgate Ave",
                city: "Kensington",
                state: "CA",
                zip: "94708"
              }]
            }
          }, where: {
            id: "1"
          }) {
            id name email active addresses {
              id name street city state zip
            }
          }
        }
      """
    Then the GraphQL query should return json like
      """
      { 
        "data": {
          "updateUser": {
            "name": "John Doe",
            "email": "jd@acme.com",
            "active": true,
            "addresses": [{
              "name": "Office",
              "street": "123 Main St",
              "city": "New York",
              "state": "NY",
              "zip": "11354"
            }, {
              "name": "Home",
              "street": "244 Colgate Ave",
              "city": "Kensington",
              "state": "CA",
              "zip": "94708"
            }]
          }
        }
      }
      """
    And the addresses table should have rows that match:
      | id | street          | city       | state | zip   | 
      | *  | 123 Main St     | New York   | NY    | 11354 |
      | *  | 244 Colgate Ave | Kensington | CA    | 94708 |
    And the _address_to_users table should have 2 rows

  Scenario: Connect has many relation on update
    Given a GraphQL server with typedefs
      """
      type User implements Node {
        id: ID! @unique
        name: String!
        email: String!
        active: Boolean!
        addresses: [Address!]!
      }

      type Address implements Node {
        id: ID! @unique
        name: String!
        street: String!
        city: String!
        state: String!
        zip: String!
      }
      """
    And rows in the users table
      | id | name     | email        | active |
      | 1  | John Doe | jd@acme.com  | true   |
    And rows in the addresses table
      | id | name   | street          | city       | state | zip   |
      | 1  | Home   | 244 Colgate Ave | Kensington | CA    | 94708 |
      | 2  | Office | 123 Main St     | New York   | NY    | 11354 |
    When a GraphQL query is executed
      """
        mutation {
          updateUser(data: {
            addresses: {
              connect: [{ id: "1" }, { id: "2" }]
            }
          }, where: {
            id: "1"
          }) {
            id name email active addresses {
              id name street city state zip
            }
          }
        }
      """
    Then the GraphQL query should return json like
      """
      { 
        "data": {
          "updateUser": {
            "name": "John Doe",
            "email": "jd@acme.com",
            "active": true,
            "addresses": [{
              "name": "Home",
              "street": "244 Colgate Ave",
              "city": "Kensington",
              "state": "CA",
              "zip": "94708"
            }, {
              "name": "Office",
              "street": "123 Main St",
              "city": "New York",
              "state": "NY",
              "zip": "11354"
            }]
          }
        }
      }
      """
    And the addresses table should have rows that match:
      | id | street          | city       | state | zip   | 
      | *  | 244 Colgate Ave | Kensington | CA    | 94708 |
      | *  | 123 Main St     | New York   | NY    | 11354 |
    And the _address_to_users table should have 2 rows

  Scenario: Disconnect has many relation on update
    Given a GraphQL server with typedefs
      """
      type User implements Node {
        id: ID! @unique
        name: String!
        email: String!
        active: Boolean!
        addresses: [Address!]!
      }

      type Address implements Node {
        id: ID! @unique
        name: String!
        street: String!
        city: String!
        state: String!
        zip: String!
      }
      """
    And rows in the users table
      | id | name     | email        | active |
      | 1  | John Doe | jd@acme.com  | true   |
    And rows in the addresses table
      | id | name   | street          | city       | state | zip   |
      | 1  | Home   | 244 Colgate Ave | Kensington | CA    | 94708 |
      | 2  | Office | 123 Main St     | New York   | NY    | 11354 |
    And rows in the _address_to_users table
      | id | address_id | user_id |
      | 1  | 1          | 1       |
      | 2  | 2          | 1       |
    When a GraphQL query is executed
      """
        mutation {
          updateUser(data: {
            addresses: {
              disconnect: [{ id: "2" }, { id: "1" }]
            }
          }, where: {
            id: "1"
          }) {
            id name email active addresses {
              id name street city state zip
            }
          }
        }
      """
    Then the GraphQL query should return json like
      """
      { 
        "data": {
          "updateUser": {
            "name": "John Doe",
            "email": "jd@acme.com",
            "active": true,
            "addresses": []
          }
        }
      }
      """
    And the addresses table should have 2 rows
    And the _address_to_users table should have 0 rows

  Scenario: Delete has many relation on update
    Given a GraphQL server with typedefs
      """
      type User implements Node {
        id: ID! @unique
        name: String!
        email: String!
        active: Boolean!
        addresses: [Address!]!
      }

      type Address implements Node {
        id: ID! @unique
        name: String!
        street: String!
        city: String!
        state: String!
        zip: String!
      }
      """
    And rows in the users table
      | id | name     | email        | active |
      | 1  | John Doe | jd@acme.com  | true   |
    And rows in the addresses table
      | id | name   | street          | city       | state | zip   |
      | 1  | Home   | 244 Colgate Ave | Kensington | CA    | 94708 |
      | 2  | Office | 123 Main St     | New York   | NY    | 11354 |
    And rows in the _address_to_users table
      | id | address_id | user_id |
      | 1  | 1          | 1       |
      | 2  | 2          | 1       |
    When a GraphQL query is executed
      """
        mutation {
          updateUser(data: {
            addresses: {
              delete: [{ id: "2" }, { id: "1" }]
            }
          }, where: {
            id: "1"
          }) {
            id name email active addresses {
              id name street city state zip
            }
          }
        }
      """
    Then the GraphQL query should return json like
      """
      { 
        "data": {
          "updateUser": {
            "name": "John Doe",
            "email": "jd@acme.com",
            "active": true,
            "addresses": []
          }
        }
      }
      """
    And the addresses table should have 0 rows
    And the _address_to_users table should have 0 rows

  Scenario: Delete has many relation on update
    Given a GraphQL server with typedefs
      """
      type User implements Node {
        id: ID! @unique
        name: String!
        email: String!
        active: Boolean!
        addresses: [Address!]!
      }

      type Address implements Node {
        id: ID! @unique
        name: String!
        street: String!
        city: String!
        state: String!
        zip: String!
      }
      """
    And rows in the users table
      | id | name     | email        | active |
      | 1  | John Doe | jd@acme.com  | true   |
    And rows in the addresses table
      | id | name   | street          | city       | state | zip   |
      | 1  | Office | 123 Main St     | New York   | NY    | 11354 |
    And rows in the _address_to_users table
      | id | address_id | user_id |
      | 1  | 1          | 1       |
    When a GraphQL query is executed
      """
        mutation {
          updateUser(data: {
            addresses: {
              update: [{
                where: { id: "1" },
                data: {
                  name: "Home",
                  street: "244 Colgate Ave",
                  city: "Kensington",
                  state: "CA",
                  zip: "94708"
                }
              }]
            }
          }, where: {
            id: "1"
          }) {
            id name email active addresses {
              id name street city state zip
            }
          }
        }
      """
    Then the GraphQL query should return json like
      """
      { 
        "data": {
          "updateUser": {
            "name": "John Doe",
            "email": "jd@acme.com",
            "active": true,
            "addresses": [{
              "name": "Home",
              "street": "244 Colgate Ave",
              "city": "Kensington",
              "state": "CA",
              "zip": "94708"
            }]
          }
        }
      }
      """
    And the addresses table should have 1 rows
    And the _address_to_users table should have 1 rows

  Scenario: Upsert-create has many relation on update
    Given a GraphQL server with typedefs
      """
      type User implements Node {
        id: ID! @unique
        name: String!
        email: String! @unique
        active: Boolean!
        addresses: [Address!]!
      }

      type Address implements Node {
        id: ID! @unique
        street: String!
        city: String!
        state: String!
        zip: String!
      }
      """
    And rows in the users table
      | id | name     | email        | active |
      | 1  | John Doe | jd@acme.com  | false  |
    When a GraphQL query is executed
      """
      mutation {
        updateUser(
          data: {
            addresses: {
              upsert: [{
                where: { id: "1" },
                update: {
                  street: "123 Main St",
                  city: "New York",
                  state: "NY",
                  zip: "11354"
                },
                create: {
                  street: "244 Colgate Ave",
                  city: "Kensington",
                  state: "CA",
                  zip: "94708"
                }
              }]
            }
          },
          where: { id: "1" }
        ) {
          id name email active addresses {
            id street city state zip
          }
        }
      }
      """
    Then the GraphQL query should return json like
      """
      { 
        "data": {
          "updateUser": {
            "name": "John Doe",
            "email": "jd@acme.com",
            "active": false,
            "addresses": [{
              "street": "244 Colgate Ave",
              "city": "Kensington",
              "state": "CA",
              "zip": "94708"
            }]
          }
        }
      }
      """
    And the addresses table should have rows that match:
      | id | street          | city       | state | zip   |
      | *  | 244 Colgate Ave | Kensington | CA    | 94708 |
    And the _address_to_users table should have 1 rows

  Scenario: Upsert-update has many relation on update
    Given a GraphQL server with typedefs
      """
      type User implements Node {
        id: ID! @unique
        name: String!
        email: String! @unique
        active: Boolean!
        addresses: [Address!]!
      }

      type Address implements Node {
        id: ID! @unique
        street: String!
        city: String!
        state: String!
        zip: String!
      }
      """
    And rows in the users table
      | id | name     | email        | active |
      | 1  | John Doe | jd@acme.com  | false  |
    And rows in the addresses table
      | id | street      | city     | state | zip   |
      | 1  | 123 Main St | New York | NY    | 11354 |
    And rows in the _address_to_users table
      | id | address_id | user_id |
      | 1  | 1          | 1       |
    When a GraphQL query is executed
      """
      mutation {
        updateUser(
          data: {
            addresses: {
              upsert: [{
                where: { id: "1" },
                update: {
                  street: "244 Colgate Ave",
                  city: "Kensington",
                  state: "CA",
                  zip: "94708"
                },
                create: {
                  street: "123 Main St",
                  city: "New York",
                  state: "NY",
                  zip: "11354"
                }
              }]
            }
          },
          where: { id: "1" }
        ) {
          id name email active addresses {
            id street city state zip
          }
        }
      }
      """
    Then the GraphQL query should return json like
      """
      { 
        "data": {
          "updateUser": {
            "name": "John Doe",
            "email": "jd@acme.com",
            "active": false,
            "addresses": [{
              "street": "244 Colgate Ave",
              "city": "Kensington",
              "state": "CA",
              "zip": "94708"
            }]
          }
        }
      }
      """
    And the addresses table should have rows that match:
      | id | street          | city       | state | zip   |
      | *  | 244 Colgate Ave | Kensington | CA    | 94708 |
    And the addresses table should have 1 rows
    And the _address_to_users table should have 1 rows