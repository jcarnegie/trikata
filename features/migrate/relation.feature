Feature: Migrate

  Database relation migrations

  Scenario: Rename relation table migration - case #1: add @relation directive
    Given a database table users
      | name   | type    | size | nullable | primary | unique |
      | id     | varchar | 36   | false    | true    |        |
      | name   | varchar | 255  | false    |         |        |
      | email  | varchar | 255  | false    |         |        |
      | active | boolean | 0    | true     |         |        |
    And a database table addresses
      | name   | type    | size | nullable | primary | unique |
      | id     | varchar | 36   | false    | true    |        |
      | street | varchar | 255  | false    |         |        |
      | city   | varchar | 255  | false    |         |        |
      | state  | varchar | 255  | false    |         |        |
      | zip    | varchar | 255  | false    |         |        |
    And a database table _address_to_users
      | name       | type    | size | nullable | primary | unique |
      | id         | varchar | 36   | false    | true    |        |
      | address_id | varchar | 36   | false    |         |        |
      | user_id    | varchar | 36   | false    |         |        |
    And the _address_to_users table has foreign keys
      | name                              | column     | foreign table | foreign column |
      | _address_to_users_address_id_fkey | address_id | addresses     | id             |
      | _address_to_users_user_id_fkey    | user_id    | users         | id             |
    And rows in the users table
      | id | name     | email       | active |
      | 2  | John Doe | jd@acme.com | false  |
    And rows in the addresses table
      | id | street          | city       | state | zip   |
      | 3  | 244 Colgate Ave | Kensington | CA    | 94708 |
    And rows in the _address_to_users table
      | id | user_id | address_id |
      | 5  | 2       | 3          |
    And GraphQL typedefs
      """
      type User {
        id: ID! @unique
        name: String!
        email: String! @unique
        active: Boolean
        addresses: [Address!]! @relation(name: "UserAddresses")
      }

      type Address {
        id: ID! @unique
        street: String!
        city: String!
        state: String!
        zip: String!
      }
      """
    When migrations are run
    Then the database should have tables "users, addresses, _user_addresses, __migrations"
    And the _user_addresses table should have these rows:
      | id | user_id  | address_id |
      | 5  | 2        | 3          |
    And the _user_addresses table should have foreign keys
      | name                            | column     | foreign table | foreign column |
      | _user_addresses_address_id_fkey | address_id | addresses     | id             |
      | _user_addresses_user_id_fkey    | user_id    | users         | id             |

  Scenario: Rename relation table migration - case #2: remove @relation directive
    Given a database table users
      | name   | type    | size | nullable | primary | unique |
      | id     | varchar | 36   | false    | true    |        |
      | name   | varchar | 255  | false    |         |        |
      | email  | varchar | 255  | false    |         |        |
      | active | boolean | 0    | true     |         |        |
    And a database table addresses
      | name   | type    | size | nullable | primary | unique |
      | id     | varchar | 36   | false    | true    |        |
      | street | varchar | 255  | false    |         |        |
      | city   | varchar | 255  | false    |         |        |
      | state  | varchar | 255  | false    |         |        |
      | zip    | varchar | 255  | false    |         |        |
    And a database table _user_addresses
      | name       | type    | size | nullable | primary | unique |
      | id         | varchar | 36   | false    | true    |        |
      | address_id | varchar | 36   | false    |         |        |
      | user_id    | varchar | 36   | false    |         |        |
    And the _user_addresses table has foreign keys
      | name                            | column     | foreign table | foreign column |
      | _user_addresses_address_id_fkey | address_id | addresses     | id             |
      | _user_addresses_user_id_fkey    | user_id    | users         | id             |
    And rows in the users table
      | id | name     | email       | active |
      | 2  | John Doe | jd@acme.com | false  |
    And rows in the addresses table
      | id | street          | city       | state | zip   |
      | 3  | 244 Colgate Ave | Kensington | CA    | 94708 |
    And rows in the _user_addresses table
      | id | user_id | address_id |
      | 5  | 2       | 3          |
    And GraphQL typedefs
      """
      type User {
        id: ID! @unique
        name: String!
        email: String! @unique
        active: Boolean
        addresses: [Address!]!
      }

      type Address {
        id: ID! @unique
        street: String!
        city: String!
        state: String!
        zip: String!
      }
      """
    When migrations are run
    Then the database should have tables "users, addresses, _address_to_users, __migrations"
    Then the _address_to_users table should have these rows:
      | id | user_id  | address_id |
      | 5  | 2        | 3          |
    And the _address_to_users table should have foreign keys
      | name                              | column     | foreign table | foreign column |
      | _address_to_users_address_id_fkey | address_id | addresses     | id             |
      | _address_to_users_user_id_fkey    | user_id    | users         | id             |

  Scenario: Rename relation table migration - case #3: use @relation oldName arg
    Given a database table users
      | name   | type    | size | nullable | primary | unique |
      | id     | varchar | 36   | false    | true    |        |
      | name   | varchar | 255  | false    |         |        |
      | email  | varchar | 255  | false    |         |        |
      | active | boolean | 0    | true     |         |        |
    And a database table addresses
      | name   | type    | size | nullable | primary | unique |
      | id     | varchar | 36   | false    | true    |        |
      | street | varchar | 255  | false    |         |        |
      | city   | varchar | 255  | false    |         |        |
      | state  | varchar | 255  | false    |         |        |
      | zip    | varchar | 255  | false    |         |        |
    And a database table _user_addresses
      | name       | type    | size | nullable | primary | unique |
      | id         | varchar | 36   | false    | true    |        |
      | address_id | varchar | 36   | false    |         |        |
      | user_id    | varchar | 36   | false    |         |        |
    And the _user_addresses table has foreign keys
      | name                            | column     | foreign table | foreign column |
      | _user_addresses_address_id_fkey | address_id | addresses     | id             |
      | _user_addresses_user_id_fkey    | user_id    | users         | id             |
    And rows in the users table
      | id | name     | email       | active |
      | 2  | John Doe | jd@acme.com | false  |
    And rows in the addresses table
      | id | street          | city       | state | zip   |
      | 3  | 244 Colgate Ave | Kensington | CA    | 94708 |
    And rows in the _user_addresses table
      | id | user_id | address_id |
      | 5  | 2       | 3          |
    And GraphQL typedefs
      """
      type User {
        id: ID! @unique
        name: String!
        email: String! @unique
        active: Boolean
        addresses: [Address!]! @relation(name: "MyAddresses", oldName: "UserAddresses")
      }

      type Address {
        id: ID! @unique
        street: String!
        city: String!
        state: String!
        zip: String!
      }
      """
    When migrations are run
    Then the database should have tables "users, addresses, _my_addresses, __migrations"
    Then the _my_addresses table should have these rows:
      | id | user_id  | address_id |
      | 5  | 2        | 3          |
    And the _my_addresses table should have foreign keys
      | name                          | column     | foreign table | foreign column |
      | _my_addresses_address_id_fkey | address_id | addresses     | id             |
      | _my_addresses_user_id_fkey    | user_id    | users         | id             |

  Scenario: Invalid relations case #1
    Given GraphQL typedefs
      """
      type User {
        id: ID! @unique
        name: String!
        email: String! @unique
        active: Boolean
        addresses: [Address!]!
        homeAddress: Address
      }

      type Address {
        id: ID! @unique
        street: String!
        city: String!
        state: String!
        zip: String!
      }
      """
    When migrations are run
    Then migrations should fail
    And migrations output should look like
      """
      Errors:

        User
          * The relation field 'addresses' must specify a '@relation' directive: '@relation(name: "MyRelation")'
          * The relation field 'homeAddress' must specify a '@relation' directive: '@relation(name: "MyRelation")'
      """