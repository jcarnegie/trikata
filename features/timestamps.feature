Feature: Timestamps

  The timestamp fields createdAt and updatedAt should be added to all types by default

  Scenario: Automatically add createdAt and updatedAt columns to DB
    Given GraphQL typedefs
      """
      type User {
        id: ID! @unique
        name: String!
        email: String! @unique
      }
      """
    When migrations are run
    Then the users table should have columns
      | name       | type                     | size | nullable | primary | unique |
      | id         | varchar                  | 36   | false    | true    | true   |
      | name       | varchar                  | 255  | false    | false   | false  |
      | email      | varchar                  | 255  | false    | false   | true   |
      | created_at | timestamp with time zone | 0    | true     | false   | false  |
      | updated_at | timestamp with time zone | 0    | true     | false   | false  |
    And the users table should have indexes
      | name            | columns | primary | unique | type  |
      | users_pkey      | id      | true    | true   | btree |
      | users_email_key | email   | false   | true   | btree |
