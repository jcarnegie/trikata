Feature: Enhance

  In order to dynamically add features to a GraphQL schema
  We need to 'enhance' it. This means adding new typedefs
  and fields. Eventually we'll back the fields with dynamically
  generated resolvers.

  Scenario: Enhance schema with general metadata
    # Todo: re-enable once test is fixed
    # Given a GraphQL schema definition
    #   """
    #   type User {
    #     id: ID!
    #     name: String!
    #   }      
    #   """
    # When the schema is enhanced
    # Then the enhanced schema should contain general definitions
    #   """
    #   type BatchPayload {
    #     count: Long!
    #   }

    #   scalar Long

    #   enum MutationType {
    #     CREATED
    #     UPDATED
    #     DELETED
    #   }

    #   interface Node {
    #     id: ID!
    #   }

    #   type PageInfo {
    #     hasNextPage: Boolean!
    #     hasPreviousPage: Boolean!
    #     startCursor: String
    #     endCursor: String
    #   }
    #   """

  Scenario: Enhance typedef with CRUD mutation fields
    Given a GraphQL schema definition
      """
      type User {
        id: ID!
        name: String!
      }
      """
    When the schema is enhanced
    Then the enhanced schema should contain enhanced definitions
      """
      type Mutation {
        createUser(data: UserCreateInput!): User!
        updateUser(data: UserUpdateInput!, where: UserWhereUniqueInput!): User
        deleteUser(where: UserWhereUniqueInput!): User
        upsertUser(where: UserWhereUniqueInput!, create: UserCreateInput!, update: UserUpdateInput!): User!
        updateManyUsers(data: UserUpdateInput!, where: UserWhereInput!): BatchPayload!
        deleteManyUsers(where: UserWhereInput!): BatchPayload!
      }
      """

  Scenario: Enhance typedef with create input type
    Given a GraphQL schema definition
      """
      type User {
        id: ID!
        name: String!
      }
      """
    When the schema is enhanced
    Then the enhanced schema should contain enhanced definitions
      """
      input UserCreateInput {
        name: String!
      }
      """
    And the input type UserCreateInput shouldn't contain an id field

  Scenario: Enhance typedef with update input type
    Given a GraphQL schema definition
      """
      type User {
        id: ID!
        name: String!
      }
      """
    When the schema is enhanced
    Then the enhanced schema should contain enhanced definitions
      """
      input UserUpdateInput {
        name: String
      }
      """

  Scenario: Enhance typedef with CRUD query fields
    Given a GraphQL schema definition
      """
      type User {
        id: ID!
        name: String!
      }
      """
    When the schema is enhanced
    Then the enhanced schema should contain enhanced definitions
      """
      type Query {
        users(where: UserWhereInput, orderBy: UserOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [User]!
        user(where: UserWhereUniqueInput!): User
        usersConnection(where: UserWhereInput, orderBy: UserOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): UserConnection!
        node(id: ID!): Node
      }
      """

  Scenario: Enhance typedef with where input type
    Given a GraphQL schema definition
      """
      type User {
        id: ID!
        name: String!
      }
      """
    When the schema is enhanced
    Then the enhanced schema should contain enhanced definitions
      """
      input UserWhereInput {
        AND: [UserWhereInput!]
        OR: [UserWhereInput!]
        id: ID
        id_not: ID
        id_in: [ID!]
        id_not_in: [ID!]
        id_lt: ID
        id_lte: ID
        id_gt: ID
        id_gte: ID
        id_contains: ID
        id_not_contains: ID
        id_starts_with: ID
        id_not_starts_with: ID
        id_ends_with: ID
        id_not_ends_with: ID
        name: String
        name_not: String
        name_in: [String!]
        name_not_in: [String!]
        name_lt: String
        name_lte: String
        name_gt: String
        name_gte: String
        name_contains: String
        name_not_contains: String
        name_starts_with: String
        name_not_starts_with: String
        name_ends_with: String
        name_not_ends_with: String
      }
      """

  Scenario: Enhance typedef with where unique input type
    Given a GraphQL schema definition
      """
      type User {
        id: ID!
        name: String!
      }
      """
    When the schema is enhanced
    Then the enhanced schema should contain enhanced definitions
      """
      input UserWhereUniqueInput {
        id: ID
      }
      """

  Scenario: Enhance typedef with order by input type
    Given a GraphQL schema definition
      """
      type User {
        id: ID!
        name: String!
      }
      """
    When the schema is enhanced
    Then the enhanced schema should contain enhanced definitions
      """
      enum UserOrderByInput {
        id_ASC
        id_DESC
        name_ASC
        name_DESC
        updatedAt_ASC
        updatedAt_DESC
        createdAt_ASC
        createdAt_DESC
      }
      """

  Scenario: Enhance typedef with previous values type
    Given a GraphQL schema definition
      """
      type User {
        id: ID!
        name: String!
      }
      """
    When the schema is enhanced
    Then the enhanced schema should contain enhanced definitions
      """
      type UserPreviousValues {
        id: ID!
        name: String!
      }
      """

  Scenario: Enhance typedef with edge type
    Given a GraphQL schema definition
      """
      type User {
        id: ID!
        name: String!
      }
      """
    When the schema is enhanced
    Then the enhanced schema should contain enhanced definitions
      """
      type UserEdge {
        node: User!
        cursor: String!
      }
      """

  Scenario: Enhance typedef with connection type
    Given a GraphQL schema definition
      """
      type User {
        id: ID!
        name: String!
      }
      """
    When the schema is enhanced
    Then the enhanced schema should contain enhanced definitions
      """
      type UserConnection {
        pageInfo: PageInfo!
        edges: [UserEdge]!
      }
      """

  Scenario: Enhance typedef with subscription field
    Given a GraphQL schema definition
      """
      type User {
        id: ID!
        name: String!
      }
      """
    When the schema is enhanced
    Then the enhanced schema should contain enhanced definitions
      """
      type Subscription {
        user(where: UserSubscriptionWhereInput): UserSubscriptionPayload
      }
      """

  Scenario: Enhance typedef with subscription payload type
    Given a GraphQL schema definition
      """
      type User {
        id: ID!
        name: String!
      }
      """
    When the schema is enhanced
    Then the enhanced schema should contain enhanced definitions
      """
      type UserSubscriptionPayload {
        mutation: MutationType!
        node: User
        updatedFields: [String!]
        previousValues: UserPreviousValues
      }
      """

  Scenario: Enhance typedef with subscription where input type
    Given a GraphQL schema definition
      """
      type User {
        id: ID!
        name: String!
      }
      """
    When the schema is enhanced
    Then the enhanced schema should contain enhanced definitions
      """
      input UserSubscriptionWhereInput {
        AND: [UserSubscriptionWhereInput!]
        OR: [UserSubscriptionWhereInput!]
        mutation_in: [MutationType!]
        updatedFields_contains: String
        updatedFields_contains_every: [String!]
        updatedFields_contains_some: [String!]
        node: UserWhereInput
      }
      """

  Scenario: Enhance typedef with subscription where input type
    Given a GraphQL schema definition
      """
      type User {
        id: ID!
        name: String!
      }
      """
    When the schema is enhanced
    Then the enhanced schema should contain enhanced definitions
      """
      input UserSubscriptionWhereInput {
        AND: [UserSubscriptionWhereInput!]
        OR: [UserSubscriptionWhereInput!]
        mutation_in: [MutationType!]
        updatedFields_contains: String
        updatedFields_contains_every: [String!]
        updatedFields_contains_some: [String!]
        node: UserWhereInput
      }
      """

  Scenario: Enhance typedef with implements Node
    Given a GraphQL schema definition
      """
      type User {
        id: ID!
        name: String!
      }
      """
    When the schema is enhanced
    Then the enhanced schema should contain enhanced definitions
      """
      type User implements Node {
        id: ID!
        name: String!
      }
      """

  Scenario: Enhance typedef retains enums
    Given a GraphQL schema definition
      """
      enum Role {
        ADMIN
        MEMBER
      }

      type User {
        id: ID!
        name: String!
        roles: [Role]
      }
      """
    When the schema is enhanced
    Then the enhanced schema should contain enhanced definitions
      """
      enum Role {
        ADMIN
        MEMBER
      }
      """

  Scenario: Enhance typedef with extended scalars
    Given a GraphQL schema definition
      """
      type User {
        id: ID!
        name: String!
      }
      """
    When the schema is enhanced
    Then the enhanced schema should contain enhanced definitions
      """
      scalar JSON
      """
