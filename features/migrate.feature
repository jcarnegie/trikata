Feature: Migrate

  Database migrations

  Scenario: Create Table Migration
    Given GraphQL typedefs
      """
      type User {
        id: ID! @unique
        name: String!
        email: String! @unique
      }
      """
    When migrations are run
    Then the users table should have columns
      | name   | type    | size | nullable | primary | unique |
      | id     | varchar | 36   | false    | true    | true   |
      | name   | varchar | 255  | false    | false   | false  |
      | email  | varchar | 255  | false    | false   | true   |
    And the users table should have indexes
      | name            | columns | primary | unique | type  |
      | users_pkey      | id      | true    | true   | btree |
      | users_email_key | email   | false   | true   | btree |

  Scenario: Migrate new typedef field
    Given GraphQL typedefs
      """
      type User {
        id: ID! @unique
        name: String!
        email: String! @unique
        active: Boolean
      }
      """
    And a database table users
      | name  | type    | size | nullable | primary | unique |
      | id    | varchar | 36   | false    | true    |        |
      | name  | varchar | 255  | false    |         |        |
      | email | varchar | 255  | false    |         |        |
    When migrations are run
    Then the users table should have columns
      | name   | type    | size | nullable | primary | unique |
      | id     | varchar | 36   | false    | true    | true   |
      | name   | varchar | 255  | false    | false   | false  |
      | email  | varchar | 255  | false    | false   | true   |
      | active | boolean | 0    | true     | false   | false  |
    And the users table should have indexes
      | name            | columns | primary | unique | type  |
      | users_pkey      | id      | true    | true   | btree |
      | users_email_key | email   | false   | true   | btree |

  Scenario: Migrate new @unique directive for typedef field
    Given a database table users
      | name  | type    | size | nullable | primary | unique |
      | id    | varchar | 36   | false    | true    |        |
      | name  | varchar | 255  | false    |         |        |
      | email | varchar | 255  | false    |         |        |
    And GraphQL typedefs
      """
      type User {
        id: ID! @unique
        name: String!
        email: String! @unique
      }
      """
    When migrations are run
    Then the users table should have columns
      | name   | type    | size | nullable | primary | unique |
      | id     | varchar | 36   | false    | true    | true   |
      | name   | varchar | 255  | false    | false   | false  |
      | email  | varchar | 255  | false    | false   | true   |
    And the users table should have indexes
      | name            | columns | primary | unique | type  |
      | users_pkey      | id      | true    | true   | btree |
      | users_email_key | email   | false   | true   | btree |

  Scenario: Migrate new @index directive for typedef field
    Given a database table users
      | name  | type    | size | nullable | primary | unique |
      | id    | varchar | 36   | false    | true    |        |
      | name  | varchar | 255  | false    |         |        |
      | email | varchar | 255  | false    |         |        |
    And GraphQL typedefs
      """
      type User {
        id: ID! @unique
        name: String!
        email: String! @index
      }
      """
    When migrations are run
    Then the users table should have columns
      | name  | type    | size | nullable | primary | unique |
      | id    | varchar | 36   | false    | true    | true   |
      | name  | varchar | 255  | false    |         |        |
      | email | varchar | 255  | false    |         |        |
    And the users table should have indexes
      | name            | columns | primary | unique | type  |
      | users_pkey      | id      | true    | true   | btree |
      | users_email_key | email   | false   | false  | btree |

  Scenario: Migrate new composite @index directive for typedef
    Given a database table users
      | name  | type    | size | nullable | primary | unique |
      | id    | varchar | 36   | false    | true    |        |
      | name  | varchar | 255  | false    |         |        |
      | email | varchar | 255  | false    |         |        |
    And GraphQL typedefs
      """
      type User
        @index(columns: ["name", "email"])
      {
        id: ID! @unique
        name: String!
        email: String!
      }
      """
    When migrations are run
    Then the users table should have columns
      | name  | type    | size | nullable | primary | unique |
      | id    | varchar | 36   | false    | true    | true   |
      | name  | varchar | 255  | false    |         |        |
      | email | varchar | 255  | false    |         |        |
    And the users table should have indexes
      | name                 | columns       | primary | unique | type  |
      | users_pkey           | id            | true    | true   | btree |
      | users_name_email_key | name, email   | false   | false  | btree |

  Scenario: Add column default when @default added to typedef field
    Given a database table users
      | name   | type    | size | nullable | primary | unique |
      | id     | varchar | 36   | false    | true    |        |
      | name   | varchar | 255  | false    |         |        |
      | email  | varchar | 255  | false    |         |        |
      | active | boolean | 0    | false    |         |        |
    And GraphQL typedefs
      """
      type User {
        id: ID! @unique
        name: String!
        email: String!
        active: Boolean! @default(value: true)
      }
      """
    When migrations are run
    Then the users table should have columns
      | name   | type    | size | nullable | primary | unique | default |
      | id     | varchar | 36   | false    | true    | true   | null    |
      | name   | varchar | 255  | false    |         |        | null    |
      | email  | varchar | 255  | false    |         |        | null    |
      | active | boolean | 0    | false    |         |        | true    |

  Scenario: Remove column default when @default removed from typedef field
    Given a database table users
      | name   | type    | size | nullable | primary | unique | default |
      | id     | varchar | 36   | false    | true    |        |         |
      | name   | varchar | 255  | false    |         |        |         |
      | email  | varchar | 255  | false    |         |        |         |
      | active | boolean |      | false    |         |        | true    |
    And GraphQL typedefs
      """
      type User {
        id: ID! @unique
        name: String!
        email: String!
        active: Boolean!
      }
      """
    When migrations are run
    Then the users table should have columns
      | name   | type    | size | nullable | primary | unique | default |
      | id     | varchar | 36   | false    | true    | true   | null    |
      | name   | varchar | 255  | false    |         |        | null    |
      | email  | varchar | 255  | false    |         |        | null    |
      | active | boolean | 0    | false    |         |        | null    |

  Scenario: Migrate new JSON field type
    And GraphQL typedefs
      """
      type User {
        id: ID! @unique
        name: String!
        email: String!
        data: JSON
      }
      """
    When migrations are run
    Then the users table should have columns
      | name  | type    | size | nullable | primary | unique |
      | id    | varchar | 36   | false    | true    | true   |
      | name  | varchar | 255  | false    |         |        |
      | email | varchar | 255  | false    |         |        |
      | data  | jsonb   | 0    | true     |         |        |

  Scenario: Set column to not null when ! added to typedef field def
    Given a database table users
      | name   | type    | size | nullable | primary | unique | default |
      | id     | varchar | 36   | false    | true    |        |         |
      | name   | varchar | 255  | true     |         |        |         |
      | email  | varchar | 255  | false    |         |        |         |
      | active | boolean |      | false    |         |        | true    |
    And GraphQL typedefs
      """
      type User {
        id: ID! @unique
        name: String!
        email: String!
        active: Boolean!
      }
      """
    When migrations are run
    Then the users table should have columns
      | name   | type    | size | nullable | primary | unique | default |
      | id     | varchar | 36   | false    | true    | true   | null    |
      | name   | varchar | 255  | false    |         |        | null    |
      | email  | varchar | 255  | false    |         |        | null    |
      | active | boolean | 0    | false    |         |        | null    |

  Scenario: Drop column not null when ! removed from typedef field def
    Given a database table users
      | name   | type    | size | nullable | primary | unique | default |
      | id     | varchar | 36   | false    | true    |        |         |
      | name   | varchar | 255  | false    |         |        |         |
      | email  | varchar | 255  | false    |         |        |         |
      | active | boolean |      | false    |         |        | true    |
    And GraphQL typedefs
      """
      type User {
        id: ID! @unique
        name: String
        email: String!
        active: Boolean!
      }
      """
    When migrations are run
    Then the users table should have columns
      | name   | type    | size | nullable | primary | unique | default |
      | id     | varchar | 36   | false    | true    | true   | null    |
      | name   | varchar | 255  | true     |         |        | null    |
      | email  | varchar | 255  | false    |         |        | null    |
      | active | boolean | 0    | false    |         |        | null    |

  Scenario: Change column type when typedef field type changed
    Given a database table users
      | name   | type    | size | nullable | primary | unique | default |
      | id     | varchar | 36   | false    | true    |        |         |
      | name   | varchar | 255  | false    |         |        |         |
      | email  | varchar | 255  | false    |         |        |         |
      | active | boolean | 0    | false    |         |        | true    |
    And GraphQL typedefs
      """
      type User {
        id: ID! @unique
        name: String!
        email: String!
        active: String!
      }
      """
    When migrations are run
    Then the users table should have columns
      | name   | type    | size | nullable | primary | unique | default |
      | id     | varchar | 36   | false    | true    | true   | null    |
      | name   | varchar | 255  | false    |         |        | null    |
      | email  | varchar | 255  | false    |         |        | null    |
      | active | varchar | 255  | false    |         |        | null    |

  Scenario: Drop column from table when field removed from typedef
    Given a database table users
      | name   | type    | size | nullable | primary | unique | default |
      | id     | varchar | 36   | false    | true    |        |         |
      | name   | varchar | 255  | false    |         |        |         |
      | email  | varchar | 255  | false    |         |        |         |
      | active | boolean | 0    | false    |         |        | true    |
    And GraphQL typedefs
      """
      type User {
        id: ID! @unique
        name: String!
        email: String!
      }
      """
    When migrations are run
    Then the users table should have columns
      | name   | type    | size | nullable | primary | unique | default |
      | id     | varchar | 36   | false    | true    | true   | null    |
      | name   | varchar | 255  | false    |         |        | null    |
      | email  | varchar | 255  | false    |         |        | null    |

  Scenario: Drop a table from the database when a typedef is removed from the schema and migrations are run
    Given a database table users
      | name   | type    | size | nullable | primary | unique | default |
      | id     | varchar | 36   | false    | true    |        |         |
      | name   | varchar | 255  | false    |         |        |         |
      | email  | varchar | 255  | false    |         |        |         |
      | active | boolean | 0    | false    |         |        | true    |
    And a database table posts
      | name   | type    | size | nullable | primary | unique | default |
      | id     | varchar | 36   | false    | true    |        |         |
      | title  | varchar | 255  | false    |         |        |         |
      | body   | varchar | 255  | false    |         |        |         |
    And GraphQL typedefs
      """
      type Post {
        id: ID! @unique
        title: String!
        body: String!
      }
      """
    When migrations are run
    Then the database should have tables "posts, __migrations"

  Scenario: Drop an index from a table when a @unique directive is removed from a field
    Given a database table users
      | name   | type    | size | nullable | primary | unique | default |
      | id     | varchar | 36   | false    | true    | true   |         |
      | name   | varchar | 255  | false    |         |        |         |
      | email  | varchar | 255  | false    |         | true   |         |
      | active | boolean | 0    | false    |         |        | true    |
    And GraphQL typedefs
      """
      type User {
        id: ID! @unique
        name: String!
        email: String!
      }
      """
    When migrations are run
    Then the database shouldn't have an index named users_email_key

  Scenario: Drop an index from a table when a @index directive is removed from a field
    Given a database table users
      | name   | type    | size | nullable | primary | unique | default |
      | id     | varchar | 36   | false    | true    | true   |         |
      | name   | varchar | 255  | false    |         |        |         |
      | email  | varchar | 255  | false    |         |        |         |
      | active | boolean | 0    | false    |         |        | true    |
    And users table indexes
      | name            | columns | primary | unique | type  |
      | users_email_key | email   | false   | false  | btree |
    And GraphQL typedefs
      """
      type User {
        id: ID! @unique
        name: String!
        email: String!
      }
      """
    When migrations are run
    Then the database shouldn't have an index named users_email_key

  Scenario: Migrate has one relation
    Given GraphQL typedefs
      """
      type User {
        id: ID! @unique
        name: String!
        email: String!
        address: Address
      }

      type Address {
        id: ID! @unique
        street: String!
        city: String!
        state: String!
        zip: String!
      }
      """
    When migrations are run
    Then the database should have tables "users, addresses, _address_to_users, __migrations"
    And the _address_to_users relation table should have columns
      | name       | type    | size | nullable | primary | unique | default |
      | id         | varchar | 36   | false    | true    | true   | null    |
      | address_id | varchar | 36   | false    |         |        | null    |
      | user_id    | varchar | 36   | false    |         |        | null    |
    And the _address_to_users table should have indexes
      | name                         | columns    | primary | unique | type  |
      | _address_to_users_pkey       | id         | true    | true   | btree |
      | _address_to_users_address_id | address_id | false   | false  | btree |
      | _address_to_users_user_id    | user_id    | false   | false  | btree |
    And the _address_to_users table should have foreign keys
      | name                              | column     | foreign table | foreign column |
      | _address_to_users_address_id_fkey | address_id | addresses     | id             |
      | _address_to_users_user_id_fkey    | user_id    | users         | id             |

  Scenario: Migrate has many relation
    Given GraphQL typedefs
      """
      type User {
        id: ID! @unique
        name: String!
        email: String!
        addresses: [Address!]!
      }

      type Address {
        id: ID! @unique
        street: String!
        city: String!
        state: String!
        zip: String!
      }
      """
    When migrations are run
    Then the database should have tables "users, addresses, _address_to_users, __migrations"
    And the _address_to_users relation table should have columns
      | name       | type    | size | nullable | primary | unique | default |
      | id         | varchar | 36   | false    | true    | true   | null    |
      | address_id | varchar | 36   | false    |         |        | null    |
      | user_id    | varchar | 36   | false    |         |        | null    |
    And the _address_to_users table should have indexes
      | name                         | columns    | primary | unique | type  |
      | _address_to_users_pkey       | id         | true    | true   | btree |
      | _address_to_users_address_id | address_id | false   | false  | btree |
      | _address_to_users_user_id    | user_id    | false   | false  | btree |
    And the _address_to_users table should have foreign keys
      | name                              | column     | foreign table | foreign column |
      | _address_to_users_address_id_fkey | address_id | addresses     | id             |
      | _address_to_users_user_id_fkey    | user_id    | users         | id             |
  
  Scenario: Migrating stores schema data in database
    Given GraphQL typedefs
      """
      type User {
        id: ID! @unique
        name: String!
        email: String!
      }
      """
    When migrations are run
    Then the database should have tables "users, __migrations"
    # Then the __migrations table should have these rows:
    #   | id | sdl | schema |
    #   | 1 | type User {\n  id: ID! @unique\n  name: String!\n  email: String!\n} | {"types":[{"name":"User","tableName":"users","fields":[{"parentType":null,"name":"id","columnName":"id","type":"ID","isList":false,"listItemsNullable":false,"sqlType":"varchar","size":36,"nullable":false,"primaryKey":true,"default":null,"unique":true,"isRelation":false,"directives":[{"name":"unique","args":{}}]},{"parentType":null,"name":"name","columnName":"name","type":"String","isList":false,"listItemsNullable":false,"sqlType":"varchar","size":255,"nullable":false,"primaryKey":false,"default":null,"unique":false,"isRelation":false,"directives":[]},{"parentType":null,"name":"email","columnName":"email","type":"String","isList":false,"listItemsNullable":false,"sqlType":"varchar","size":255,"nullable":false,"primaryKey":false,"default":null,"unique":false,"isRelation":false,"directives":[]}],"indexes":[{"parentType":null,"name":"users_pkey","columns":["id"],"type":"btree","primary":false,"unique":true}],"isRelation":false,"directives":[]}],"enums":[],"relations":[]} |

  Scenario: Allow for stateless types
    Given GraphQL typedefs
      """
      type TemporaryUser @stateless {
        id: ID!
        name: String!
        email: String!
      }
      """
    When migrations are run
    Then the temporary_users table shouldn't exist

  Scenario: Migrate @sql custom column type
    Given GraphQL typedefs
      """
      type User {
        id: ID! @unique
        loginAttempts: Int @sql(type: "smallint")
      }
      """
    And a database table users
      | name            | type    | size | nullable | primary | unique |
      | id              | varchar | 36   | false    | true    |        |
      | login_attempts  | integer | 0    | true     |         |        |
    When migrations are run
    Then the users table should have columns
      | name            | type     | size | nullable | primary | unique |
      | id              | varchar  | 36   | false    | true    | true   |
      | login_attempts  | smallint | 0    | true     |         |        |

  Scenario: Migrate @sql custom jsonb column type (strongly typed in GQL)
    Given GraphQL typedefs
      """
      type CustomData @stateless {
        foo: String
      }

      type User {
        id: ID! @unique
        customData: CustomData @sql(type: "jsonb")
      }
      """
    When migrations are run
    Then the database should have tables "users, __migrations"
    Then the users table should have columns
      | name         | type    | size | nullable | primary | unique |
      | id           | varchar | 36   | false    | true    | true   |
      | custom_data  | jsonb   | 0    | true     |         |        |