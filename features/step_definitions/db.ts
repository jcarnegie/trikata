/* eslint-disable */
import p from 'bluebird';
import { Given, When, Then } from 'cucumber';
import { expect } from 'chai';
import {
  any,
  append,
  equals,
  filter,
  find,
  head,
  is,
  isEmpty,
  isNil,
  join,
  last,
  length,
  map,
  keys,
  path,
  pickAll,
  propEq,
  range,
  reduce,
  split,
  tail,
  zipObj,
} from 'ramda';
// Todo: generalize this to support multipe DBs
import { execStmt } from '../../src/db/pg';
import { parseDBSchema } from '../../src/db/pg/ddl';
import {
  createSchemaSql,
  JOIN_TABLE_METADATA_SQL,
  TABLE_INDEX_METADATA_SQL,
} from '../../src/db/pg/sql';
import { sortedValues } from '../../src/util/object';

Given('a row in the {word} table', async function (tableName, rowData) {
  const table = rowData.raw();
  const columns = head(table);
  const values = last(table);
  const columnsStr = join(', ', columns);
  const placeholders = join(', ', map(i => `$${i}`, range(1, length(values) + 1)));
  const insertQ = `insert into ${this.dbCtx.schemaName}.${tableName} (${columnsStr}) values (${placeholders})`;
  await this.client.query(insertQ, values);
});

Given('rows in the {word} table', async function (tableName, rowData) {
  const table = rowData.raw();
  const columns = head(table);
  const rows = tail(table);
  const columnsStr = join(', ', columns);
  const placeholders = join(', ', map(i => `$${i}`, range(1, length(head(rows)) + 1)));
  const insertQ = `insert into ${this.dbCtx.schemaName}.${tableName} (${columnsStr}) values (${placeholders})`;
  await p.all(map(v => this.client.query(insertQ, v), rows));
});

Given('a database table {word}', async function(tableName, rowData) {
  const table = rowData.raw();
  const metadata = tail(table);
  const colSpec = c => {
    const [name, type, size, nullable, primary, unique, def] = c;
    const dataType = (!isEmpty(size) && parseInt(size, 10) > 0) ? `${type} (${size})` : type;
    const nullability = nullable === 'false' ? 'not null' : '';
    const pkIdx = primary === 'true' ? 'primary key' : '';
    const uniqueIdx = unique === 'true' ? 'unique' : '';
    // Todo: make this general so we can test multiple DBs
    const defValue = (!isEmpty(def) && !isNil(def))
      ? `default ${type === 'varchar' ? `'${def}'` : def}`
      : '';
    return `${name} ${dataType} ${nullability} ${pkIdx} ${uniqueIdx} ${defValue}`;
  };
  const sql = `
    create table ${this.dbCtx.schemaName}.${tableName} (
      ${join(',\n', map(colSpec, metadata))}
    );
  `;
  await execStmt(this.client, createSchemaSql(this.dbCtx));
  await this.client.query(sql);
});

Given('{word} table indexes', async function (tableName, indexData) {
  const data = indexData.raw();
  const indexes = tail(data);
  await p.all(map(async row => {
    const maybeToBool = v => v === 'true' ? true : v === 'false' ? false : v;
    const [name, cols, p, u, type] = row;
    const columns = split(/\s*,\s*/, cols);
    const primary = maybeToBool(p);
    const unique = maybeToBool(u);
    const sql = `create ${unique ? 'unique' : ''} index ${name} on ${this.dbCtx.schemaName}.${tableName} using ${type} (${cols})`;
    await this.client.query(sql);
  }, indexes));
});

Given('the {word} table has foreign keys', async function (tableName, fkData) {
  const data = fkData.raw();
  const foreignKeys = tail(data);
  const { schemaName } = this.dbCtx;
  await p.all(map(async row => {
    const maybeToBool = v => v === 'true' ? true : v === 'false' ? false : v;
    const [name, column, foreignTable, foreignColumn] = row;
    const sql = `alter table ${schemaName}.${tableName} add constraint ${name} foreign key (${column}) references ${schemaName}.${foreignTable} (${foreignColumn})`
    await this.client.query(sql);
  }, foreignKeys));
});

Then('a new row should be in the {word} table:', async function (tableName, rowData) {
  const maybeToBool = v => v === 'true' ? true : v === 'false' ? false : v;
  const table = rowData.rawTable;
  const data = zipObj(head(table), map(maybeToBool, last(table)));
  const columns = join(', ', keys(data));
  // Todo: this means that this is dependent on 'When a GraphQL query is executed'
  const id = path(['data', keys(this.queryResponse.data)[0], 'id'], this.queryResponse);
  const result = await this.client.query(`select ${columns} from ${this.dbCtx.schemaName}.${tableName} where id = $1`, [id]);
  const dbRow = result.rows[0];
  if (data.id === '*') data.id = id;
  map(col => {
    if (data[col] === '*') return;

    if (is(String, data[col])) {
      const matches = (data[col] || '').match(/^\/(.*)\/$/);

      if (matches && matches[1]) {
        const matcher = new RegExp(matches[1]);
        return expect(dbRow[col]).to.match(matcher);
      }
    }

    expect(dbRow[col]).to.equal(data[col]);
  }, keys(data));
});

Then('the {word} table should have rows that match:', async function(tableName, rowData) {
  const maybeToBool = v => v === 'true' ? true : v === 'false' ? false : v;
  const table = rowData.raw();
  const formatFn = r => zipObj(head(table), map(maybeToBool, r));
  const data = map(formatFn, tail(table));
  const columns = join(', ', head(table));
  const sql = `select ${columns} from ${this.dbCtx.schemaName}.${tableName}`;
  const result = await this.client.query(sql);
  const dbRows = result.rows;
  map(r => {
    const reduceFn = (cols, col) =>
      (r[col] !== '*') ? append(col, cols) : cols;
    const matchableColumns = reduce(reduceFn, [], keys(r));
    const expectedData = pickAll(matchableColumns, r);
    const matched = any(equals(true), map(dbRow => {
      const dbData = pickAll(matchableColumns, dbRow);
      return equals(expectedData, dbData);
    }, dbRows));
    if (!matched) throw new Error("Database didn't contain matching rows");
  }, data);
});

Then('a row with id {int} in the {word} table is not found', async function (id, tableName) {
  const result = await this.client.query(`select * from ${this.dbCtx.schemaName}.${tableName} where id = $1`, [id]);
  expect(length(result.rows)).to.equal(0);
});

Then('the {word} table should have these rows:', async function (tableName, rowData) {
  const table = rowData.raw();
  const columns = head(table);
  const maybeToBool = v => v === 'true' ? true : v === 'false' ? false : v;
  const rows = map(row => map(maybeToBool, row), tail(table));
  const columnsStr = join(', ', columns);
  const selectQ = `select ${columnsStr} from ${this.dbCtx.schemaName}.${tableName}`;
  const results = await this.client.query(selectQ);
  const dbRows = results.rows;
  map(row => {
    const dbRow = find(propEq('id', row[0]), dbRows);
    expect(sortedValues(columns, dbRow)).to.eql(row);
  }, rows);
});

Then('the {word} table should have indexes', async function (tableName, indexData) {
  const table = indexData.raw();
  const columns = head(table);
  const data = tail(table);
  const dbIndexes = (await this.client.query(TABLE_INDEX_METADATA_SQL, [tableName, this.dbCtx.schemaName])).rows;
  map(row => {
    const maybeToBool = v => v === 'true' ? true : v === 'false' ? false : v;
    const [name, columns, p, u, type] = row;
    // const columns = split(/\s*,\s*/, cols);
    const primary = maybeToBool(p);
    const unique = maybeToBool(u);
    const dbIndex = find(propEq('indexname', name), dbIndexes);
    if (!dbIndex) throw new Error(`No index '${name}' for '${this.dbCtx.schemaName}.${tableName}' table`);
    expect(columns).to.equal(dbIndex.columns);
    expect(unique).to.equal(dbIndex.unique);
    expect(primary).to.equal(dbIndex.primary);
    expect(type).to.equal(dbIndex.method);
  }, data);
});

Then('the {word} table should have foreign keys', async function (tableName, fkData) {
  const table = fkData.raw();
  const columns = head(table);
  const data = tail(table);
  const allDBConstraints = (await this.client.query(JOIN_TABLE_METADATA_SQL, [this.dbCtx.schemaName])).rows;
  const dbConstraints = filter(r => r.table_name === tableName, allDBConstraints);
  map(row => {
    const maybeToBool = v => v === 'true' ? true : v === 'false' ? false : v;
    const [name, column, foreignTable, foreignColumn] = row;
    const dbConstraint = find(propEq('constraint_name', name), dbConstraints);
    if (!dbConstraint) throw new Error(`No constraint ${name} found`);
    expect(column).to.equal(dbConstraint.column_name);
    expect(foreignTable).to.equal(dbConstraint.foreign_table_name);
    expect(foreignColumn).to.equal(dbConstraint.foreign_column_name);
  }, data);
});

Then('the database should only have the {word} table', async function (tableName) {
  const dbTables = await this.db.tableNames(this.dbCtx);
  expect(dbTables).to.eql([tableName]);
});

Then('the database shouldn\'t have an index named {word}', async function (indexName) {
  const metadata = await parseDBSchema(this.dbCtx);
  const index = find(propEq('name', indexName), metadata.types[0].indexes);
  expect(index).to.equal(undefined);
});

Then(/the database should have tables "(.*)"/, async function (rawTables) {
  const tables = split(/\s*,\s*/, rawTables);
  const dbTables = await this.db.tableNames(this.dbCtx);
  expect(tables.sort()).to.eql(dbTables.sort());
});

Then('the {word} table should have {int} rows', async function (tableName, rowCount) {
  const sql = `select count(*) from ${this.dbCtx.schemaName}.${tableName}`;
  const results = await this.client.query(sql);
  const dbRowCount = parseInt(results.rows[0].count, 10);
  expect(dbRowCount).to.eql(rowCount);
});

Then('the {word} table is empty', async function (tableName) {
  const sql = `select count(*) from ${this.dbCtx.schemaName}.${tableName}`;
  const results = await this.client.query(sql);
  const dbRowCount = parseInt(results.rows[0].count, 10);
  expect(dbRowCount).to.eql(0);
});