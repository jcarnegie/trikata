/* eslint-disable */
import chaiThings from 'chai-things';
import chai, { expect } from 'chai';
import { promise } from 'fluture';
import { Given, When, Then } from 'cucumber';
import { find, isEmpty, head, map, prop, propEq, tail, uniq } from 'ramda';

When('migrations are run', async function () {
  try {
    const migrateP = promise(this.db.migrate(this.typedefs, this.schema));
    this.migrationsResult = await migrateP;
  } catch (e) {
    // console.error("When migrations are run error:", e);
    this.migrationsError = e;
  }
});

Then('the {word} table should have columns', async function (tableName, rowData) {
  const table = rowData.raw();
  const values = tail(table);
  const dbSchema = await this.db.parseDBSchema();
  const dbTypes = dbSchema.types;
  const dbType = find(propEq('tableName', tableName), dbTypes);
  // make sure db has columns specified in rowData - note: createdAt and updatedAt
  // are added automatically, let's not require all of the tests to add them
  const expectedColumns = uniq([...map(head, values), 'created_at', 'updated_at']);
  const dbColumns = map(prop('columnName'), dbType.fields);
  expect(dbColumns).to.eql(expectedColumns);
  // make sure column specs are same
  map(v => {
    const castInput = v => {
      if (v === 'true') return true;
      if (v === 'false') return false;
      if (v === 'null') return null;
      if (v === 'undefined') return undefined;
      if (v === undefined) return null;
      return v;
    }
    const [name, sqlType, size, nullable, primaryKey, unique, def] = v;
    const field = find(propEq('columnName', name), dbType.fields)
    if (!field) throw new Error(`Column ${tableName}.${name} not found in DB`);
    expect(field.sqlType).to.equal(sqlType);
    expect(field.size).to.equal(isEmpty(size) ? null : parseInt(size, 10));
    expect(field.nullable).to.equal((nullable === 'true') ? true : false);
    expect(field.primaryKey).to.equal((primaryKey === 'true') ? true : false);
    expect(field.unique).to.equal((unique === 'true') ? true : false);
    expect(field.default).to.equal(castInput(def));
  }, values)
});

// Relation / join tables don't have createdAt/updatedAt fields added automatically
Then("the {word} relation table should have columns", async function(
  tableName,
  rowData
) {
  const table = rowData.raw();
  const values = tail(table);
  const dbSchema = await this.db.parseDBSchema();
  const dbTypes = dbSchema.types;
  const dbType = find(propEq("tableName", tableName), dbTypes);
  // make sure db has columns specified in rowData - note: createdAt and updatedAt
  // are added automatically, let's not require all of the tests to add them
  const expectedColumns = map(head, values);
  const dbColumns = map(prop("columnName"), dbType.fields);
  expect(dbColumns).to.eql(expectedColumns);
  // make sure column specs are same
  map(v => {
    const castInput = v => {
      if (v === "true") return true;
      if (v === "false") return false;
      if (v === "null") return null;
      if (v === "undefined") return undefined;
      if (v === undefined) return null;
      return v;
    };
    const [name, sqlType, size, nullable, primaryKey, unique, def] = v;
    const field = find(propEq("columnName", name), dbType.fields);
    if (!field) throw new Error(`Column ${tableName}.${name} not found in DB`);
    expect(field.sqlType).to.equal(sqlType);
    expect(field.size).to.equal(isEmpty(size) ? null : parseInt(size, 10));
    expect(field.nullable).to.equal(nullable === "true" ? true : false);
    expect(field.primaryKey).to.equal(primaryKey === "true" ? true : false);
    expect(field.unique).to.equal(unique === "true" ? true : false);
    expect(field.default).to.equal(castInput(def));
  }, values);
});

Then('migrations should fail', function() {
  expect(this.migrationsError[0]).to.equal(false);
});

Then('migrations output should look like', function (docString) {
  expect(this.migrationsError[1]).to.include(docString);
});

Then('the {word} table shouldn\'t exist', async function(tableName) {
  const dbSchema = await this.db.parseDBSchema();
  const dbType = find(propEq('tableName', tableName), dbSchema.types);
  if (dbType) throw new Error(`${tableName} table found in database. It shouldn't be there!`);
});