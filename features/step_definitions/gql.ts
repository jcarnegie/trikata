/* eslint-disable */
import uuid from 'uuid/v4';
import chaiThings from 'chai-things';
import chaiSubset from 'chai-subset';
import chai, { expect } from 'chai';
import { chain, promise } from 'fluture';
import { Given, When, Then } from 'cucumber';
import { graphql } from 'graphql';
import { path, split } from 'ramda';
import LoaderManager from '../../src/schema/loaders';
import { createParseSchemaContext } from '../../src/schema/context';
import { create, parseTypedefs } from '../../src/schema';
import { init } from '../../src/db/pg';
import { sqlType } from '../../src/db/pg/meta';
import { stringify } from '../../src/util/string';

chai.use(chaiThings);
chai.use(chaiSubset);

Given('GraphQL typedefs', async function (typedefs) {
  const parseContext = { sqlType };
  const schema = parseTypedefs(parseContext, typedefs);
  const dbCtx = { schemaName: 'trikata', client: this.client, schema, idFn: uuid };
  const db = init(dbCtx);
  this.db = db;
  this.context = { db, schema };
  this.typedefs = typedefs;
  this.schema = schema;
  const dbTables = await this.db.tableNames(this.dbCtx);
});

Given('a GraphQL server with typedefs', function (typedefs) {
  const initServerWithTypedefs = parseContext => {
    const schema = parseTypedefs(parseContext, typedefs);
    const dbCtx = {
      schemaName: "trikata",
      client: this.client,
      schema,
      idFn: uuid,
    };
    const db = init(dbCtx);
    this.db = db;
    this.context = { db, schema };
    this.typedefs = typedefs;
    this.schema = schema;
    this.gqlSchema = create(parseContext, this.context, typedefs);
    return db.migrate(this.typedefs, schema);
  };
  return promise(chain(initServerWithTypedefs)(createParseSchemaContext()));
});

When("the user is logged in", function() {
  this.context.user = { id: "1" };
});

When(/the user is logged in with the (.*) role/, function(role) {
  this.context.user = { id: "1", roles: [role] };
});

When('a GraphQL query is executed', async function (query) {
  this.context.loaders = new LoaderManager();
  this.queryResponse = await graphql(this.gqlSchema, query, null, this.context);
  // console.log('queryResponse:', this.queryResponse);
});

Then('the GraphQL query should return', function (responseJson) {
  expect(this.queryResponse).to.deep.equal(JSON.parse(responseJson));
});

Then(/the GraphQL query result should have error message "(.*)"/, function(errorMessage) {
  expect(this.queryResponse.errors[0].message).to.equal(errorMessage);
});

Then('the GraphQL query should return json like', function (responseJson) {
  const expectedResponse = JSON.parse(responseJson);
  expect(this.queryResponse).to.containSubset(expectedResponse);
});

Then('the GraphQL query result {word} should have an id property', function (field) {
  const resp = path(['queryResponse', 'data', field], this);
  expect(resp).to.have.property('id');
  expect(resp.id).to.be.a('string');
});

Then(/the GraphQL result field "(.*)" should match "(.*)"/, function (fieldPath, matchString) {
  const parts = split('.', fieldPath);
  const value = path(['queryResponse', ...parts], this);
  expect(value).to.have.string(matchString);
});
