// babel-register can't load configuration from babel.config.js
// load babel configuration explicitly here
// filename should be in top of folder to be loaded first

// @ts-ignore
// tslint:disable-next-line
require("@babel/register")({
  extensions: [".js", ".ts"],
  presets: [
    ["@babel/preset-env"],
    ["@babel/preset-typescript", { allExtensions: true }],
  ],
  plugins: [
    // "@babel/proposal-class-properties",
    // "@babel/proposal-object-rest-spread",
  ],
});