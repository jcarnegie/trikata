Feature: Enhanced Schema for Has Many Relations

  Schema enhancements for has many relations.

  Scenario: Enhance typedef with has many create input type
    Given a GraphQL schema definition
      """
      type User implements Node {
        id: ID! @unique
        name: String!
        email: String! @unique
        addresses: [Address!]!
      }

      type Address implements Node {
        id: ID! @unique
        street: String!
        city: String!
        state: String!
        zip: String!
      }
      """
    When the schema is enhanced
    Then the enhanced schema should contain enhanced definitions
      """
      input UserCreateInput {
        name: String!
        email: String!
        addresses: AddressCreateManyInput
      }

      input AddressCreateManyInput {
        create: [AddressCreateInput!]
        connect: [AddressWhereUniqueInput!]
      }

      input AddressCreateInput {
        street: String!
        city: String!
        state: String!
        zip: String!
      }

      input AddressWhereUniqueInput {
        id: ID
      }
      """

  Scenario: Enhance typedef with bidirectional has many create input type
    Given a GraphQL schema definition
      """
      type User implements Node {
        id: ID! @unique
        name: String!
        email: String! @unique
        address: [Address!]!
      }

      type Address implements Node {
        id: ID! @unique
        street: String!
        city: String!
        state: String!
        zip: String!
        user: User
      }
      """
    When the schema is enhanced
    Then the enhanced schema should contain enhanced definitions
      """
      input UserCreateInput {
        name: String!
        email: String!
        address: AddressCreateManyWithoutUserInput
      }

      input AddressCreateManyWithoutUserInput {
        create: [AddressCreateWithoutUserInput!]
        connect: [AddressWhereUniqueInput!]
      }

      input AddressCreateWithoutUserInput {
        street: String!
        city: String!
        state: String!
        zip: String!
      }
      """

  Scenario: Enhance typedef with has many bidirectional update input type
    Given a GraphQL schema definition
      """
      type User implements Node {
        id: ID! @unique
        name: String!
        email: String! @unique
        address: [Address!]!
      }

      type Address implements Node {
        id: ID! @unique
        street: String!
        city: String!
        state: String!
        zip: String!
        user: User
      }
      """
    When the schema is enhanced
    Then the enhanced schema should contain enhanced definitions
      """
      input UserUpdateInput {
        name: String
        email: String
        address: AddressUpdateManyWithoutUserInput
      }

      input AddressUpdateManyWithoutUserInput {
        create: [AddressCreateWithoutUserInput!]
        connect: [AddressWhereUniqueInput!]
        disconnect: [AddressWhereUniqueInput!]
        delete: [AddressWhereUniqueInput!]
        update: [AddressUpdateWithoutUserDataInput!]
        upsert: [AddressUpsertWithoutUserNestedInput!]
      }

      input AddressCreateWithoutUserInput {
        street: String!
        city: String!
        state: String!
        zip: String!
      }

      input AddressUpdateWithoutUserDataInput {
        street: String
        city: String
        state: String
        zip: String
      }

      input AddressUpsertWithoutUserNestedInput {
        where: AddressWhereUniqueInput!
        update: AddressUpdateWithoutUserDataInput!
        create: AddressCreateWithoutUserInput!
      }

      input AddressWhereUniqueInput {
        id: ID
      }
      """

  Scenario: Enhance typedef with has many field filters
    Given a GraphQL schema definition
      """
      type User implements Node {
        id: ID! @unique
        name: String!
        email: String! @unique
        addresses: [Address!]!
      }

      type Address implements Node {
        id: ID! @unique
        street: String!
        city: String!
        state: String!
        zip: String!
      }
      """
    When the schema is enhanced
    Then the enhanced schema should contain enhanced definitions
      """
        type User implements Node {
          addresses(where: AddressWhereInput, orderBy: AddressOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Address!]
        }
      """
