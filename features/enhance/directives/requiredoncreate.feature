Feature: @requiredOnCreate Directive

  @requiredOnCreate field-level directive

  Scenario: Enhance schema with @requiredOnCreate directive
    Given a GraphQL schema definition
      """
      type User {
        id: ID! @unique
        email: String @unique @requiredOnCreate
      }      
      """
    When the schema is enhanced
    Then the enhanced schema should contain general definitions
      """
      type User implements Node {
        id: ID!
        email: String
        createdAt: DateTime
        updatedAt: DateTime
      }
      """
    And the enhanced schema should contain general definitions
      """
      input UserCreateInput {
        email: String!
      }
      """
    And the enhanced schema should contain general definitions
      """
      input UserUpdateInput {
        email: String
      }
      """
    And the enhanced schema should contain general definitions
      """
      type UserPreviousValues {
        id: ID!
        email: String
        createdAt: DateTime
        updatedAt: DateTime
      }
      """
