Feature: @writeOnly Directive

  @writeOnly field-level directive

  Scenario: Enhance schema with @writeOnly directive
    Given a GraphQL schema definition
      """
      type User {
        id: ID! @unique
        email: String! @unique
        password: String! @writeOnly
      }      
      """
    When the schema is enhanced
    Then the enhanced schema should contain general definitions
      """
      type User implements Node {
        id: ID!
        email: String!
        createdAt: DateTime
        updatedAt: DateTime
      }
      """
    And the enhanced schema should contain general definitions
      """
      input UserCreateInput {
        email: String!
        password: String!
      }
      """
    And the enhanced schema should contain general definitions
      """
      input UserUpdateInput {
        email: String
        password: String
      }
      """

    And the enhanced schema should contain general definitions
      """
      enum UserOrderByInput {
        id_ASC
        id_DESC
        email_ASC
        email_DESC
        createdAt_ASC
        createdAt_DESC
        updatedAt_ASC
        updatedAt_DESC
      }
      """
    And the enhanced schema should contain general definitions
      """
      type UserPreviousValues {
        id: ID!
        email: String!
        createdAt: DateTime
        updatedAt: DateTime
      }
      """
