Feature: Auth @loginRequired directive

  Protect GraphQL endpoints and fields by requiring login authentications

  Background:
    Given a GraphQL server with typedefs
      """
      type Post @loginRequired {
        id: ID! @unique
        title: String!
        contents: String!
        published: Boolean @default(value: false)
      }
      """

  Scenario: create method should return access denied when user not logged in
    When a GraphQL query is executed
      """
        mutation {
          createPost(data: { title: "Test", contents: "Empty" }) {
            id title contents published
          }
        }
      """
    Then the GraphQL query result should have error message "Login required"

  Scenario: create method should allow access denied when user logged in
    When the user is logged in
    And a GraphQL query is executed
      """
        mutation {
          createPost(data: { title: "Test", contents: "Empty" }) {
            title contents published
          }
        }
      """
    Then the GraphQL query should return
      """
      { "data":
        {
          "createPost": {
            "title": "Test", "contents": "Empty", "published": false
          }
        }
      }
      """

  Scenario: require login selectively for auto-gen resolvers
    Given a GraphQL server with typedefs
      """
      type Post
        @loginRequired(applyTo: ["createPost"])
      {
        id: ID! @unique
        title: String!
        contents: String!
        published: Boolean @default(value: false)
      }
      """
    And a GraphQL query is executed
      """
        mutation {
          createPost(data: { title: "Test", contents: "Empty" }) {
            title contents published
          }
        }
      """
    Then the GraphQL query result should have error message "Login required"

  Scenario: require login should only apply to resolvers specified
    Given a GraphQL server with typedefs
      """
      type Post
        @loginRequired(applyTo: ["updatePost"])
      {
        id: ID! @unique
        title: String!
        contents: String!
        published: Boolean @default(value: false)
      }
      """
    And a GraphQL query is executed
      """
        mutation {
          createPost(data: { title: "Test", contents: "Empty" }) {
            title contents published
          }
        }
      """
    Then the GraphQL query should return
      """
      { "data":
        {
          "createPost": {
            "title": "Test", "contents": "Empty", "published": false
          }
        }
      }
      """