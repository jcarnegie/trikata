Feature: Query Filters

  Be able to specify SQL-like where clause elements via GraphQL

  Background:
    Given a GraphQL server with typedefs
      """
      type User implements Node {
        id: ID! @unique
        name: String!
        email: String! @unique
        active: Boolean!
      }
      """
    And rows in the users table
      | id | name      | email        | active |
      | 1  | John Doe  | jd@acme.com  | false  |
      | 2  | Jane Doe  | jad@acme.com | false  |
      | 3  | Joe Doe   | jod@acme.com | true   |
      | 4  | Chris Doe | cd@acme.com  | true   |
      | 5  | Anne Doe  | ad@acme.com  | true   |
      | 6  | Peter Doe | pd@acme.com  | true   |

  Scenario: Where column equal
    When a GraphQL query is executed
      """
        query {
          users(where: { active: false }) {
            id name email active
          }
        }
      """
    Then the GraphQL query should return
      """
      { "data":
        {
          "users": [
            { "id": "1", "name": "John Doe", "email": "jd@acme.com", "active": false },
            { "id": "2", "name": "Jane Doe", "email": "jad@acme.com", "active": false }
          ]
        }
      }
      """

  Scenario: Where column not equal
    When a GraphQL query is executed
      """
        query {
          users(where: { active_not: false }) {
            id name email active
          }
        }
      """
    Then the GraphQL query should return
      """
      { "data":
        {
          "users": [
            { "id": "3", "name": "Joe Doe", "email": "jod@acme.com", "active": true },
            { "id": "4", "name": "Chris Doe", "email": "cd@acme.com", "active": true },
            { "id": "5", "name": "Anne Doe", "email": "ad@acme.com", "active": true },
            { "id": "6", "name": "Peter Doe", "email": "pd@acme.com", "active": true }
          ]
        }
      }
      """

  Scenario: Where column less than
    When a GraphQL query is executed
      """
        query {
          users(where: { id_lt: 3 }) {
            id name email active
          }
        }
      """
    Then the GraphQL query should return
      """
      { "data":
        {
          "users": [
            { "id": "1", "name": "John Doe", "email": "jd@acme.com", "active": false },
            { "id": "2", "name": "Jane Doe", "email": "jad@acme.com", "active": false }
          ]
        }
      }
      """
  
  Scenario: Where column less than or equal
    When a GraphQL query is executed
      """
        query {
          users(where: { id_lte: 3 }) {
            id name email active
          }
        }
      """
    Then the GraphQL query should return
      """
      { "data":
        {
          "users": [
            { "id": "1", "name": "John Doe", "email": "jd@acme.com", "active": false },
            { "id": "2", "name": "Jane Doe", "email": "jad@acme.com", "active": false },
            { "id": "3", "name": "Joe Doe", "email": "jod@acme.com", "active": true }
          ]
        }
      }
      """

  Scenario: Where column greater than
    When a GraphQL query is executed
      """
        query {
          users(where: { id_gt: 4 }) {
            id name email active
          }
        }
      """
    Then the GraphQL query should return
      """
      { "data":
        {
          "users": [
            { "id": "5", "name": "Anne Doe", "email": "ad@acme.com", "active": true },
            { "id": "6", "name": "Peter Doe", "email": "pd@acme.com", "active": true }
          ]
        }
      }
      """

  Scenario: Where column greater than equal
    When a GraphQL query is executed
      """
        query {
          users(where: { id_gte: 4 }) {
            id name email active
          }
        }
      """
    Then the GraphQL query should return
      """
      { "data":
        {
          "users": [
            { "id": "4", "name": "Chris Doe", "email": "cd@acme.com", "active": true },
            { "id": "5", "name": "Anne Doe", "email": "ad@acme.com", "active": true },
            { "id": "6", "name": "Peter Doe", "email": "pd@acme.com", "active": true }
          ]
        }
      }
      """

  Scenario: Where column contains
    When a GraphQL query is executed
      """
        query {
          users(where: { name_contains: "ane D" }) {
            id name email active
          }
        }
      """
    Then the GraphQL query should return
      """
      { "data":
        {
          "users": [
            { "id": "2", "name": "Jane Doe", "email": "jad@acme.com", "active": false }
          ]
        }
      }
      """


  Scenario: Where column does not contain
    When a GraphQL query is executed
      """
        query {
          users(where: { name_not_contains: "Chris" }) {
            id name email active
          }
        }
      """
    Then the GraphQL query should return
      """
      { "data":
        {
          "users": [
            { "id": "1", "name": "John Doe", "email": "jd@acme.com", "active": false },
            { "id": "2", "name": "Jane Doe", "email": "jad@acme.com", "active": false },
            { "id": "3", "name": "Joe Doe", "email": "jod@acme.com", "active": true },
            { "id": "5", "name": "Anne Doe", "email": "ad@acme.com", "active": true },
            { "id": "6", "name": "Peter Doe", "email": "pd@acme.com", "active": true }
          ]
        }
      }
      """

  Scenario: Where column starts with
    When a GraphQL query is executed
      """
        query {
          users(where: { email_starts_with: "j" }) {
            id name email active
          }
        }
      """
    Then the GraphQL query should return
      """
      { "data":
        {
          "users": [
            { "id": "1", "name": "John Doe", "email": "jd@acme.com", "active": false },
            { "id": "2", "name": "Jane Doe", "email": "jad@acme.com", "active": false },
            { "id": "3", "name": "Joe Doe", "email": "jod@acme.com", "active": true }
          ]
        }
      }
      """

  Scenario: Where column does not start with
    When a GraphQL query is executed
      """
        query {
          users(where: { email_not_starts_with: "j" }) {
            id name email active
          }
        }
      """
    Then the GraphQL query should return
      """
      { "data":
        {
          "users": [
            { "id": "4", "name": "Chris Doe", "email": "cd@acme.com", "active": true },
            { "id": "5", "name": "Anne Doe", "email": "ad@acme.com", "active": true },
            { "id": "6", "name": "Peter Doe", "email": "pd@acme.com", "active": true }
          ]
        }
      }
      """

  Scenario: Where column ends with
    When a GraphQL query is executed
      """
        query {
          users(where: { name_ends_with: "Doe" }) {
            id name email active
          }
        }
      """
    Then the GraphQL query should return
      """
      { "data":
        {
          "users": [
            { "id": "1", "name": "John Doe", "email": "jd@acme.com", "active": false },
            { "id": "2", "name": "Jane Doe", "email": "jad@acme.com", "active": false },
            { "id": "3", "name": "Joe Doe", "email": "jod@acme.com", "active": true },
            { "id": "4", "name": "Chris Doe", "email": "cd@acme.com", "active": true },
            { "id": "5", "name": "Anne Doe", "email": "ad@acme.com", "active": true },
            { "id": "6", "name": "Peter Doe", "email": "pd@acme.com", "active": true }
          ]
        }
      }
      """

  Scenario: Where column does not end with
    When a GraphQL query is executed
      """
        query {
          users(where: { name_not_ends_with: "Doe" }) {
            id name email active
          }
        }
      """
    Then the GraphQL query should return
      """
      { "data":
        {
          "users": []
        }
      }
      """

  Scenario: Where column in
    When a GraphQL query is executed
      """
        query {
          users(where: { email_in: ["cd@acme.com", "pd@acme.com"] }) {
            id name email active
          }
        }
      """
    Then the GraphQL query should return
      """
      { "data":
        {
          "users": [
            { "id": "4", "name": "Chris Doe", "email": "cd@acme.com", "active": true },
            { "id": "6", "name": "Peter Doe", "email": "pd@acme.com", "active": true }
          ]
        }
      }
      """

  Scenario: Where column not in
    When a GraphQL query is executed
      """
        query {
          users(where: { email_not_in: ["cd@acme.com", "pd@acme.com"] }) {
            id name email active
          }
        }
      """
    Then the GraphQL query should return
      """
      { "data":
        {
          "users": [
            { "id": "1", "name": "John Doe", "email": "jd@acme.com", "active": false },
            { "id": "2", "name": "Jane Doe", "email": "jad@acme.com", "active": false },
            { "id": "3", "name": "Joe Doe", "email": "jod@acme.com", "active": true },
            { "id": "5", "name": "Anne Doe", "email": "ad@acme.com", "active": true }
          ]
        }
      }
      """