import { FieldDefinitionNode, InputValueDefinitionNode } from 'graphql';
import {
    equals,
    map,
    path,
} from 'ramda';
import { whereAll } from '../util/object';

export type Argument = {
    name: string,
    type: string,
    nullable: boolean,
    isList: boolean,
    listItemsNullable: boolean,
};

const NULLABLE_LIST_TYPE_SPEC = { type: { kind: equals('ListType') } };
const NON_NULL_LIST_TYPE_SPEC = { type: { kind: equals('NonNullType'), type: { kind: equals('ListType') } } };
const NULLABLE_LIST_TYPE_NULLABLE_LIST_ITEMS_SPEC = { type: { kind: equals('ListType'), type: { kind: equals('NamedType') } } };
const NON_NULL_LIST_TYPE_NON_NULL_LIST_ITEMS_SPEC = { type: { kind: equals('NonNullType'), type: { kind: equals('ListType'), type: { kind: equals('NamedType') } } } };

export const fieldName = (arg: InputValueDefinitionNode): string => arg.name.value;

export const isFieldNonNullScalarType = (arg: InputValueDefinitionNode) => arg.type.kind === 'NonNullType';

export const isListType = (arg: InputValueDefinitionNode) => {
    // nullable list
    const case1 = whereAll(NULLABLE_LIST_TYPE_SPEC, arg);
    // non-null list
    const case2 = whereAll(NON_NULL_LIST_TYPE_SPEC, arg);
    return case1 || case2;
};

export const isListNullable = (arg: InputValueDefinitionNode): boolean =>
    whereAll(NULLABLE_LIST_TYPE_SPEC, arg);

export const listElementsNullable = (arg: InputValueDefinitionNode): boolean => {
    const case1 = whereAll(NULLABLE_LIST_TYPE_NULLABLE_LIST_ITEMS_SPEC, arg);
    const case2 = whereAll(NON_NULL_LIST_TYPE_NON_NULL_LIST_ITEMS_SPEC, arg);
    return case1 || case2;
};

export const fieldType = (arg: InputValueDefinitionNode): string => {
    let typePath = [];
    if (isListType(arg) && isListNullable(arg)) {
        if (listElementsNullable(arg)) {
            typePath = ['type', 'type', 'name', 'value'];
        } else {
            typePath = ['type', 'type', 'type', 'name', 'value'];
        }
    } else if (isListType(arg) && !isListNullable(arg)) {
        if (listElementsNullable(arg)) {
            typePath = ['type', 'type', 'type', 'name', 'value'];
        } else {
            typePath = ['type', 'type', 'type', 'type', 'name', 'value'];
        }
    } else if (isFieldNonNullScalarType(arg)) {
        typePath = ['type', 'type', 'name', 'value'];
    } else {
        typePath = ['type', 'name', 'value'];
    }
    return path(typePath, arg) || '';
};

export const createArgument = (arg: InputValueDefinitionNode) => ({
    name: fieldName(arg),
    type: fieldType(arg),
    nullable: !isFieldNonNullScalarType(arg),
    isList: isListType(arg),
    listItemsNullable: listElementsNullable(arg),
});

export const createArguments = (field: FieldDefinitionNode) => map(createArgument, field.arguments || []);

export const renderGQL = (arg: Argument) => {
    let argType = arg.nullable ? arg.type : `${arg.type}!`;
    argType = arg.isList ? `[${argType}]` : argType;
    argType = arg.isList && !arg.listItemsNullable ? `${argType}!` : argType;
    return `${arg.name}: ${argType}`;
};