// @flow

import { tableize, underscore } from 'inflection';
import {
  ascend,
  compose,
  concat,
  curry,
  filter,
  find,
  isNil,
  map,
  merge,
  not,
  path,
  prop,
  propEq,
  reduce,
  sort,
} from 'ramda';

import { ParseSchemaContext } from './context';
import { Schema } from './index';
import { Type } from './type';
import { Field } from './field';

// import { createIdField } from './field';
import { lowerFirstLetter } from '../util/string';

export type Relation = {
  name: string,
  relationType: string,
  direction: string,
  tableName: string,
  types: Type[],
  srcField: Field,
  srcType: Type,
  srcJoinColName: string,
  targetType: Type,
  targetJoinColName: string,
  bidirectionalField?: Field,
  renameTo?: string,
};

export const RELATION_TYPE_HAS_ONE = 'hasOne';
export const RELATION_TYPE_HAS_MANY = 'hasMany';
export const UNIDIRECTIONAL = 'unidirectional';
export const BIDIRECTIONAL = 'bidirectional';

const notNil = compose(not, isNil);

export const createRelationType = (relation: Relation): Type => {
  const srcType = relation.types[0];
  const dstType = relation.types[1];
  const srcColName = underscore(`${lowerFirstLetter(srcType.name)}Id`);
  const dstColName = underscore(`${lowerFirstLetter(dstType.name)}Id`);
  const type = {
    name: relation.name,
    tableName: relation.tableName,
    indexes: [],
    directives: [],
    fields: [],
  };
  const fields: Field[] = [{
    parentType: type,
    name: 'id',
    columnName: 'id',
    type: 'ID',
    isList: false,
    listItemsNullable: false,
    sqlType: 'varchar',
    size: 36,
    primaryKey: true,
    nullable: false,
    unique: false,
    default: null,
    isRelation: false,
    directives: [],
  }, {
    parentType: type,
    name: `${lowerFirstLetter(srcType.name)}Id`,
    columnName: underscore(`${lowerFirstLetter(srcType.name)}Id`),
    type: 'String',
    isList: false,
    listItemsNullable: false,
    sqlType: 'varchar',
    size: 36,
    primaryKey: false,
    nullable: false,
    unique: false,
    default: null,
    isRelation: false,
    directives: [],
  }, {
    parentType: type,
    name: `${lowerFirstLetter(dstType.name)}Id`,
    columnName: underscore(`${lowerFirstLetter(dstType.name)}Id`),
    type: 'String',
    isList: false,
    listItemsNullable: false,
    sqlType: 'varchar',
    size: 36,
    primaryKey: false,
    nullable: false,
    unique: false,
    default: null,
    isRelation: false,
    directives: [],
  }];
  const indexes = [{
    parentType: type,
    name: `${relation.tableName}_${srcColName}`,
    columns: [srcColName],
    type: 'btree',
    primary: false,
    unique: false,
  }, {
    parentType: type,
    name: `${relation.tableName}_${dstColName}`,
    columns: [dstColName],
    type: 'btree',
    primary: false,
    unique: false,
  }];

  return merge(type, { fields, indexes });
};

export const defaultName = (t1: Type, t2: Type) => {
  const names = [t1.name, t2.name].sort();
  return `${names[0]}To${names[1]}`;
};

export const createRelation = (
  context: ParseSchemaContext,
  field: Field,
  targetType: Type,
): Relation => {
  const names = [field.parentType.name, targetType.name].sort();
  const relationDirective = find(propEq('name', 'relation'), field.directives);
  const relationDirectiveName = <string>path(['args', 'name'], relationDirective);
  const renameTo = <string>path(['args', 'name'], relationDirective);
  const name = relationDirectiveName || defaultName(field.parentType, targetType);
  const tableName = tableize(`_${name}`);
  const byName = ascend(prop('name'));
  const types = sort(byName, [targetType, field.parentType]);
  return {
    name,
    tableName,
    types,
    renameTo,
    direction: UNIDIRECTIONAL,
    srcField: field,
    relationType: field.isList ? RELATION_TYPE_HAS_MANY : RELATION_TYPE_HAS_ONE,
    srcType: field.parentType,
    srcJoinColName: underscore(`${lowerFirstLetter(field.parentType.name)}Id`),
    targetType: targetType,
    targetJoinColName: underscore(`${lowerFirstLetter(targetType.name)}Id`),
  };
};

export const filterRelationFields = (types: Type[], type: Type) =>
  filter(f => notNil(find(propEq('name', f.type), types)), type.fields);

export const findBidirectionalField = (relation: Relation): Field | undefined =>
  find(f => f.type === relation.srcType.name, relation.targetType.fields);

export const relationFromField = curry((context: ParseSchemaContext, types: Type[], field: Field): Relation | null => {
  // Todo: fix. this is a hack. needs to be done in a functional way
  field.isRelation = true;
  const targetType = find(propEq('name', field.type), types);
  if (isNil(targetType)) return null;
  const relation = createRelation(context, field, targetType);
  relation.bidirectionalField = findBidirectionalField(relation);
  relation.direction = (relation.bidirectionalField) ? BIDIRECTIONAL : UNIDIRECTIONAL;
  return relation;
});

export const createRelations = (context: ParseSchemaContext, types: Type[]): Relation[] =>
  reduce((relations: Relation[], type): Relation[] => {
    const relationFields: Field[] = filterRelationFields(types, type);
    const extractedRelations = map(relationFromField(context, types), relationFields);
    const actualRelations = <Relation[]>filter(r => r !== null, extractedRelations);
    return concat(relations, actualRelations);
  }, [], types);

export const isHasOne = (relation: Relation) => relation.relationType === RELATION_TYPE_HAS_ONE;

export const isHasMany = (relation: Relation) => relation.relationType === RELATION_TYPE_HAS_MANY;

export const unidrectionalRelations = (relations: Relation[]): Relation[] =>
  filter(propEq('direction', UNIDIRECTIONAL), relations);

export const bidrectionalRelations = (relations: Relation[]): Relation[] =>
  filter(propEq('direction', BIDIRECTIONAL), relations);

export const hasOneRelations = (relations: Relation[]): Relation[] =>
  filter(propEq('relationType', RELATION_TYPE_HAS_ONE), relations);

export const hasManyRelations = (relations: Relation[]): Relation[] =>
  filter(propEq('relationType', RELATION_TYPE_HAS_MANY), relations);

export const hasOneUnidirectionalRelations = (relations: Relation[]): Relation[] =>
  filter(
    r => r.direction === UNIDIRECTIONAL && r.relationType === RELATION_TYPE_HAS_ONE,
    relations,
  );

export const hasManyUnidirectionalRelations = (relations: Relation[]): Relation[] =>
  filter(
    r => r.direction === UNIDIRECTIONAL && r.relationType === RELATION_TYPE_HAS_MANY,
    relations,
  );

export const hasOneBidirectionalRelations = (relations: Relation[]): Relation[] =>
  filter(
    r => r.direction === BIDIRECTIONAL && r.relationType === RELATION_TYPE_HAS_ONE,
    relations,
  );

export const hasManyBidirectionalRelations = (relations: Relation[]): Relation[] =>
  filter(
    r => r.direction === BIDIRECTIONAL && r.relationType === RELATION_TYPE_HAS_MANY,
    relations,
  );

export const findRelationByFieldName = (
  schema: Schema,
  type: Type,
  fieldName: string,
): Relation => {
  const f = find(propEq('name', fieldName), type.fields);
  if (!f) throw new Error(`Couldn't find field ${type.name}.${fieldName}`);
  const targetType = find(propEq('name', f.type), schema.types);
  if (!targetType) throw new Error(`Can't find target type ${f.type}`);
  const relationMatcher = (r: Relation) =>
    r.srcType.name === f.parentType.name &&
    r.targetType.name === targetType.name;
  const relation = find(relationMatcher, schema.relations);
  if (!relation) throw new Error(`Can't find relation for field ${type.name}.${f.name}`);
  return relation;
};

// export const isValid = (schema: Schema) => {

// };
