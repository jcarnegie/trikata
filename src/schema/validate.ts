import isEmail from 'validator/lib/isEmail';
import { parsePhoneNumberFromString } from 'libphonenumber-js';
import { find, path, propEq } from 'ramda';
import { Type } from './type';
import { Field, hasDirective } from './field';
import { curry, map } from 'ramda';

const isPhone = (phone: string) => {
  const phoneNumber = parsePhoneNumberFromString(phone);
  return phoneNumber && phoneNumber.isValid();
};

const validateField = curry((data: any, field: Field) => {
  const directive = find(propEq('name', 'validate'), field.directives || []);
  if (!directive) return;

  const validateFormat = path(['args', 'format'], directive);
  if (validateFormat === 'email' && data[field.name] && !isEmail(data[field.name])) throw new Error(`${field.parentType.name}.${field.name} is not a valid email address`);
  if (validateFormat === 'phone' && data[field.name] && !isPhone(data[field.name])) throw new Error(`${field.parentType.name}.${field.name} is not a valid phone number`);
});

export const validate = (type: Type, data: any) => {
  const { fields } = type;
  map(validateField(data), fields);
  return true;
};