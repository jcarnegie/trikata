import { join as joinPath, relative } from 'path';
import F, { FutureInstance, map, parallel } from 'fluture';
// import { map } from 'ramda';
import S from '../util/sanctuary';
import { GraphQLScalarType } from 'graphql';
import { Directory, safeDirectory } from '../util/fs';

export type Scalar = {
    name: string,
    resolver: GraphQLScalarType,
};

// createScalarFromCode :: string -> string -> Scalar
const createScalarFromCode = S.curry2((dir: string, file: string): Scalar => ({
    name: file,
    resolver: <GraphQLScalarType>require(joinPath(relative(__dirname, dir), file)),
}));

// loadScalarTypes :: string -> FutureInstance<Error, Scalar[]>
export const loadScalarTypes = (
  dirs: string[]
): FutureInstance<{}, Scalar[]> => {
  const readdirFutures: FutureInstance<{}, Directory>[] =
    S.map(safeDirectory)(dirs);

  const createScalars = (dir: Directory): Scalar[] =>
    S.map(createScalarFromCode(dir.path))(dir.files);

  const readdirResults: FutureInstance<{}, Directory[]> =
    <FutureInstance<{}, Directory[]>>parallel(dirs.length)(readdirFutures);

  const mapFn = (results: Directory[]): Scalar[] =>
    S.join(S.map(createScalars)(results));

  return map(mapFn)(readdirResults);
};
