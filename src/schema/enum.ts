// @flow

import { EnumTypeDefinitionNode } from 'graphql/language';

import { clone, map, path, pathOr } from 'ramda';

export type Enum = {
  name: string,
  values: string[]
};

export const createEnumFromEnumTypeDefinition = (enumTypedef: EnumTypeDefinitionNode): Enum => {
  const clonedEnum = clone(enumTypedef);
  const name = pathOr([], ['name', 'value'], clonedEnum);
  const values = map(v => v.name.value, clonedEnum.values || []);
  return { name, values };
};
