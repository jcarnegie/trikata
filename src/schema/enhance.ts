// @flow
import { assoc, concat, contains, curry, filter, find, isNil, join, keys, map, mergeAll, path, propEq, reduce } from 'ramda';
import { camelize, capitalize, pluralize } from 'inflection';
import { Schema } from '.';
import { ParseSchemaContext } from './context';
import { Type, readableFields, readableScalarFields, isTypedJsonField } from './type';
import { Field, renderFieldGQL, hasDirective, isEnumType } from './field';
import { Relation } from './relation';
import { parseTypedefs } from '.';
import {
  RELATION_TYPE_HAS_ONE,
  RELATION_TYPE_HAS_MANY,
  BIDIRECTIONAL,
  UNIDIRECTIONAL,
  hasOneUnidirectionalRelations,
  hasOneBidirectionalRelations,
  hasManyUnidirectionalRelations,
  hasManyBidirectionalRelations,
  unidrectionalRelations,
} from './relation';
import { renderTypedef, renderStatelessTypedef } from './enhance/typedef';
import { renderEnum } from './enhance/enum';

/**
 * Given a typedef:
 *
 * type User {
 *   id: ID
 *   email: String
 *
 * }
 */

const BATCH_PAYLOAD_TYPEDEF = `
type BatchPayload {
  count: Long!
}
`;

const UPLOAD_SCALAR = `scalar Upload`;

const MUTATION_TYPE_TYPEDEF = `
enum MutationType {
  CREATED
  UPDATED
  DELETED
}
`;

const NODE_TYPEDEF = `
interface Node {
  id: ID!
}
`;

const PAGE_INFO_TYPEDEF = `
type PageInfo {
  hasNextPage: Boolean!
  hasPreviousPage: Boolean!
  startCursor: String
  endCursor: String
}
`;

const formatTypeField = (schema: Schema, field: Field, inputType: string = '', options: any = {}): string => {
  const {
    name,
    type,
    nullable,
    isRelation,
  } = field;
  // special case for nullable - if this is a create input, and the
  // @requiredOnCreate directive is applied, set the field to non-nullable
  const isNullable = (inputType === 'Create' && hasDirective('requiredOnCreate', field))
    ? false
    : nullable || options.forceNullable;

  if (isRelation) {
    const relation = find(
      r =>
        (r.srcField.name === field.name &&
        r.srcField.type === field.type &&
        r.srcType.name === field.parentType.name),
      schema.relations,
    );

    if (!relation) throw new Error(`Can't find relation for field ${field.parentType.name}.${field.name}`);

    if (relation.relationType === RELATION_TYPE_HAS_ONE) {
      if (relation.direction === UNIDIRECTIONAL) {
        const uniHasOneInputTypeName = `${relation.targetType.name}${inputType}OneInput`;
        return `${name}: ${uniHasOneInputTypeName}${isNullable ? '' : '!'}`;
      } else if (relation.direction === BIDIRECTIONAL) {
        const bidirectionalFieldName = capitalize(path(['bidirectionalField', 'name'], relation) || '');
        const biHasOneInputTypeName = `${relation.targetType.name}${inputType}OneWithout${bidirectionalFieldName}Input`;
        return `${name}: ${biHasOneInputTypeName}${isNullable ? '' : '!'}`;
      }
    }

    if (relation.relationType === RELATION_TYPE_HAS_MANY) {
      if (relation.direction === UNIDIRECTIONAL) {
        const uniHasManyInputTypeName = `${relation.targetType.name}${inputType}ManyInput`;
        return `${name}: ${uniHasManyInputTypeName}`;
      } else if (relation.direction === BIDIRECTIONAL) {
        const bidirectionalFieldName = capitalize(path(['bidirectionalField', 'name'], relation) || '');
        const biHasManyInputTypeName = `${relation.targetType.name}${inputType}ManyWithout${bidirectionalFieldName}Input`;
        return `${name}: ${biHasManyInputTypeName}`;
      }
    }

    return `${name}: ${isRelation ? `${type}${inputType}Input` : type}${isNullable ? '' : '!'}`;
  }

  // if (field.sqlType === 'jsonb' && field.type !== 'JSON' && !field.isScalarType) {
  if (field.type !== 'JSON' && !field.isScalarType && !isEnumType(schema, field)) {
    return (inputType)
      ? field.isList
        ? field.listItemsNullable
          ? `${name}: [${type}${inputType}Input]${isNullable ? '' : '!'}`
          : `${name}: [${type}${inputType}Input!]${isNullable ? '' : '!'}`
        : `${name}: ${type}${inputType}Input${isNullable ? '' : '!'}`
      : field.isList
        ? field.listItemsNullable
          ? `${name}: [${type}]${isNullable ? '' : '!'}`
          : `${name}: [${type}!]${isNullable ? '' : '!'}`
        : `${name}: ${type}${isNullable ? '' : '!'}`;
  }

  return field.isList
    ? field.listItemsNullable
      ? `${name}: [${type}]${isNullable ? '' : '!'}`
      : `${name}: [${type}!]${isNullable ? '' : '!'}`
    : `${name}: ${type}${isNullable ? '' : '!'}`;
};

const typeCreateInput = curry((schema: Schema, type: Type): string => {
  const typeName = type.name;
  const sFields = filter(f => !contains(f.name, ['id', 'createdAt', 'updatedAt']), type.fields);
  const formattedScalarFields = map(f => `  ${formatTypeField(schema, f, 'Create')}`, sFields);
  return `
input ${typeName}CreateInput {
${join('\n', formattedScalarFields)}
}
`;
});

const typeHasOneUnidirectionalCreateInput = (relation: Relation): string => {
  const { name } = relation.targetType;
  return `
input ${name}CreateOneInput {
  create: ${name}CreateInput
  connect: ${name}WhereUniqueInput
}
`;
};

const typeHasManyUnidirectionalCreateInput = (relation: Relation): string => {
  const { name } = relation.targetType;
  return `
input ${name}CreateManyInput {
  create: [${name}CreateInput!]
  connect: [${name}WhereUniqueInput!]
}
`;
};

const typeHasOneBidirectionalCreateInput = curry((schema: Schema, relation: Relation): string => {
  const { name } = relation.targetType;
  const bidirectionalFieldName = capitalize(path(['bidirectionalField', 'name'], relation) || '');
  const sFields = filter(f => f.name !== 'id', relation.targetType.fields);
  const formattedScalarFields = map(f => `  ${formatTypeField(schema, f, 'Create')}`, sFields);
  return `
input ${name}CreateOneWithout${bidirectionalFieldName}Input {
  create: ${name}CreateWithout${bidirectionalFieldName}Input
  connect: ${name}WhereUniqueInput
}

input ${name}CreateWithout${bidirectionalFieldName}Input {
${join('\n', formattedScalarFields)}
}
`;
});

const typeHasManyBidirectionalCreateInput = curry((schema: Schema, relation: Relation): string => {
  const { name } = relation.targetType;
  const bidirectionalFieldName = capitalize(path(['bidirectionalField', 'name'], relation) || '');
  const sFields = filter(f => f.name !== 'id', relation.targetType.fields);
  const formattedScalarFields = map(f => `  ${formatTypeField(schema, f, 'Create')}`, sFields);
  return `
input ${name}CreateManyWithout${bidirectionalFieldName}Input {
  create: [${name}CreateWithout${bidirectionalFieldName}Input!]
  connect: [${name}WhereUniqueInput!]
}

input ${name}CreateWithout${bidirectionalFieldName}Input {
${join('\n', formattedScalarFields)}
}
`;
});

const typeUpdateInput = curry((schema: Schema, type: Type): string => {
  const typeName = type.name;
  const fields = filter(f => !contains(f.name, ['id', 'createdAt', 'updatedAt']), type.fields);
  const opts = { forceNullable: true };
  const formattedFields = map(f => `  ${formatTypeField(schema, f, 'Update', opts)}`, fields);
  return `
input ${typeName}UpdateInput {
${join('\n', formattedFields)}
}
`;
});

const typeHasOneUnidirectionalUpdateInput = (relation: Relation): string => {
  const { name } = relation.targetType;
  return `
input ${name}UpdateOneInput {
  create: ${name}CreateInput
  connect: ${name}WhereUniqueInput
  disconnect: Boolean
  delete: Boolean
  update: ${name}UpdateDataInput
  upsert: ${name}UpsertNestedInput
}
`;
};

const typeHasManyUnidirectionalUpdateInput = (relation: Relation): string => {
  const { name } = relation.targetType;
  return `
input ${name}UpdateManyInput {
  create: [${name}CreateInput!]
  connect: [${name}WhereUniqueInput!]
  disconnect: [${name}WhereUniqueInput!]
  delete: [${name}WhereUniqueInput!]
  update: [${name}UpdateWithWhereUniqueDataInput!]
  upsert: [${name}UpsertWithWhereUniqueNestedInput!]
}

input ${name}UpdateWithWhereUniqueDataInput {
  where: ${name}WhereUniqueInput!
  data: ${name}UpdateDataInput!
}

input ${name}UpsertWithWhereUniqueNestedInput {
  where: ${name}WhereUniqueInput!
  update: ${name}UpdateDataInput!
  create: ${name}CreateInput!
}
`;
};

const typeHasOneBidirectionalUpdateInput = curry((schema: Schema, relation: Relation): string => {
  const { name } = relation.targetType;
  const bidirectionalFieldName = capitalize(path(['bidirectionalField', 'name'], relation) || '');
  const fields = filter(f => f.name !== 'id', relation.targetType.fields);
  const opts = { forceNullable: true };
  const formattedFields = map(f => `  ${formatTypeField(schema, f, 'Update', opts)}`, fields);
  return `
input ${name}UpdateOneWithout${bidirectionalFieldName}Input {
  create: ${name}CreateWithout${bidirectionalFieldName}Input
  connect: ${name}WhereUniqueInput
  disconnect: Boolean
  delete: Boolean
  update: ${name}UpdateWithout${bidirectionalFieldName}DataInput
  upsert: ${name}UpsertWithout${bidirectionalFieldName}NestedInput
}

input ${name}UpdateWithout${bidirectionalFieldName}DataInput {
${join('\n', formattedFields)}
}

input ${name}UpsertWithout${bidirectionalFieldName}NestedInput {
  update: ${name}UpdateWithout${bidirectionalFieldName}DataInput!
  create: ${name}CreateWithout${bidirectionalFieldName}Input!
}
`;
});

const typeHasManyBidirectionalUpdateInput = curry((schema: Schema, relation: Relation): string => {
  const { name } = relation.targetType;
  const bidirectionalFieldName = capitalize(path(['bidirectionalField', 'name'], relation) || '');
  const fields = filter(f => f.name !== 'id', relation.targetType.fields);
  const opts = { forceNullable: true };
  const formattedFields = map(f => `  ${formatTypeField(schema, f, 'Update', opts)}`, fields);
  return `
input ${name}UpdateManyWithout${bidirectionalFieldName}Input {
  create: [${name}CreateWithout${bidirectionalFieldName}Input!]
  connect: [${name}WhereUniqueInput!]
  disconnect: [${name}WhereUniqueInput!]
  delete: [${name}WhereUniqueInput!]
  update: [${name}UpdateWithout${bidirectionalFieldName}DataInput!]
  upsert: [${name}UpsertWithout${bidirectionalFieldName}NestedInput!]
}

input ${name}UpdateWithout${bidirectionalFieldName}DataInput {
${join('\n', formattedFields)}
}

input ${name}UpsertWithout${bidirectionalFieldName}NestedInput {
  where: ${name}WhereUniqueInput!
  update: ${name}UpdateWithout${bidirectionalFieldName}DataInput!
  create: ${name}CreateWithout${bidirectionalFieldName}Input!
}
`;
});

const typeUpdateDataInput = curry((schema: Schema, relation: Relation): string => {
  const type = relation.targetType;
  const typeName = type.name;
  const fields = filter(f => f.name !== 'id', type.fields);
  const opts = { forceNullable: true };
  const formattedFields = map(f => `  ${formatTypeField(schema, f, 'Update', opts)}`, fields);
  return `
input ${typeName}UpdateDataInput {
${join('\n', formattedFields)}
}
`;
});

const typeUpsertNestedInput = (relation: Relation): string => {
  const { name } = relation.targetType;
  return `
input ${name}UpsertNestedInput {
  update: ${name}UpdateDataInput!
  create: ${name}CreateInput!
}
`;
};

const typeWhereInput = curry((schema: Schema, type: Type): string => {
  const queryableFields = filter(f => !isTypedJsonField(schema, f), readableScalarFields(type));
  const opData: any = reduce((operators: any, field) => {
    const scalarOps = ['not', 'lt', 'lte', 'gt', 'gte', 'contains', 'not_contains', 'starts_with', 'not_starts_with', 'ends_with', 'not_ends_with'];
    // these are array ops since they take arrays instead of scalar values
    const arrayOps = ['in', 'not_in'];

    const result = mergeAll([
      operators,
      { [field.name]: field.type },
      reduce((ops, op) => assoc(`${field.name}_${op}`, field.type, ops), {}, scalarOps),
      reduce((ops, op) => assoc(`${field.name}_${op}`, `[${field.type}!]`, ops), {}, arrayOps),
    ]);

    return result;
  }, {}, queryableFields);
  opData.AND = `[${type.name}WhereInput!]`;
  opData.OR = `[${type.name}WhereInput!]`;
  // eslint-disable-next-line no-return-assign
  const opString = reduce((str, key) => (str += `  ${key.toString()}: ${opData[key]}\n`), '', keys(opData).sort());
  return `
input ${type.name}WhereInput {
${opString}
}
`;
});

const typeWhereUniqueInput = (type: Type): string => {
  const uniqueFields = filter(f => !isNil(find(propEq('name', 'unique'), f.directives)) && f.name !== 'id', type.fields);
  const uniqueFieldsWithTypes = map(f => `  ${f.name}: ${f.type}`, uniqueFields);
  return `
input ${type.name}WhereUniqueInput {
  id: ID
${join('\n', uniqueFieldsWithTypes)}
}`;
};

const typeOrderByInput = curry((schema: Schema, type: Type): string => {
  const orderableFields = filter(f => !isTypedJsonField(schema, f), readableFields(type));
  const orderByFields = map(f => `  ${f.name}_ASC\n  ${f.name}_DESC\n`, orderableFields);
  return `
enum ${type.name}OrderByInput {
${join('', orderByFields)}
}
`;
});

const typePreviousValues = curry((schema: Schema, type: Type): string => {
  const formattedFields = map(f => `  ${formatTypeField(schema, f)}`, readableScalarFields(type));
  return `
type ${type.name}PreviousValues {
  ${join('\n  ', formattedFields)}
}
`;
});

const typeEdge = (type: Type): string => `
type ${type.name}Edge {
  node: ${type.name}!
  cursor: String!
}
`;

const typeSubscriptionPayload = (type: Type): string => `
type ${type.name}SubscriptionPayload {
  mutation: MutationType!
  node: ${type.name}
  updatedFields: [String!]
  previousValues: ${type.name}PreviousValues
}
`;

const typeSubscriptionWhereInput = (type: Type): string => `
input ${type.name}SubscriptionWhereInput {
  AND: [${type.name}SubscriptionWhereInput!]
  OR: [${type.name}SubscriptionWhereInput!]
  mutation_in: [MutationType!]
  updatedFields_contains: String
  updatedFields_contains_every: [String!]
  updatedFields_contains_some: [String!]
  node: ${type.name}WhereInput
}
`;

const typeConnection = (type: Type): string => `
type ${type.name}Connection {
  pageInfo: PageInfo!
  edges: [${type.name}Edge]!
}
`;

const typeSubscription = (type: Type): string =>
  `${camelize(type.name, true)}(where: ${type.name}SubscriptionWhereInput): ${type.name}SubscriptionPayload`;

  
export const enhance = (context: ParseSchemaContext, typedefs: string): string => {
  const schema: Schema = parseTypedefs(context, typedefs);

  const existingQueryFields = schema.queryType
    ? map(renderFieldGQL, schema.queryType.fields)
    : [];

  const typeQueries = reduce((methods: string[], type): string[] =>
    concat(methods, [
      `${camelize(type.name, true)}(where: ${type.name}WhereUniqueInput!): ${type.name}`,
      `${pluralize(camelize(type.name, true))}(where: ${type.name}WhereInput, orderBy: ${type.name}OrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [${type.name}]!`,
      `${pluralize(camelize(type.name, true))}Connection(where: ${type.name}WhereInput, orderBy: ${type.name}OrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): ${type.name}Connection!`,
    ]), existingQueryFields, schema.types);
  
  const queries = `
type Query {
  ${join('\n  ', typeQueries)}
  node(id: ID!): Node
}
  `;

  const existingMutationFields = schema.mutationType
    ? map(renderFieldGQL, schema.mutationType.fields)
    : [];

  const typeMutations = reduce((methods: string[], type) =>
    concat(methods, [
      `create${type.name}(data: ${type.name}CreateInput!): ${type.name}!`,
      `update${type.name}(data: ${type.name}UpdateInput!, where: ${type.name}WhereUniqueInput!): ${type.name}`,
      `delete${type.name}(where: ${type.name}WhereUniqueInput!): ${type.name}`,
      `upsert${type.name}(where: ${type.name}WhereUniqueInput!, create: ${type.name}CreateInput!, update: ${type.name}UpdateInput!): ${type.name}!`,
      `updateMany${pluralize(type.name)}(data: ${type.name}UpdateInput!, where: ${type.name}WhereInput!): BatchPayload!`,
      `deleteMany${pluralize(type.name)}(where: ${type.name}WhereInput!): BatchPayload!`,
    ]), existingMutationFields, schema.types);

  const mutations = `
type Mutation {
  ${join('\n  ', typeMutations)}
}
  `;
  const typeSubscriptions = map(typeSubscription, schema.types);
  const subscriptions = `
type Subscription {
  ${join('\n  ', typeSubscriptions)}
}
  `;
  return `
${BATCH_PAYLOAD_TYPEDEF}
${MUTATION_TYPE_TYPEDEF}
${UPLOAD_SCALAR}
${NODE_TYPEDEF}
${PAGE_INFO_TYPEDEF}
${join('\n', map(s => `scalar ${s.name}`, context.scalars))}
${join('\n', map(renderEnum, schema.enums))}
${join('\n', map(renderStatelessTypedef(schema), schema.statelessTypes))}
${join('\n', map(renderTypedef(schema), schema.types))}
${join('\n', map(typeCreateInput(schema), schema.statelessTypes))}
${join('\n', map(typeCreateInput(schema), schema.types))}
${join('\n', map(typeHasOneUnidirectionalCreateInput, hasOneUnidirectionalRelations(schema.relations)))}
${join('\n', map(typeHasManyUnidirectionalCreateInput, hasManyUnidirectionalRelations(schema.relations)))}
${join('\n', map(typeHasOneBidirectionalCreateInput(schema), hasOneBidirectionalRelations(schema.relations)))}
${join('\n', map(typeHasManyBidirectionalCreateInput(schema), hasManyBidirectionalRelations(schema.relations)))}
${join('\n', map(typeWhereInput(schema), schema.types))}
${join('\n', map(typeUpdateInput(schema), schema.statelessTypes))}
${join('\n', map(typeUpdateInput(schema), schema.types))}
${join('\n', map(typeUpdateDataInput(schema), unidrectionalRelations(schema.relations)))}
${join('\n', map(typeHasOneUnidirectionalUpdateInput, hasOneUnidirectionalRelations(schema.relations)))}
${join('\n', map(typeHasManyUnidirectionalUpdateInput, hasManyUnidirectionalRelations(schema.relations)))}
${join('\n', map(typeHasOneBidirectionalUpdateInput(schema), hasOneBidirectionalRelations(schema.relations)))}
${join('\n', map(typeHasManyBidirectionalUpdateInput(schema), hasManyBidirectionalRelations(schema.relations)))}
${join('\n', map(typeUpsertNestedInput, hasOneUnidirectionalRelations(schema.relations)))}
${join('\n', map(typeWhereUniqueInput, schema.types))}
${join('\n', map(typeOrderByInput(schema), schema.types))}
${join('\n', map(typePreviousValues(schema), schema.types))}
${join('\n', map(typeEdge, schema.types))}
${join('\n', map(typeSubscriptionPayload, schema.types))}
${join('\n', map(typeSubscriptionWhereInput, schema.types))}
${join('\n', map(typeConnection, schema.types))}
${queries}
${mutations}
${subscriptions}
`;
};
