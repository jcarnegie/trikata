// @flow
import {
    clone,
    compose,
    find,
    isEmpty,
    isNil,
    last as listLast,
    length,
    map as rMap,
    match,
    mergeAll,
    not,
    pick,
    propEq,
    reduce,
    values
} from 'ramda';
import { camelize, pluralize, underscore } from 'inflection';
import { createBatchResolver } from 'graphql-resolve-batch';
import { FutureInstance, chain, go, map, resolve, parallel } from 'fluture';
import { basename, extname, join as joinPath, relative } from 'path';

import S from "../util/sanctuary";
import { safeDirectory, safeReaddir } from '../util/fs';
import { Schema } from '../schema';
import { Type, readableScalarFields } from '../schema/type';
import { Field } from '../schema/field';
import { Relation, relationFromField } from '../schema/relation';
import { ParseSchemaContext, RuntimeContext } from '../schema/context';
import { parseTypedefs } from '../schema';
import { relationFields } from '../schema/field';
import { cryptHook } from '../schema/hooks';
import { log } from '../util/logging';
import { merge } from '../util/object';
import { Directory, directory } from '../util/fs';
import { lowerFirstLetter } from '../util/string';
import { type } from 'os';

export type ResolverFn = (
  source: any,
  args: any,
  context: any,
  info?: any
) => Promise<any>;

export type ResolverFnMap = { [key: string]: ResolverFn };

const notNil = compose(not, isNil);

export const makeFindOneResolver = (type: Type) =>
    (object: any, args: any, context: any) => {
        log.info({
          resolver: 'findOne',
          type: type.name,
          args,
        });
        const { db } = context;
        const { where } = args;
        return db.findOne(type, where);
    };

export const makeFindOneInRelationResolver = (
    srcField: Field,
    targetType: Type,
    relation: Relation,
) => createBatchResolver(async (sources: any, args: any, context: any) => {
    log.info({
      resolver: 'findOneInRelation',
      type: type.name,
      relation: relation.name,
      sources,
      args,
    });
    const { db } = context;
    const srcColName = underscore(`${lowerFirstLetter(relation.srcType.name)}Id`);
    const srcIds = rMap(src => src[srcColName] || src.id, sources);
    return db.findOneThrough(relation, srcIds);
});

export const makeFindManyInRelationResolver = (
    srcField: Field,
    targetType: Type,
    relation: Relation,
) => createBatchResolver(async (sources: any, args: any, context: any) => {
    log.info({
        resolver: 'findManyInRelation',
        type: type.name,
        relation: relation.name,
        sources,
        args,
    });
    const { db } = context;
    const srcColName = underscore(`${lowerFirstLetter(relation.srcType.name)}Id`);
    const srcIds = rMap(src => src[srcColName] || src.id, sources);
    const {
        where,
        orderBy,
        skip,
        after,
        before,
        first,
        last,
    } = args;
    return db.findThrough(relation, srcIds, where, orderBy, skip, after, before, first, last);
});

export const makeFindResolver = (type: Type) =>
    (object: any, args: any, context: any) => {
        log.info({
          resolver: 'find',
          type: type.name,
          args
        });
        const { db } = context;
        const {
            where,
            orderBy,
            skip,
            after,
            before,
            first,
            last,
        } = args;
        return db.find(type, where, orderBy, skip, after, before, first, last);
    };

export const makeCreateResolver = (type: Type) => {
    return async (object: any, args: any, context: any, info: any) => {
        log.info({
          resolver: 'create',
          type: type.name,
          args
        });
        const createResolver = (object: any, args: any, context: any, info: any) => {
            const { db } = context;
            const { data } = args;
            return db.insert(type, data);
        };
        const resolvers = [cryptHook(type), createResolver];
        return await reduce(
            async (p, resolver) => {
                await p;
                return resolver(object, args, context, info);
            },
            Promise.resolve(),
            resolvers
        );
    };
};

export const makeUpdateResolver = (type: Type) =>
    (object: any, args: any, context: any) => {
        log.info({
          resolver: 'update',
          type: type.name,
          args
        });
        const { db } = context;
        const { data, where } = args;
        return db.update(type, data, where);
    };

export const makeUpsertResolver = (type: Type) =>
    (object: any, args: any, context: any) => {
        log.info({
          resolver: 'upsert',
          type: type.name,
          args
        });
        const { db } = context;
        const { where, create, update } = args;
        return db.upsert(type, where, create, update);
    };

export const makeDeleteResolver = (type: Type) =>
    (object: any, args: any, context: any) => {
        log.info({
          resolver: 'delete',
          type: type.name,
          args
        });
        const { db } = context;
        const { where } = args;
        return db.del(type, where);
    };

export const makeUpdateManyResolver = (type: Type) =>
    (object: any, args: any, context: any) => {
        log.info({
          resolver: 'updateMany',
          type: type.name,
          args
        });
        const { db } = context;
        const { data, where } = args;
        return db.updateMany(type, data, where);
    };

export const makeDelManyResolver = (type: Type) =>
    (object: any, args: any, context: any) => {
        log.info({
          resolver: 'deleteMany',
          type: type.name,
          args
        });
        const { db } = context;
        const { where } = args;
        return db.delMany(type, where);
    };

const makeRelationFieldResolver = (schema: Schema, type: Type, field: Field): any => {
    const targetType = find(propEq('name', field.type), schema.types);
    if (!targetType) throw new Error(`Couldn't find relation target type for field ${type.name}.${field.name}`);
    const relationMatcher = (r: Relation) =>
        r.srcType.name === field.parentType.name &&
        r.targetType.name === targetType.name;
    const relation = find(relationMatcher, schema.relations);
    if (!relation) throw new Error(`Couldn't find relation for ${type.name}.${field.name} --> ${targetType.name}`);
    if (field.isList) {
        return makeFindManyInRelationResolver(field, targetType, relation);
    }
    return makeFindOneInRelationResolver(field, targetType, relation);
};

const makeFindConnectionResolver = (schema: Schema, type: Type): any =>
    async (object: any, args: any, context: any) => {
        const { db } = context;
        const {
            where,
            orderBy,
            skip,
            after,
            before,
            first,
            last,
        } = args;
        try {
            const edges = await db.find(type, where, orderBy, skip, after, before, first, last);
            let count = 0;
            if (first || last) {
                count = await db.count(type, where, orderBy, skip, after, before, first, last);
            }
            const data = {
                pageInfo: {
                    hasNextPage: notNil(first) && count > length(edges),
                    hasPreviousPage: notNil(last) && count > length(edges),
                    startCursor: (edges[0] ? edges[0].id : null),
                    endCursor: (edges[edges.length - 1] ? edges[edges.length - 1].id : null),
                },
                edges: rMap(e => ({ cursor: e.id, node: e }), edges),
            };
            return data;
        } catch (e) {
            console.log('error:', e);
        }
    };

// Todo: this function is getting pretty ugly. Need to refactor and simplify
export const generate = (
    parseContext: ParseSchemaContext,
    runtimeContext: RuntimeContext,
    typedefs: string,
): any => {
    const schema: Schema = parseTypedefs(parseContext, typedefs);
    const scalarResolvers = mergeAll(rMap(s => ({ [s.name]: s.resolver }), parseContext.scalars));
    const reduceFn = (resolvers: any, type: Type) => {
      // for handling type directives
      type RuntimeDirective = {
        directive: any;
        resolver: any;
      };

      const runtimeTypeDirectives = rMap((directive: any) => {
        const defaultResolver = async () => {};
        const resolver =
          parseContext.directiveResolvers[directive.name] ||
          defaultResolver;
        return { directive, resolver };
      }, type.directives);

      const applyTypeDirectives = (context: any, originalResolver: any) => {
        return reduce(
          (composedResolver: any, runtimeDirective: any) =>
            runtimeDirective.resolver(
              { ...context, directive: runtimeDirective.directive },
              composedResolver
            ),
          originalResolver,
          runtimeTypeDirectives
        );
      };

      // for handling field-level directives
      const runtimeFieldDirectives = (field: Field) =>
        rMap((directive: any) => {
          const defaultResolver = async () => {};
          const resolver =
            parseContext.directiveResolvers[directive.name] ||
            defaultResolver;
          return { directive, resolver };
        }, field.directives);

      // const fieldDirectiveNames = (field: Field) => rMap(d => d.name, field.directives);
      // const fieldDirectiveResolvers = (field: Field) => values(pick(fieldDirectiveNames(field), parseContext.directiveResolvers));
      const applyFieldDirectives = (
        field: Field,
        context: any,
        originalResolver: any
      ) =>
        reduce(
          (composedResolver, runtimeDirective) =>
            runtimeDirective.resolver(
              { ...context, directive: runtimeDirective.directive },
              composedResolver
            ),
          originalResolver,
          runtimeFieldDirectives(field)
        );

      const fieldResolvers = reduce(
        (resolvers: any, field: Field) => {
          const resolver = (source: any) => source[field.name];
          const context = {
            typeName: field.parentType.name,
            fieldName: field.name,
            type: field.parentType,
            field
          };
          const fieldResolver = {
            [field.name]: applyFieldDirectives(field, context, resolver)
          };
          return merge(resolvers, fieldResolver);
        },
        {},
        readableScalarFields(type)
      );

      const relationFieldResolvers = reduce(
        (_resolvers, field) => {
          const context = {
            typeName: field.parentType.name,
            fieldName: field.name,
            field,
            type,
            relation: relationFromField(parseContext, schema.types, field)
          };
          const baseResolver = makeRelationFieldResolver(
            schema,
            type,
            field
          );
          const resolver = applyFieldDirectives(
            field,
            context,
            baseResolver
          );
          return merge(_resolvers, { [field.name]: resolver });
        },
        {},
        relationFields(type)
      );

      // Query resolvers
      const findOneResolver = (type: Type): any => {
        const fieldName = camelize(type.name, true);
        const context = { typeName: "Query", fieldName, type };
        return {
          [fieldName]: applyTypeDirectives(
            context,
            makeFindOneResolver(type)
          )
        };
      };

      const findResolver = (type: Type): any => {
        const fieldName = pluralize(camelize(type.name, true));
        const context = { typeName: "Query", fieldName, type };
        return {
          [fieldName]: applyTypeDirectives(context, makeFindResolver(type))
        };
      };

      const findConnectionResolver = (type: Type): any => {
        const fieldName = `${pluralize(
          camelize(type.name, true)
        )}Connection`;
        const context = { typeName: "Query", fieldName, type };
        return {
          [fieldName]: applyTypeDirectives(
            context,
            makeFindConnectionResolver(schema, type)
          )
        };
      };

      // Mutation resolvers
      const createResolver = (type: Type): any => {
        const fieldName = `create${type.name}`;
        const context = { typeName: "Mutation", fieldName, type };
        return {
          [fieldName]: applyTypeDirectives(
            context,
            makeCreateResolver(type)
          )
        };
      };

      const updateResolver = (type: Type): any => {
        const fieldName = `update${type.name}`;
        const context = { typeName: "Mutation", fieldName, type };
        return {
          [fieldName]: applyTypeDirectives(
            context,
            makeUpdateResolver(type)
          )
        };
      };

      const upsertResolver = (type: Type): any => {
        const fieldName = `upsert${type.name}`;
        const context = { typeName: "Mutation", fieldName, type };
        return {
          [fieldName]: applyTypeDirectives(
            context,
            makeUpsertResolver(type)
          )
        };
      };

      const deleteResolver = (type: Type): any => {
        const fieldName = `delete${type.name}`;
        const context = { typeName: "Mutation", fieldName, type };
        return {
          [fieldName]: applyTypeDirectives(
            context,
            makeDeleteResolver(type)
          )
        };
      };

      const updateManyResolver = (type: Type): any => {
        const fieldName = `updateMany${pluralize(type.name)}`;
        const context = { typeName: "Mutation", fieldName, type };
        return {
          [fieldName]: applyTypeDirectives(
            context,
            makeUpdateManyResolver(type)
          )
        };
      };

      const deleteManyResolver = (type: Type): any => {
        const fieldName = `deleteMany${pluralize(type.name)}`;
        const context = { typeName: "Mutation", fieldName, type };
        return {
          [fieldName]: applyTypeDirectives(
            context,
            makeDelManyResolver(type)
          )
        };
      };

      const mergedResolvers = merge(resolvers, {
        Query: {
          ...findOneResolver(type),
          ...findResolver(type),
          ...findConnectionResolver(type)
        },
        Mutation: {
          ...createResolver(type),
          ...updateResolver(type),
          ...upsertResolver(type),
          ...deleteResolver(type),
          ...updateManyResolver(type),
          ...deleteManyResolver(type)
        },
        [type.name]: merge(fieldResolvers, relationFieldResolvers)
      });
      return mergedResolvers;
    };
    const generatedResolvers = reduce(reduceFn, scalarResolvers, schema.types);
    
    // Merge in the custom resolvers
    return merge(generatedResolvers, parseContext.resolvers);
};

/**
 * Loading custom resolvers from file system
 */
export type CustomResolvers = {
    Query: Record<string, any>,
    Mutation: Record<string, any>,
};

type FutureCustomResolvers = FutureInstance<Error, CustomResolvers>;

export const loadCustomResolvers = (dirs: string[]): FutureCustomResolvers => {
  // for each dir:
  // 1) get files in queries and mutations sub dirs
  // 2) load each resolver in queries and mutations sub dirs
  // 3) put everything into a CustomResolvers data structure
  
  const reduceFn = (resolversFuture: FutureCustomResolvers, dir: string): FutureCustomResolvers => go(function* () {
    const addResolver = S.curry3((dir: string, resolvers: any, path: string) => {
      return ({
        ...resolvers,
        [`${basename(path, extname(path))}`]: require(joinPath(relative(__dirname, dir), path)).default
      });
    });
    const mutationsPath = `${dir}/mutations`;
    const queriesPath = `${dir}/queries`;
    const mutationsDirectory: Directory = yield safeDirectory(mutationsPath);
    const queriesDirectory: Directory = yield safeDirectory(queriesPath);
    const isTest = (file: string) => !isEmpty(match(/(__tests__|.test.js|.test.ts)/, file));
    const notTest = (file: string) => !isTest(file);
    // filter out test files
    const mutationFiles = S.filter(notTest)(mutationsDirectory.files);
    const queryFiles = S.filter(notTest)(queriesDirectory.files);
    const mutationResolvers = S.reduce(addResolver(mutationsPath))({})(mutationFiles);
    const queryResolvers = S.reduce(addResolver(queriesPath))({})(queryFiles);
    const resolvers = clone(yield resolversFuture);
    resolvers.Query = { ...resolvers.Query, ...queryResolvers };
    resolvers.Mutation = { ...resolvers.Mutation, ...mutationResolvers }
    return resolvers;
  });

  const seed: FutureCustomResolvers = resolve({ Query: {}, Mutation: {} });
  return reduce(reduceFn, seed, dirs);
};

export const loadDirectiveResolvers = (dirs: string[]): FutureInstance<{}, any> =>
    // Todo: generalize this so it reads all modules from the directives directory
    resolve({
        loginRequired: require('../directives/loginRequired' ).default,
        auth: require('../directives/auth').default,
    });