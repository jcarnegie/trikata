// @flow
import { curry, filter, join, map } from 'ramda';

import { Schema } from '../index';
import { Type, readableFields } from '../type';
import { Field } from '../field';

import {
  findRelationByFieldName,
  isHasMany,
} from '../relation';

export const formatTypeField = (schema: Schema, field: Field): string => {
  const {
    name,
    type,
    nullable,
    isRelation,
  } = field;

  if (isRelation) {
    const relation = findRelationByFieldName(schema, field.parentType, field.name);
    if (!relation) throw new Error('');
    if (isHasMany(relation)) {
      const targetName = relation.targetType.name;
      const fieldParams = `(where: ${targetName}WhereInput, orderBy: ${targetName}OrderByInput, skip: Int, after: String, before: String, first: Int, last: Int)`;
      return `${name}${fieldParams}: [${type}!]`;
    }
  }
  return field.isList
    ? field.listItemsNullable
      ? `${name}: [${type}]${nullable ? '' : '!'}`
      : `${name}: [${type}!]${nullable ? '' : '!'}`
    : `${name}: ${type}${nullable ? '' : '!'}`;
};

export const renderTypedef = curry((schema: Schema, type: Type) => {
  const typedef = `
type ${type.name} implements Node {
${join('\n', map(f => `  ${formatTypeField(schema, f)}`, readableFields(type)))}
}
`;
  return typedef;
});

export const renderStatelessTypedef = curry((schema: Schema, type: Type) => {
  const typedef = `
type ${type.name} {
${join('\n', map(f => `  ${formatTypeField(schema, f)}`, readableFields(type)))}
}
`;
  return typedef;
});
