// @flow

import { FieldDefinitionNode } from 'graphql/language';

import {
  contains,
  curry,
  equals,
  filter,
  find,
  isNil,
  join,
  map,
  path,
  propEq,
} from 'ramda';
import { underscore } from 'inflection';

import { ParseSchemaContext } from './context';
import { Schema } from '.';
import { Type } from './type';

import { directives } from './directives';
import { createArguments, renderGQL as renderArgumentGQL } from './argument';
import { whereAll } from '../util/object';

export type FieldType =
  'String' |
  'Int' |
  'Float' |
  'Boolean' |
  'DateTime' |
  'JSON' |
  'ID';

export type Field = {
  parentType: Type,
  name: string,
  columnName: string,
  type: string,
  isList: boolean,
  listItemsNullable: boolean,
  sqlType: string,
  size: number,
  primaryKey: boolean,
  nullable: boolean,
  default: any,
  unique: boolean,
  isRelation: boolean,
  isScalarType: boolean,
  // Todo: add more typing for arguments and directives...
  arguments: any,
  directives: any,
};

const SCALAR_TYPES = ['ID', 'String', 'Int', 'Float', 'Boolean'];

const NULLABLE_LIST_TYPE_SPEC = { type: { kind: equals('ListType') } };
const NON_NULL_LIST_TYPE_SPEC = { type: { kind: equals('NonNullType'), type: { kind: equals('ListType') } } };
const NULLABLE_LIST_TYPE_NULLABLE_LIST_ITEMS_SPEC = { type: { kind: equals('ListType'), type: { kind: equals('NamedType') } } };
const NON_NULL_LIST_TYPE_NON_NULL_LIST_ITEMS_SPEC = { type: { kind: equals('NonNullType'), type: { kind: equals('ListType'), type: { kind: equals('NamedType') } } } };

export const fieldName = (field: FieldDefinitionNode): string => field.name.value;

export const isFieldNonNullScalarType = (field: FieldDefinitionNode) => field.type.kind === 'NonNullType';

export const isListType = (field: FieldDefinitionNode) => {
  // nullable list
  const case1 = whereAll(NULLABLE_LIST_TYPE_SPEC, field);
  // non-null list
  const case2 = whereAll(NON_NULL_LIST_TYPE_SPEC, field);
  return case1 || case2;
};

export const isListNullable = (field: FieldDefinitionNode): boolean =>
  whereAll(NULLABLE_LIST_TYPE_SPEC, field);

export const listElementsNullable = (field: FieldDefinitionNode): boolean => {
  const case1 = whereAll(NULLABLE_LIST_TYPE_NULLABLE_LIST_ITEMS_SPEC, field);
  const case2 = whereAll(NON_NULL_LIST_TYPE_NON_NULL_LIST_ITEMS_SPEC, field);
  return case1 || case2;
};

export const fieldType = (field: FieldDefinitionNode): string => {
  let typePath = [];
  if (isListType(field) && isListNullable(field)) {
    if (listElementsNullable(field)) {
      typePath = ['type', 'type', 'name', 'value'];
    } else {
      typePath = ['type', 'type', 'type', 'name', 'value'];
    }
  } else if (isListType(field) && !isListNullable(field)) {
    if (listElementsNullable(field)) {
      typePath = ['type', 'type', 'type', 'name', 'value'];
    } else {
      typePath = ['type', 'type', 'type', 'type', 'name', 'value'];
    }
  } else if (isFieldNonNullScalarType(field)) {
    typePath = ['type', 'type', 'name', 'value'];
  } else {
    typePath = ['type', 'name', 'value'];
  }
  return path(typePath, field) || '';
};

export const sqlType = (context: any, field: FieldDefinitionNode) => {
  // see if there's an @sql directive see if we need to override the default type
  const sqlDir = find(propEq("name", "sql"), directives(field));
  if (sqlDir && sqlDir.args.type) {
    return {
      sqlType: sqlDir.args.type,
      size: sqlDir.args.size || 0,
    };
  }

  // otherwise, use default mapping of GQL types to SQL types
  return context.sqlType(fieldType(field));
};

export const fieldDefault = (defaultDirective: any): any => {
  if (!defaultDirective) return null;
  return defaultDirective.args.value;
};

export const scalarFields = (type: Type): Field[] =>
  filter(propEq('isRelation', false), type.fields);

export const relationFields = (type: Type): Field[] =>
  filter(propEq('isRelation', true), type.fields);

export const createFieldFromFieldDef =
  curry((context: ParseSchemaContext, parentType: Type, field: FieldDefinitionNode): Field => {
    return ({
      parentType,
      name: fieldName(field),
      columnName: underscore(fieldName(field)),
      type: fieldType(field),
      isList: isListType(field),
      // Todo: needs testing, not sure this is valid in all cases
      listItemsNullable: listElementsNullable(field),
      sqlType: sqlType(context, field).sqlType,
      size: sqlType(context, field).size,
      nullable: !isFieldNonNullScalarType(field),
      primaryKey: (fieldType(field) === 'ID'),
      default: fieldDefault(find(propEq('name', 'default'), directives(field))),
      unique: !isNil(find(propEq('name', 'unique'), directives(field))),
      // isRelation: !contains(fieldType(field), SCALAR_TYPES),
      isRelation: false,
      isScalarType: contains(fieldType(field), SCALAR_TYPES) || contains(fieldType(field), map(s => s.name, context.scalars || [])),
      arguments: createArguments(field),
      directives: directives(field),
    });
  });

export const createTimestampField =
  curry((context: ParseSchemaContext, parentType: Type, name: string): Field => {
    return ({
      parentType,
      name,
      columnName: underscore(name),
      type: 'DateTime',
      isList: false,
      // Todo: needs testing, not sure this is valid in all cases
      listItemsNullable: false,
      sqlType: 'timestamp with time zone',
      size: 0,
      nullable: true,
      primaryKey: false,
      default: null,
      unique: false,
      // isRelation: !contains(fieldType(field), SCALAR_TYPES),
      isRelation: false,
      isScalarType: true,
      arguments: [],
      directives: [],
    });
  });

export const renderFieldGQL = (field: Field) => {
  const renderedArgs = map(renderArgumentGQL, field.arguments);
  const argsStr = renderedArgs.length > 0 ? `(${join(', ', renderedArgs)})` : '';
  let returnType = field.nullable ? field.type : `${field.type}!`;
  returnType = field.isList ? `[${returnType}]` : returnType;
  returnType = field.isList && !field.listItemsNullable ? `${returnType}!` : returnType;
  return `${field.name}${argsStr}: ${returnType}`;
};

export const createIdField = (
  context: ParseSchemaContext,
  name: string,
  parentType: Type,
): Field => ({
  parentType,
  name,
  columnName: underscore(name),
  type: 'String',
  isList: false,
  listItemsNullable: false,
  sqlType: 'varchar',
  size: 36,
  nullable: false,
  primaryKey: false,
  default: null,
  unique: false,
  isRelation: false,
  isScalarType: true,
  arguments: [],
  directives: [],
});

export const hasDirective = curry((name: string, field: Field): boolean =>
  find(propEq('name', name), field.directives) !== undefined);

export const isEnumType = curry((schema: Schema, field: Field):boolean =>
  find(propEq('name', field.type), schema.enums) !== undefined);