// @flow
import { parse } from 'graphql/language';
import { contains, filter, find, map, propEq, unnest } from 'ramda';
import { makeExecutableSchema } from 'graphql-tools';
import { ParseSchemaContext, RuntimeContext } from './context';
import { Type } from './type';
import { Enum } from './enum';
import { Field } from './field';
import { Relation } from './relation';
import {
  createTypeFromTypeDef,
  filterEnumTypeDefs,
  filterTypeDefs,
} from './type';
import { createEnumFromEnumTypeDefinition } from './enum';
import { enhance } from './enhance';
import { generate as generateResolvers } from './resolvers';
import { createRelations } from './relation';

export type Schema = {
  types: Type[],
  statelessTypes: Type[],
  queryType?: Type,
  mutationType?: Type,
  typedJsonFields: Field[],
  enums: Enum[],
  relations: Relation[],
};

export const isStateless = (t: Type) =>
  find(propEq('name', 'stateless'), t.directives) !== undefined;

export const isQueryType = (t: Type) => t.name === 'Query';
export const isMutationType = (t: Type) => t.name === 'Mutation';

/**
 * Types are excluded if:
 * 
 * 1) It's the Query or Mutation type
 * 2) It's not marked as stateless
 */
export const isTypeIncluded = (t: Type) =>
  !contains(t.name, ['Query', 'Mutation']) &&
  !isStateless(t);

export const parseTypedefs = (context: ParseSchemaContext, typedefs: string): Schema => {
  const ast = parse(typedefs);
  const parsedTypedefs = filterTypeDefs(ast);
  const allTypes = map(td => createTypeFromTypeDef(context, td), parsedTypedefs);
  // Make sure we're not enhancing reserved types 'Query' or 'Mutation', @stateless, etc
  const types = filter(isTypeIncluded, allTypes);
  const statelessTypes = filter(isStateless, allTypes);
  const queryType = find(isQueryType, allTypes);
  const mutationType = find(isMutationType, allTypes);
  const allFields = unnest(map(t => t.fields, allTypes));
  const allTypeNames = map(t => t.name, allTypes);
  const typedJsonFields = filter((f: Field) => f.sqlType === 'jsonb' && contains(f.type, allTypeNames), allFields);
  const relations = createRelations(context, types);
  const enumTypedefs = filterEnumTypeDefs(ast);
  const enums = map(createEnumFromEnumTypeDefinition, enumTypedefs);
  return {
    types,
    statelessTypes,
    queryType,
    mutationType,
    typedJsonFields,
    enums,
    relations,
  };
};

export const create = (
  parseContext: ParseSchemaContext,
  runtimeContext: RuntimeContext,
  typedefs: string,
): any => {
  const enhancedTypedefs = enhance(parseContext, typedefs);
  // console.log(enhancedTypedefs);
  const resolvers = generateResolvers(parseContext, runtimeContext, typedefs);
  return makeExecutableSchema({
    resolverValidationOptions: { requireResolversForResolveType: false },
    typeDefs: enhancedTypedefs,
    resolvers,
  });
};
