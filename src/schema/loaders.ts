// @flow
import DataLoader from 'dataloader';

import { Relation } from './relation';

export default class LoaderManager {
  loaders: any;

  constructor() {
    this.loaders = {};
  }

  clear() {
    this.loaders = {};
  }

  get(relation: Relation, loadFn: any): any {
    const loaderKey = `${relation.srcType.name}.${relation.srcField.name}`;
    if (!this.loaders[loaderKey]) {
      this.loaders[loaderKey] = new DataLoader(loadFn);
    }
    return this.loaders[loaderKey];
  }
}
