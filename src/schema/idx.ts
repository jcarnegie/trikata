// @flow
import { curry, find, isNil, join, propEq } from 'ramda';

import { Type } from './type';
import { Field } from './field';

// type SORT_ORDER = 'asc' | 'desc';
// type NULL_SORT_ORDER = 'null first' | 'nulls last';

export type Index = {
  parentType: Type,
  name: string,
  columns: string[],
  type: string,
  primary: boolean,
  unique: boolean,
};

export const createIndexFromField = (field: Field): Index => ({
  parentType: field.parentType,
  name: field.type === 'ID' ? `${field.parentType.tableName}_pkey` : `${field.parentType.tableName}_${field.columnName}_key`,
  columns: [field.columnName],
  type: 'btree',
  primary: false,
  unique: !isNil(find(propEq('name', 'unique'), field.directives)),
});

export const createIndexFromDirective = curry((type: Type, directive: any): Index => ({
  parentType: type,
  name: `${type.tableName}_${join('_', directive.args.columns)}_key`,
  columns: directive.args.columns,
  type: directive.args.type || 'btree',
  primary: false,
  unique: directive.args.unique || false,
}));
