import nodeGlob from 'glob';
import nodeRmdir from 'rmdir-recursive';
import {
    access as nodeAccess,
    readdir as nodeReaddir,
    readFile as nodeReadFile,
    mkdir as nodeMkdir,
    writeFile as nodeWriteFile,
} from 'fs';
import S from './sanctuary';
import F, { FutureInstance, node, go } from 'fluture';
import { basename, join } from 'path';

export type Directory = {
    path: string,
    name: string,
    files: string[],
    filePaths: string[],
};

export type FutureDirectory = FutureInstance<Error, Directory>;

// readFile :: string -> FutureInstance<Error, string>
export const readFile = (path: string): FutureInstance<Error, string> =>
    node(done => nodeReadFile(path, 'utf8', done));

export const readFileSafe = (path: string): FutureInstance<Error, string> =>
    node(done => nodeReadFile(path, 'utf8', (err, contents) => done(null, contents)));

export const mkdir = (path: string): FutureInstance<Error, any> =>
    node(done => nodeMkdir(path, () => done(null)));

export const rmdir = (path: string): FutureInstance<Error, any> =>
    node(done => nodeRmdir(path, () => done(null)));

export const writeFile = (path: string, contents: string) =>
    node(done => nodeWriteFile(path, contents, 'utf8', done));

// readdir :: string -> FutureInstance<Error, string[]>
export const readdir = (dir: string): FutureInstance<Error, string[]> =>
    node(done => nodeReaddir(dir, done));

export const createDirectory = (name: string, path: string, files: string[], filePaths: string[]): Directory => ({
    name,
    path,
    files,
    filePaths,
});

export const makeFilePaths = S.curry2((path: string, files: string[]) => S.map((f: string) => join(path, f))(files));

export const directory = (path: string): FutureDirectory => <FutureDirectory>go(function* () {
    const files: string[] = yield readdir(path);
    const filePaths = makeFilePaths(path)(files);
    const directory: Directory = {
        name: basename(path),
        path,
        files,
        filePaths
    };
    return directory;
});

// export const directory = (path: string): FutureInstance<Error, Directory> =>
//     map((files: string[]) => createDirectory(basename(path), path, files, makeFilePaths(path, files)))(readdir(path));

// safeReaddir :: string -> FutureInstance<Error, string[]>
export const safeReaddir = (dir: string): FutureInstance<Error, string[]> =>
    node(done => nodeReaddir(dir, (err, files) => {
        if (err && err.code === 'ENOENT') return done(null, []);
        done(err, files);
    }));

// safeDirectory :: string -> FutureInstance<Error, Directory>
export const safeDirectory = (path: string): FutureDirectory => <FutureDirectory>go(function* () {
    const files: string[] = yield safeReaddir(path);
    const filePaths = makeFilePaths(path)(files);
    const directory: Directory = {
        name: basename(path),
        path,
        files,
        filePaths
    };
    return directory;
});

export const fileExists = (path: string): FutureInstance<Error, boolean> =>
    node(done => nodeAccess(path, (err) => done(null, err ? false : true)));

export const glob = (pattern: string, options: any): FutureInstance<Error, string[]> =>
    node(done => nodeGlob(pattern, options, done));