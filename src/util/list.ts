import { concat as rConcat, reduce, unapply } from 'ramda';

export const concat = unapply(reduce(rConcat, <any>[]));
