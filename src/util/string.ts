import { curry, isEmpty, match, not, replace, toLower } from 'ramda';
import T from 'ramda/es/T';

export const matches = (re: RegExp, str: string) => not(isEmpty(match(re, str)));

export const lowerFirstLetter = (s: string) => replace(/^./, (m) => toLower(m), s);

// Taken from here: https://github.com/moll/json-stringify-safe/blob/master/stringify.js
const _serializer = (replacer: any, cycleReplacer: any) => {
  const stack: any[] = [];
  const keys: any[] = [];

  if (cycleReplacer == null) {
    cycleReplacer = () => null;
    // cycleReplacer = (key, value) => {
    //   if (stack[0] === value) return '[Circular ~]';
    //   const level = keys.slice(0, stack.indexOf(value)).join('.');
    //   return `[Circular ~. ${level}]`;
    // };
  }

  return function x(this: any, key: any, value: any) {
    if (stack.length > 0) {
      const thisPos = stack.indexOf(this);
      /* eslint-disable */
      ~thisPos
        ? stack.splice(thisPos + 1)
        : stack.push(this);
      ~thisPos
        ? keys.splice(thisPos, Infinity, key)
        : keys.push(key);
      if (~stack.indexOf(value)) value = cycleReplacer.call(this, key, value);
      /* eslint-enable */
    } else {
      stack.push(value);
    }

    return replacer == null ? value : replacer.call(this, key, value);
  };
};

export const stringify = (obj: any, replacer: any = null, spaces: any = null, cycleReplacer: any = null) =>
  JSON.stringify(obj, _serializer(replacer, cycleReplacer), spaces);


type FnToString = <T>(fn: T) => string;
type FnStrCompare = <T>(a: T, b: T) => number;

export const ascendString = curry(<T>(fn: (obj: T) => string, a: T, b: T): number => {
  const aa = fn(a);
  const bb = fn(b);
  return aa.localeCompare(bb);
});

export const descendString = curry(<T>(fn: (a: T) => string, a: T): (b: T) => number => {
  return (b) => fn(b).localeCompare(fn(a));
});
