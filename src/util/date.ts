import { curry, sort } from 'ramda';

const cmpPropAsc = curry((prop: string, a: any, b: any): number =>
  a[prop].getTime() - b[prop].getTime());

const cmpPropDesc = curry((prop: string, a: any, b: any): number =>
  b[prop].getTime() - a[prop].getTime());

export const sortAscByDate =
  sort((a: Date, b: Date) => a.getTime() - b.getTime());

export const sortDescByDate =
  sort((a: Date, b: Date) => b.getTime() - a.getTime());

export const sortAscByDateProp = (prop: string, list: any) => {
  return sort(cmpPropAsc(prop), list);
};

export const sortDescByDateProp = (prop: string, list: any) => {
  return sort(cmpPropDesc(prop), list);
};