import uuid from 'uuid/v4';
import { MigrationPlan, createFromSDLAndDB } from './plan';
import { migrationSql } from './sql';
import { PGDBContext, connect } from '../db/pg';
import { log } from '../util/logging';


/**
 * 
 * @param client 
 */
export const exec = async () => {
  log.level(bunyan.FATAL + 1);
  const client = await connect();
  const typedefs = await loadTypedefs();
  const error = (e: any) => {
    console.error("Error running migrations:", e);
    process.exit(1);
    client.end();
  };
  const success = (plan: MigrationPlan) => {
    const dbCtx: PGDBContext = {
      schemaName: "trikata",
      client,
      idFn: uuid,
    };
    const sql = migrationSql(dbCtx, plan);
    console.log(formatSql(sql));
    client.end();
  };
  fork(error)(success)(createFromSDLAndDB(client, typedefs));
};
