import uuid from 'uuid/v4';
import { FutureInstance, attemptP, chain } from 'fluture';
import {
  append,
  compose,
  concat,
  contains,
  curry,
  differenceWith,
  filter,
  find,
  isNil,
  length,
  map as rMap,
  not,
  propEq,
  reduce,
  unnest,
} from 'ramda';
import S from "../util/sanctuary";
import { Schema, parseTypedefs } from "../schema";
import { Type } from '../schema/type';
import { Index } from "../schema/idx";
import { Field, scalarFields } from "../schema/field";
import { Relation, defaultName } from "../schema/relation";
import {
  ParseSchemaContext,
  RuntimeContext,
  createParseSchemaContext
} from "../schema/context";
import { PGDBContext } from "../db/pg";
import { parseDBSchema } from '../db/pg/ddl';

export enum MigrationActions {
  ADD_COLUMN,
  CREATE_TABLE,
  CREATE_RELATION_TABLE,
  RENAME_RELATION_TABLE,
  ADD_INDEX,
  UPDATE_COLUMN,
  REMOVE_COLUMN,
  REMOVE_TABLE,
  REMOVE_INDEX,
};

export type CreateTableAction = {
  actionType: MigrationActions,
  type: Type
};

export type RenameRelationTableAction = {
  actionType: MigrationActions,
  old: Relation,
  new: Relation,
};

export type CreateRelationTableAction = {
  actionType: MigrationActions,
  relation: Relation,
};

export type CreateAddColumnAction = {
  actionType: MigrationActions,
  field: Field,
};

export type CreateAddIndexAction = {
  actionType: MigrationActions,
  index: Index,
};

export type CreateUpdateColumnAction = {
  actionType: MigrationActions,
  changes: string[],
  field: Field,
  dbField: Field,
};

export type CreateRemoveColumnAction = {
  actionType: MigrationActions,
  field: Field,
};

export type CreateRemoveTableAction = {
  actionType: MigrationActions;
  type: Type;
};

export type CreateRemoveIndexAction = {
  actionType: MigrationActions;
  index: Index;
};

export type MigrationAction =
  | CreateTableAction
  | CreateRelationTableAction
  | CreateAddColumnAction
  | CreateAddIndexAction
  | CreateUpdateColumnAction
  | CreateRemoveColumnAction
  | CreateRemoveTableAction;

export type MigrationPlan = {
  actions: MigrationAction[];
};

const notNil = compose(not, isNil);
const cmpTypes = (x: any, y: any) => x.tableName === y.tableName;
const cmpByName = (x: any, y: any) => x.name === y.name;

const renamableRelations = (
  schema: Schema,
  dbSchema: Schema
): { old: Relation; new: Relation }[] => {
  // Filter for relations where the relation types have no other relation between them
  const filterSingleTypeRelations = filter((relation: Relation) => {
    const typeACount = reduce(
      (acc, r) => {
        const hasType =
          r.srcType.name === relation.srcType.name ||
          r.targetType.name === relation.srcType.name;
        return acc + (hasType ? 1 : 0);
      },
      0,
      schema.relations
    );
    const typeBCount = reduce(
      (acc, r) => {
        const hasType =
          r.srcType.name === relation.targetType.name ||
          r.targetType.name === relation.targetType.name;
        return acc + (hasType ? 1 : 0);
      },
      0,
      schema.relations
    );
    return typeACount === 1 && typeBCount === 1;
  });

  const singleTypeRelations = filterSingleTypeRelations(schema.relations);

  const relationMatcher = curry((r1: Relation, r2: Relation): boolean => {
    return (
      (r1.srcType.name === r2.types[0].name && r1.targetType.name === r2.types[1].name) ||
      (r1.srcType.name === r2.types[1].name && r1.targetType.name === r2.types[0].name)
    ) && (
      r1.name !== r2.name &&
      (
        r1.name === defaultName(r1.srcType, r1.targetType) ||
        r2.name === defaultName(r1.srcType, r1.targetType) ||
        notNil(r1.renameTo)
      )
    )
  });

  const relationsToRename = reduce(
    (acc: any, r) => {
      const matchingRelation = find(relationMatcher(r), dbSchema.relations);
      return (matchingRelation)
        ? append({ old: matchingRelation, new: r }, acc)
        : acc;
    },
    [],
    singleTypeRelations
  );

  return relationsToRename;
};

export const createCreateTableAction = (type: Type): CreateTableAction => {
  return {
    actionType: MigrationActions.CREATE_TABLE,
    type,
  };
};

export const createCreateRelationTableAction = (relation: Relation): CreateRelationTableAction => {
  return {
    actionType: MigrationActions.CREATE_RELATION_TABLE,
    relation,
  }
}

export const createRenameRelationTableAction = (relationPair: { old: Relation; new: Relation }): RenameRelationTableAction => {
  return {
    actionType: MigrationActions.RENAME_RELATION_TABLE,
    new: relationPair.new,
    old: relationPair.old,
  }
};

export const createAddColumnAction = (field: Field): CreateAddColumnAction => {
  return {
    actionType: MigrationActions.ADD_COLUMN,
    field,
  };
};

export const createAddIndexAction = (index: Index): CreateAddIndexAction => {
  return {
    actionType: MigrationActions.ADD_INDEX,
    index,
  }
};

export const createUpdateColumnAction = (updateData: any): CreateUpdateColumnAction => {
  return {
    actionType: MigrationActions.UPDATE_COLUMN,
    ...updateData,
  }
};

export const createRemoveColumnAction = (field: Field): CreateRemoveColumnAction => {
  return {
    actionType: MigrationActions.REMOVE_COLUMN,
    field,
  };
};

export const createRemoveTableAction = (type: Type): CreateRemoveTableAction => {
  return {
    actionType: MigrationActions.REMOVE_TABLE,
    type,
  };
};

export const createRemoveIndexAction = (index: Index): CreateRemoveIndexAction => {
  return {
    actionType: MigrationActions.REMOVE_INDEX,
    index,
  };
};

export const missingRelations = (a: Schema, b: Schema): Relation[] => {
  const missingDBRelations = differenceWith(cmpByName, a.relations, b.relations);
  const renamableNames = rMap(p => p.new.name, renamableRelations(a, b));
  return filter(r => !contains(r.name, renamableNames), missingDBRelations);
};

export const missingFields = (a: Schema, b: Schema): Field[] => {
  const missing = rMap((type: Type) => {
    const dbType = find(propEq("name", type.name), b.types);
    if (!dbType) return [];
    return filter(
      (f: Field) => isNil(find(propEq("columnName", f.columnName), dbType.fields)),
      scalarFields(type)
    );
  }, a.types);
  // Todo: fix: casting to any is cheap hack to suppress TS compile error
  return unnest(missing);
};

export const missingIndexes = (a: Schema, b: Schema): Index[] =>
  unnest(
    rMap(type => {
      const dbType = find(propEq("name", type.name), b.types);
      if (!dbType) return [];
      return filter(
        (idx: Index) => isNil(find(propEq("name", idx.name), dbType.indexes)),
        type.indexes
      );
    }, a.types)
  );

export const updatedColumns = (a: Schema, b: Schema): any => {
  // Changes that can be made to a field / dbCtx.client column
  //  1) change in type
  //  2) rename (as opposed to drop existing column, add new column, resulting in potential data loss)
  //  3) set / drop not null
  //  4) set / drop default
  //  5) change type size (i.e. varchar (255) --> varchar (80))
  // Todo: handle rename separately since we won't necessarily find matching DB type if field name is changed
  //       Note: need to handle case where new field name exists in DB
  return reduce((updates: any, type: Type) => {
    const dbType = find(propEq('name', type.name), b.types);
    if (!dbType) return updates;
    return concat(updates, reduce((_updates: any, field: Field) => {
      const dbField = find(propEq('name', field.name), dbType.fields);
      if (!dbField) return _updates;
      let changes: any = [];
      // change in type
      if (field.sqlType !== dbField.sqlType || field.size !== dbField.size) changes = append('type', changes);
      // change in nullability
      if (field.nullable !== dbField.nullable) changes = append('nullability', changes);
      // change in default
      if (field.default !== dbField.default) changes = append('default', changes);
      if (length(changes) === 0) return _updates;

      return append({
        changes,
        field,
        dbField,
      }, _updates);
    }, [], type.fields));
  }, [], a.types);
};

export const removeableFields = (a: Schema, b: Schema): Field[] => {
  return unnest(
    rMap(type => {
      const dbType = find(propEq("name", type.name), b.types);
      if (!dbType) return [];
      return filter(
        (f: Field) => isNil(find(propEq("columnName", f.columnName), type.fields)),
        dbType.fields
      );
    }, a.types)
  );
};

export const removeableTypes = (a: Schema, b: Schema): Type[] => {
  const droppableDBTypes = differenceWith(cmpTypes, b.types, a.types);
  const renamableNames = rMap(p => p.old.name, renamableRelations(a, b));
  const typeIsRelation = curry((t: Type, r: Relation) => t.name === r.name);
  return filter(t => !contains(t.name, renamableNames) && find(typeIsRelation(t), a.relations) === undefined, droppableDBTypes);
};

export const removeableIndexes = (a: Schema, b: Schema): Index[] => {
  return unnest(rMap(type => {
    const dbType = find(propEq('name', type.name), b.types);
    if (!dbType) return [];
    return filter(idx => isNil(find(propEq('name', idx.name), type.indexes)), dbType.indexes);
  }, a.types));
}

export const create = (a: Schema, b: Schema) => {
  const missingDBTypes = differenceWith(cmpTypes, a.types, b.types);
  const renamableRelationPairs = renamableRelations(a, b);
  const missingRelationsList = missingRelations(a, b);
  const missingFieldsList = missingFields(a, b);
  const missingIndexList = missingIndexes(a, b);
  const updatedColumnsList = updatedColumns(a, b);
  const removeableFieldList = removeableFields(a, b);
  const removeableTypeList = removeableTypes(a, b);
  const removeableIndexList = removeableIndexes(a, b);

  const createTableActions = S.map(createCreateTableAction)(missingDBTypes);
  const renameRelationTableActions = S.map(createRenameRelationTableAction)(renamableRelationPairs);
  const createRelationTableActions = S.map(createCreateRelationTableAction)(missingRelationsList);
  const addColumnActions = S.map(createAddColumnAction)(missingFieldsList);
  const addIndexActions = S.map(createAddIndexAction)(missingIndexList);
  const addUpdateColumnActions = S.map(createUpdateColumnAction)(updatedColumnsList);
  const removeColumnActions = S.map(createRemoveColumnAction)(removeableFieldList);
  const removeTableActions = S.map(createRemoveTableAction)(removeableTypeList);
  const removeIndexActions = S.map(createRemoveIndexAction)(removeableIndexList);

  const plan: MigrationPlan = {
    actions: <MigrationAction[]><unknown>S.reduce(S.concat)(<any>[])([
      createTableActions,
      renameRelationTableActions,
      createRelationTableActions,
      addColumnActions,
      addIndexActions,
      addUpdateColumnActions,
      removeColumnActions,
      removeTableActions,
      removeIndexActions,
    ])
  };

  return plan;
};

export const createFromSDLAndDB = (client: any, typedefs: any): FutureInstance<{}, MigrationPlan> => {
  const createPlan = (parseSchemaContext: ParseSchemaContext): FutureInstance<{}, MigrationPlan> => {
    const schema = parseTypedefs(parseSchemaContext, typedefs);
    const dbCtx: PGDBContext = {
      schemaName: "trikata",
      client,
      schema,
      idFn: uuid,
    };
    return attemptP(async () => {
      const dbSchema = await parseDBSchema(dbCtx);
      return create(schema, dbSchema);
    });
  };
  const schemaCtxF = createParseSchemaContext();
  return chain(createPlan)(schemaCtxF);
};