import moment from 'moment';
import { attemptP, go, chain, chainRej, parallel, reject } from 'fluture';
import { differenceWith, map as rMap } from 'ramda';
import { QueryResult } from 'pg';
import S from '../util/sanctuary';
import { FutureMigrationArray, Migration } from '.';
import { TS_FORMAT } from './fs';
import { PGDBContext } from '../db/pg';
import { sortAscByDateProp } from '../util/date';


const cmpByCreatedAt = (a: any, b: any) => {
  return a.createdAt.getTime() === b.createdAt.getTime();
};

export const commit = S.curry2((dbCtx: PGDBContext, result: any) => go(function* () {
  yield attemptP(() => dbCtx.client.query('commit'));
  return result;
}));

export const rollback = S.curry2((dbCtx: PGDBContext, error: any) => chain(reject)(go(function* () {
  yield attemptP(() => dbCtx.client.query('rollback'));
  return error;
})));

export const transaction = (dbCtx: PGDBContext, fn: any) => {
  const txnFuture = go(function* () {
    yield attemptP(() => dbCtx.client.query('begin transaction'));
    return yield fn();
  });
  const successFuture = chain (commit(dbCtx)) (txnFuture);
  return chainRej (rollback(dbCtx)) (successFuture);
};

export const createMigrationsTable = (dbCtx: PGDBContext) => {
  const createSql = `
    create table if not exists ${dbCtx.schemaName}.__migrations (
      id bigserial,
      created_at timestamp with time zone, 
      typedefs text,
      typedefs_hash varchar(64),
      sql text,
      sql_hash varchar(64)
    )
  `;
  return attemptP(() => dbCtx.client.query(createSql));
}

export const loadMigrations = (dbCtx: PGDBContext): FutureMigrationArray =>
  <FutureMigrationArray>go(function* () {
    const sql = `select * from ${dbCtx.schemaName}.__migrations order by created_at`;
    const result = yield attemptP(() => dbCtx.client.query(sql));
    const createMigration = (row: any): Migration => ({
      createdAt: row.created_at,
      typedefs: row.typedefs,
      typedefsHash: row.typedefs_hash,
      sql: row.sql,
      sqlHash: row.sql_hash,
      beforeFiles: [],
      afterFiles: [],
    });
    return rMap(createMigration, result.rows);
  });

export const writeMigrationMetadata = (dbCtx: PGDBContext, migration: Migration) => go(function* () {
  const insertSql = `
    insert into ${dbCtx.schemaName}.__migrations
      (id, created_at, typedefs, typedefs_hash, sql, sql_hash)
    values
      (default, $1, $2, $3, $4, $5)
  `;
  const values = [
    migration.createdAt,
    migration.typedefs,
    migration.typedefsHash,
    migration.sql,
    migration.sqlHash
  ];
  return yield attemptP(() => dbCtx.client.query(insertSql, values));
});

export const applyMigration = S.curry2((dbCtx: PGDBContext, migration: Migration) => go(function* () {
  console.log(`Applying migration ${moment(migration.createdAt).format(TS_FORMAT)}`);
  yield transaction(dbCtx, () => go(function* () {
    if (migration.beforeSql) {
      yield attemptP(() => dbCtx.client.query(migration.beforeSql || ''));
    }

    yield attemptP(() => dbCtx.client.query(migration.sql));
    
    if (migration.afterSql) {
      yield attemptP(() => dbCtx.client.query(migration.afterSql || ''));
    }
    return yield writeMigrationMetadata(dbCtx, migration);
  }));
}));

export const applyMigrations = (
  dbCtx: PGDBContext,
  migrations: Migration[],
) => go(function* () {
  const futures = S.map(applyMigration(dbCtx))(migrations);
  return yield parallel(Infinity)(futures);
});

export const storeMigrationMetadata = async (dbCtx: PGDBContext, migration: Migration) => {
  try {
    const createSql = `
        create table if not exists ${dbCtx.schemaName}.__migrations (
          id bigserial,
          created_at timestamp with time zone, 
          typedefs text,
          typedefs_hash varchar(64),
          sql text,
          sql_hash varchar(64),
        )
      `;

    await dbCtx.client.query(createSql);

    const insertSql = `
        insert into ${dbCtx.schemaName}.__migrations
          (id, created_at, typedefs, typedefs_hash, sql, sql_hash)
        values
          (default, $1, $2, $3, $4, $5)
      `;

    await dbCtx.client.query(insertSql, [
      migration.createdAt,
      migration.typedefs,
      migration.typedefsHash,
      migration.sql,
      migration.sqlHash
    ]);
  } catch (e) {
    throw e;
  }
};