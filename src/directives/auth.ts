import { contains, intersection, length, pathOr } from 'ramda';

export default (directiveContext: any, originalResolver: any) =>
  async (source: any, args: any, context: any, info: any) => {
    const { directive, fieldName } = directiveContext;
    const { user } = context;

    const isLoggedIn = !!user;
    if (!isLoggedIn) throw new Error('Access denied');

    const requiredRoles = directive.args.roles;
    const userRoles = pathOr([], ['roles'], user);
    const authorized = length(intersection(userRoles, requiredRoles)) > 0;
    
    // if the target is a field then we just return null if they're not logged in
    const targetIsField = !!directiveContext.field;
    if (!authorized && targetIsField) return null;

    // Target is the type. Handle CRUD resolvers + applyTo here
    const doesApply = !directive.args.applyTo || contains(fieldName, directive.args.applyTo);
    if (doesApply && !authorized) throw new Error('Access denied');

    // Otherwise, call the original resolver
    return originalResolver(source, args, context, info);
  };