import { readFile as rf } from 'fs';
import { promisify } from 'bluebird';
import { start } from './server';

const readFile: (path: string, encoding: string) => any = promisify(rf);

const main = async () => {
  const typedefsPath = process.argv[2] || './examples/basic/index.graphql';
  const typedefs = await readFile(typedefsPath, 'utf8');
  await start(typedefs);
};

main();
