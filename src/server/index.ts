import cookieParser from 'cookie-parser';
import bunyanLogger from 'express-bunyan-logger';
import expressLogging from 'express-logging';
import uuid from 'uuid/v4';
import { GraphQLServer, GraphQLServerLambda } from 'graphql-yoga';
import { FutureInstance, attemptP, chain, go, map, promise } from "fluture";
import { existsSync } from 'fs';
import { join, relative } from 'path';
import S from '../util/sanctuary';
import { create as createGQLSchema, parseTypedefs } from '../schema';
import { ParseSchemaContext, RuntimeContext, createParseSchemaContext } from '../schema/context';
import { connect, init, PGDBContext } from '../db/pg';
import { makeApi } from '../api';
import { log } from '../util/logging';

export const start = async (typedefs: any) => {
  // 0. configure db + cache connections
  // 1. enhance schema
  // 2. parse enhanced schema
  // 3. generate resolvers
  // 4. create server

  const client = await connect();

  const doInit = (parseSchemaContext: ParseSchemaContext) => {
    const schema = parseTypedefs(parseSchemaContext, typedefs);
    const dbCtx: PGDBContext = {
      client,
      schemaName: "trikata",
      schema,
      idFn: uuid,
    };
    const db = init(dbCtx);
    const api = makeApi(db, schema);
    
    const runtimeContext: RuntimeContext = { db, schema, idFn: uuid };
    const gqlSchema = createGQLSchema(
      parseSchemaContext,
      runtimeContext,
      typedefs
    );
    let serverOptions = {};
    let startOptions = {};

    if (existsSync("server.js")) {
      const path = join(relative(__dirname, "."), "server");
      const server = require(path);
      serverOptions = server.serverOptions;
      startOptions = server.startOptions;
    }

    let middlewares = [];

    if (existsSync("middlewares.js")) {
      const path = join(relative(__dirname, "."), "middlewares");
      middlewares = require(path).default;
    }

    let customCtxInitFn: any = null;
    if (existsSync('context.js')) {
      const path = join(relative(__dirname, "."), "context");
      customCtxInitFn = require(path).default;
    }

    const cors = {
      origin: (origin: string, callback: any) => {
        callback(null, true);
      },
      methods: "GET,HEAD,PUT,PATCH,POST,DELETE",
      optionsSuccessStatus: 200,
      credentials: true
    };

    const server = new GraphQLServer({
      schema: gqlSchema,
      context: context => {
        const ctx = {
          db,
          api,
          schema,
          req: context.request,
          res: context.response,
        };
        return customCtxInitFn
          ? customCtxInitFn(ctx)
          : ctx;
      },
      resolverValidationOptions: { requireResolversForResolveType: false },
      ...serverOptions
    });
    server.express.use(cookieParser());
    server.express.use(bunyanLogger({ name: "trikata" }));

    // app defined middleware
    S.map((mw: any) => server.express.use(mw))(middlewares);
    
    const startFuture = attemptP(() => server.start({ cors, ...startOptions }));
    return startFuture;
  };

  const displayStartMessage = server => {
    console.log(`Server is running on localhost:${server.address().port}`);
    return server;
  };

  const initFuture = map
    (displayStartMessage)
    (chain(doInit)(createParseSchemaContext()));
  
  return promise(initFuture);
};

export const startLambda = (typedefs: any, client: any, options: any) => go(function* () {
  const parseSchemaContext = yield createParseSchemaContext();
  const schema = parseTypedefs(parseSchemaContext, typedefs);
  const dbCtx: PGDBContext = {
    client,
    schemaName: "trikata",
    schema,
    idFn: uuid,
  };
  const db = init(dbCtx);
  const api = makeApi(db, schema);

  const runtimeContext: RuntimeContext = { db, schema, idFn: uuid };
  const gqlSchema = createGQLSchema(
    parseSchemaContext,
    runtimeContext,
    typedefs
  );

  let customCtxInitFn: any = null;
  if (existsSync('context.js')) {
    const path = join(relative(__dirname, "."), "context");
    customCtxInitFn = require(path).default;
  }

  const lambda = new GraphQLServerLambda({
    options,
    schema: gqlSchema,
    context: context => {
      const ctx = {
        db,
        api,
        schema,
        event: context.event,
      };
      return customCtxInitFn
        ? customCtxInitFn(ctx)
        : ctx;
    },
    resolverValidationOptions: { requireResolversForResolveType: false }
  });

  return lambda;
});

export const migrate = (client: any, typedefs: any): FutureInstance<{}, [boolean, string]> => {
  // 0. configure db + cache connections
  // 1. enhance schema
  // 2. parse enhanced schema
  // 3. generate resolvers
  // 4. create server

  const doMigrate = (parseSchemaContext: ParseSchemaContext) => {
    const schema = parseTypedefs(parseSchemaContext, typedefs);
    const dbCtx: PGDBContext = {
      schemaName: "trikata",
      client,
      schema,
      idFn: uuid,
    };
    const db = init(dbCtx);
    return db.migrate(typedefs, schema);
  };

  return chain(doMigrate)(createParseSchemaContext());
};
