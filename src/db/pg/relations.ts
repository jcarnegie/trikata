// @flow

import { PGDBContext } from './index';
import { Relation } from '../../schema/relation';

import { execStmt, findOne, insert, update } from './index';

export const upsertRelation = async (
  dbCtx: PGDBContext,
  relation: Relation,
  where: any,
  parentEntity: any,
  upsertRelationData: any,
): Promise<any> => {
  const findSql = `select ${relation.targetJoinColName} from ${dbCtx.schemaName}.${relation.tableName} where ${relation.srcJoinColName} = $1`;
  const result = await execStmt(dbCtx.client, { sql: findSql, values: [parentEntity.id] });
  const relationId = (result.rowCount > 0) ? result.rows[0][relation.targetJoinColName] : '';
  const type = relation.targetType;

  const entity = await findOne(dbCtx, relation.targetType, { id: relationId });

  if (entity) {
    return update(dbCtx, type, upsertRelationData.update, where);
  }

  const newEntity = await insert(dbCtx, type, upsertRelationData.create);
  const joinSql = `insert into ${dbCtx.schemaName}.${relation.tableName} (id, ${relation.srcJoinColName}, ${relation.targetJoinColName}) values ($1, $2, $3)`;
  const vals = [dbCtx.idFn(), parentEntity.id, newEntity.id];
  await execStmt(dbCtx.client, { sql: joinSql, values: vals });
  return newEntity;
};
