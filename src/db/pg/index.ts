// @flow
import P from 'bluebird';
import { format as formatSql } from 'sql-formatter';
import { FutureInstance } from 'fluture';
import { SqlType } from '../index';
import { Client, QueryResult } from 'pg';
import {
  addIndex as rAddIndex,
  ascend,
  contains,
  curry,
  filter,
  find,
  groupBy,
  head,
  isEmpty,
  isNil,
  join,
  keys,
  last,
  length,
  map,
  match,
  mergeAll,
  path,
  pathOr,
  pick,
  prepend,
  prop,
  propEq,
  range,
  reverse,
  sort,
  split,
  toPairs,
  toString,
} from 'ramda';
import { camelize, underscore } from 'inflection';

import { SQLWithValues } from './sql';
import { Schema } from '../../schema';
import { Field } from '../../schema/field';
import { Type } from '../../schema/type';
import { Relation } from '../../schema/relation';
import { validate } from '../../schema/validate';
import { DB } from '../../db';

import { findFieldByName } from '../../schema/type';
import {
  scalarFields as filterScalarFields,
  relationFields as filterRelationFields,
} from '../../schema/field';
import {
  RELATION_TYPE_HAS_MANY,
  findRelationByFieldName,
  isHasOne,
  isHasMany,
} from '../../schema/relation';
import { formatSqlWithBindParams, fromWhereInput } from '../where';
import { sqlType } from './meta';
import { tableNamesSql } from './sql';
import { migrate } from './migrate';
import { parseDBSchema, dropAllTables, createTables } from './ddl';
import { log } from '../../util/logging';
import { sleep } from '../../util/sleep';
import { concat } from '../../util/list';
import { ascendString, lowerFirstLetter } from '../../util/string';
import { mapKeys, merge, sortedValues } from '../../util/object';
import { upsertRelation } from './relations';

export type PGDBContext = {
  schemaName: string,
  schema?: Schema,
  client: Client,
  idFn: () => string,
};

export const PG_URL = process.env.PG_URL || 'postgresql://postgres:password@localhost:5432/postgres';

const mapIndexed = rAddIndex(map);

export const connect = async (
  connectionString: string = PG_URL,
  attempts: number = 30,
  interval: number = 1000,
): Promise<Client> => {
  if (attempts === 0) throw new Error(`Failed to connect to ${PG_URL}`);
  try {
    const client = new Client({ connectionString });
    await client.connect();
    return client;
  } catch (e) {
    await sleep(interval);
    return connect(connectionString, attempts - 1, interval);
  }
};

export const columnNamesToGQLNames = curry((type: Type, dbData: any) => {
  if (!dbData) return null;
  return mapKeys((k: string) => {
    const field = find(propEq('columnName', k), type.fields);
    return (field) ? field.name : k;
  }, dbData);
});

// export const gqlFieldNameToColumnName = curry((type: Type, ))

export const execStmt = async (db: Client, q: SQLWithValues): Promise<QueryResult> => {
  try {
    log.info({ sql: formatSql(q.sql), values: q.values });
    return await db.query(q.sql, q.values);
  } catch (e) {
    // console.error('execStmt', e);
    throw e;
  }
};

export const beginTransaction = (dbCtx: PGDBContext): Promise<QueryResult> =>
  execStmt(dbCtx.client, { sql: 'begin transaction', values: [] });

export const commitTransaction = (dbCtx: PGDBContext): Promise<QueryResult> =>
  execStmt(dbCtx.client, { sql: "commit transaction", values: [] });

export const rollbackTransaction = (dbCtx: PGDBContext): Promise<QueryResult> =>
  execStmt(dbCtx.client, { sql: 'rollback', values: [] });

export const tableNames = async (dbCtx: PGDBContext): Promise<string[]> =>
  map(r => (prop('tablename', r) || '').toString(), (await execStmt(dbCtx.client, tableNamesSql(dbCtx))).rows);

export const makeWhereClause = (type: Type, where: any, bindParamStartIndex: number = 0): [string, any[]] => {
  const whereAST = fromWhereInput(type, where);
  const [whereSql, whereVals] = formatSqlWithBindParams(whereAST);
  const replaceFn = () => { let i = bindParamStartIndex; return () => `$${++i}`; };
  const pgWhereSql = whereSql.replace(/\?/g, replaceFn());
  const whereClause = (isEmpty(pgWhereSql)) ? '' : `where ${pgWhereSql}`;
  return [whereClause, whereVals];
};

export const fieldFromDBMetadata = (parentType: Type, row: any): Field => ({
  parentType,
  name: camelize(row.column_name, true),
  columnName: row.column_name,
  // normalize to varchar instead of 'character varying'
  sqlType: (row.data_type === 'character varying') ? 'varchar' : row.data_type,
  // Todo: set GraphQL type
  type: gqlType({
    columnName: camelize(row.column_name, true),
    sqlType: (row.data_type === 'character varying') ? 'varchar' : row.data_type
  }),
  isList: false,
  listItemsNullable: false,
  size: row.character_maximum_length || 0,
  nullable: (row.is_nullable === 'YES'),
  default: castValue({
    columnName: camelize(row.column_name, true),
    sqlType: (row.data_type === 'character varying') ? 'varchar' : row.data_type
  }, row.column_default),
  // Todo
  unique: false,
  // Todo
  primaryKey: false,
  isRelation: false,
  directives: [],
  arguments: [],
});

export const gqlType = (field: Field | { columnName: string, sqlType: string }): string => {
  if (field.columnName === 'id') return 'ID';
  if (field.sqlType.match(/(char|varchar|text)/i)) return 'String';
  if (field.sqlType.match(/boolean/i)) return 'Boolean';
  if (field.sqlType.match(/smallint|integer|bigint|decimal|numeric|serial/i)) return 'Integer';
  if (field.sqlType.match(/float|real|double precision/i)) return 'Float';
  if (field.sqlType.match(/jsonb/i)) return 'JSON';
  if (field.sqlType.match(/date/i)) return 'Date';
  if (field.sqlType.match(/time with time zone/i)) return 'Time';
  if (field.sqlType.match(/timestamp with time zone/i)) return 'DateTime';
  return '<Custom>';
  // throw new Error(`Don't know how to convert ${field.sqlType} to GraphQL type`);
};

export const castValue = (field: Field | { columnName: string, sqlType: string }, value: any): any => {
  if (isNil(value)) return null;
  switch (gqlType(field)) {
    case 'ID': return value.toString();
    case 'String': return value.toString();
    case 'Integer': return parseInt(value, 10);
    case 'Float': return parseFloat(value);
    case 'Boolean': return (value === 'true');
    case 'JSON': return value;
    case 'Date':
    case 'Time':
    case 'DateTime':
      return value;
    default:
      throw new Error(`Unable to cast value: unknown type ${field.sqlType}`);
  }
};

export const formatOrderByInput = (orderBy: string = '', tableName: string = ''): string =>
  (isEmpty(orderBy) ? '' : `order by ${tableName ? `${tableName}.` : ''}${join(' ', split('_', orderBy))}`);

export const findOne = async (dbCtx: PGDBContext, type: Type, where: any): Promise<any> => {
  if (length(keys(where)) === 0) throw new Error('Must specify at least one field in where spec');
  const [whereClause, whereVals] = makeWhereClause(type, where);
  const sql = `select * from ${dbCtx.schemaName}.${type.tableName} ${whereClause}`;
  const result = await execStmt(dbCtx.client, { sql, values: whereVals });
  return columnNamesToGQLNames(type, head(result.rows));
  // return head(result.rows);
};

export const findThrough = async (
  dbCtx: PGDBContext,
  relation: Relation,
  srcIds: string[],
  where?: any,
  orderBy?: any,
  skip?: number,
  after?: string,
  before?: string,
  first?: number,
  last?: number,
): Promise<any> => {
  const srcColName = underscore(`${lowerFirstLetter(relation.srcType.name)}Id`);
  const targetColName = underscore(`${lowerFirstLetter(relation.targetType.name)}Id`);
  const targetTableName = `${dbCtx.schemaName}.${relation.targetType.tableName}`;
  const relationTableName = `${dbCtx.schemaName}.${relation.tableName}`;
  if (first && last) throw new Error('Including first and last in a query is not supported');
  const afterFilter = after && isNil(last) ? { id_gt: { tableName: targetTableName, value: after } } : {};
  const beforeFilter = before && isNil(first) ? { id_lt: { tableName: targetTableName, value: before } } : {};
  const paginatedWhere = mergeAll([where || {}, afterFilter, beforeFilter]);
  const bindVals = map(i => `$${i}`, range(1, length(srcIds) + 1));
  const whereAST = fromWhereInput(relation.targetType, paginatedWhere);
  const [whereSql, whereVals] = formatSqlWithBindParams(whereAST);
  const replaceFn = () => { let i = length(srcIds); return () => `$${++i}`; };
  const pgWhereSql = whereSql.replace(/\?/g, replaceFn());
  const whereClauseRemainder = (isEmpty(pgWhereSql)) ? '' : `and ${pgWhereSql}`;
  const order = orderBy || ((first || after) ? 'id_ASC' : (last || before) ? 'id_DESC' : '');
  const orderSql = formatOrderByInput(order, targetTableName);
  const limit = first || last;
  const limitClause = limit ? `limit ${limit}` : '';
  const sql = `
    select ${targetTableName}.*, ${srcColName} from
      ${targetTableName} inner join ${relationTableName} jt
    on
      ${relation.targetType.tableName}.id = jt.${targetColName}
    where
      jt.${srcColName} in (${join(", ", bindVals)})
      ${whereClauseRemainder}
    ${orderSql}
    ${limitClause}
  `;
  // Type coercion hack to make compile error go away.
  // Todo: remove : any type on next line and find better
  //       way to resolve the compile error
  const bindValues: any = concat(srcIds, whereVals);
  const result = await execStmt(dbCtx.client, {
    sql,
    values: bindValues,
  });

  const gqlResults = map(columnNamesToGQLNames(relation.targetType), result.rows);
  const groupedEntities = groupBy(row => row[srcColName].toString(), gqlResults);
  const entities = map(id => {
    const entities = groupedEntities[id] || [];
    return last ? reverse(entities) : entities;
  }, srcIds);

  return entities;
};

export const findOneThrough = async (
  dbCtx: PGDBContext,
  relation: Relation,
  srcIds: string[],
): Promise<any> => map(head, await findThrough(dbCtx, relation, srcIds));

export const findOneInRelation = async (
  dbCtx: PGDBContext,
  srcId: string,
  srcField: Field,
  targetType: Type,
  relation: Relation,
): Promise<any> => {
  const srcColName = underscore(`${lowerFirstLetter(relation.srcType.name)}Id`);
  const targetColName = underscore(`${lowerFirstLetter(relation.targetType.name)}Id`);
  const sql = `
    select * from
      ${dbCtx.schemaName}.${targetType.tableName} inner join ${dbCtx.schemaName}.${relation.tableName} jt
    on
       ${targetType.tableName}.id = jt.${targetColName}
    where
      jt.${srcColName} = $1
    limit 1
  `;
  const result = await execStmt(dbCtx.client, { sql, values: [srcId] });
  return head(result.rows);
};

export const findManyInRelation = async (
  dbCtx: PGDBContext,
  srcId: string,
  srcField: Field,
  targetType: Type,
  relation: Relation,
): Promise<any> => {
  const srcColName = underscore(`${lowerFirstLetter(relation.srcType.name)}Id`);
  const targetColName = underscore(`${lowerFirstLetter(relation.targetType.name)}Id`);
  const sql = `
    select * from
      ${dbCtx.schemaName}.${targetType.tableName} inner join ${dbCtx.schemaName}.${relation.tableName} jt
    on
       ${targetType.tableName}.id = jt.${targetColName}
    where
      jt.${srcColName} = $1
  `;
  const result = await execStmt(dbCtx.client, {
    sql,
    values: [srcId],
  });
  return result.rows;
};

export const findMany = async (
  dbCtx: PGDBContext,
  type: Type,
  where: any,
  orderBy?: any,
  skip?: number,
  after?: string,
  before?: string,
  first?: number,
  last?: number,
): Promise<any> => {
  if (first && last) throw new Error('Including first and last in a query is not supported');
  const afterFilter = after && isNil(last) ? { id_gt: after } : {};
  const beforeFilter = before && isNil(first) ? { id_lt: before } : {};
  const paginatedWhere = mergeAll([where, afterFilter, beforeFilter]);
  const [whereClause, whereVals] = makeWhereClause(type, paginatedWhere);
  // eslint-disable-next-line
  const order = orderBy ? orderBy : (first ? 'id_ASC' : (last ? 'id_DESC' : ''));
  const orderSql = formatOrderByInput(order);
  const limit = first || last;
  const limitClause = limit ? `limit ${limit}` : '';
  const sql = `select * from ${dbCtx.schemaName}.${type.tableName} ${whereClause} ${orderSql} ${limitClause}`;
  const result = await execStmt(dbCtx.client, { sql, values: whereVals });
  const entities = last ? reverse(result.rows) : result.rows;
  return map(columnNamesToGQLNames(type), entities);
};

export const count = async (
  dbCtx: PGDBContext,
  type: Type,
  where: any,
  orderBy?: any,
  skip?: number,
  after?: string,
  before?: string,
  first?: number,
  last?: number,
): Promise<any> => {
  if (first && last) throw new Error('Including first and last in a query is not supported');
  const afterFilter = after && isNil(last) ? { id_gt: after } : {};
  const beforeFilter = before && isNil(first) ? { id_lt: before } : {};
  const paginatedWhere = mergeAll([where, afterFilter, beforeFilter]);
  const [whereClause, whereVals] = makeWhereClause(type, paginatedWhere);
  // eslint-disable-next-line
  const sql = `select count(id) from ${dbCtx.schemaName}.${type.tableName} ${whereClause}`;
  const result = await execStmt(dbCtx.client, { sql, values: whereVals });
  return pathOr(0, ['rows', '0', 'count'], result);
};

const disconnectHasOneRelation = async (
  dbCtx: PGDBContext,
  relation: Relation,
  srcId: string,
) => {
  const removeSql = `delete from ${dbCtx.schemaName}.${relation.tableName} where ${relation.srcJoinColName} = $1`;
  await execStmt(dbCtx.client, { sql: removeSql, values: [srcId] });
};

const disconnectHasManyRelation = async (
  dbCtx: PGDBContext,
  relation: Relation,
  srcId: string,
  where: any,
) => {
  const relations = await findMany(dbCtx, relation.targetType, { OR: where });
  const targetIds = map(r => r.id, relations);
  const removeWhere = { [`${relation.targetJoinColName}_in`]: targetIds };
  const [whereClause, whereVals] = makeWhereClause(relation.targetType, removeWhere);
  const removeSql = `delete from ${dbCtx.schemaName}.${relation.tableName} ${whereClause}`;
  await execStmt(dbCtx.client, { sql: removeSql, values: whereVals });
};

const connectRelation = async (
  dbCtx: PGDBContext,
  relation: Relation,
  srcId: string,
  targetId: string,
) => {
  if (isHasOne(relation)) await disconnectHasOneRelation(dbCtx, relation, srcId);

  // set new relation/join data
  const joinSql = `insert into ${dbCtx.schemaName}.${relation.tableName} (id, ${relation.srcJoinColName}, ${relation.targetJoinColName}) values ($1, $2, $3)`;
  const vals = [dbCtx.idFn(), srcId, targetId];
  await execStmt(dbCtx.client, { sql: joinSql, values: vals });
};

export const insertSql = (
  dbCtx: PGDBContext,
  type: Type,
  data: any,
  sortedFieldNames: string[],
): string => {
  const vals = sortedValues(sortedFieldNames, data);
  const vars = map(i => `$${i}`, range(1, length(vals) + 1));
  const formattedFieldNames = join(', ', sortedFieldNames);
  const formattedVars = join(', ', vars);
  return `insert into ${dbCtx.schemaName}.${type.tableName} (${formattedFieldNames}) values (${formattedVars})`;
};

export const insert = curry(async (dbCtx: PGDBContext, type: Type, data: any): Promise<any> => {
  try {
    beginTransaction(dbCtx);

    validate(type, data);

    const dataWithId = merge(data, { id: dbCtx.idFn() });
    const byName = ascendString(prop('name'));
    const fieldNamesWithData = keys(data);
    const scalarFields = filter(field => contains(field.name, fieldNamesWithData) || field.name === 'id', filterScalarFields(type));
    const sortedFields = sort(byName, scalarFields);
    const sortedFieldNames = map(f => f.name, sortedFields);
    const sortedDBFieldNames = map(f => f.columnName, sortedFields);
    const qValues = sortedValues(sortedFieldNames, dataWithId);
    // hack to address the issue found here: https://github.com/brianc/node-postgres/issues/1519
    const stringifyJSONArray = (v: any) => Array.isArray(v) ? JSON.stringify(v) : v;
    const safeValues = map(stringifyJSONArray, qValues);
    const sql = insertSql(dbCtx, type, dataWithId, sortedDBFieldNames);
    const result = await execStmt(dbCtx.client, { sql: `${sql} returning *`, values: safeValues });
    const entity = columnNamesToGQLNames(type, head(result.rows));
    const relationFields = filterRelationFields(type);

    const relationData = await P.all(map(async (field: Field) => {
      const relation = findRelationByFieldName(dbCtx.schema, type, field.name);
      const { targetType } = relation;

      const createRelationData = path([field.name, 'create'], data);
      if (createRelationData) {
        if (isHasOne(relation)) {
          const relationEntity = await insert(dbCtx, targetType, createRelationData);
          await connectRelation(dbCtx, relation, entity.id, relationEntity.id);
          return { [field.name]: relationEntity };
        } else if (relation.relationType === RELATION_TYPE_HAS_MANY) {
          // in the has many case, 
          const hasManyCreateData: any[] = path([field.name, 'create'], data) || [];
          const relationEntities = await P.all(map(insert(dbCtx, targetType), hasManyCreateData));
          await P.all(map(
            async re => connectRelation(dbCtx, relation, entity.id, re.id),
            relationEntities,
          ));
          return { [field.name]: relationEntities };
        }
      }

      const connectFilter = path([field.name, 'connect'], data);
      if (connectFilter) {
        if (isHasOne(relation)) {
          const relationEntity = await findOne(dbCtx, relation.targetType, connectFilter);
          await connectRelation(dbCtx, relation, entity.id, relationEntity.id);
          return { [field.name]: relationEntity };
        } else if (relation.relationType === RELATION_TYPE_HAS_MANY) {
          // [{ id: '1' }, { name: 'Home' }
          // -->
          // update addresses set ... where 
          const where = { OR: connectFilter };
          // Todo: guard against empty connectFilter list
          const relationEntities = await findMany(dbCtx, relation.targetType, where);
          const _connect = async (r: any)  => connectRelation(dbCtx, relation, entity.id, r.id);
          await P.all(map(_connect, relationEntities));
          return { [field.name]: relationEntities };
        }
      }
      // default case
      return {};
    }, relationFields));
    commitTransaction(dbCtx);
    return mergeAll(prepend(entity, relationData));
  } catch (e) {
    rollbackTransaction(dbCtx);
    throw e;
  }
});

export const update = async (
  dbCtx: PGDBContext,
  type: Type,
  data: any,
  where: any,
): Promise<any> => {
  validate(type, data);

  const scalarFields = filterScalarFields(type);
  const relationFields = filterRelationFields(type);
  const scalarFieldNames = map(f => f.name, scalarFields);
  const relationFieldNames = map(f => f.name, relationFields);
  const scalarData = pick(scalarFieldNames, data);
  const relationData = pick(relationFieldNames, data);

  let entity: any = null;
  if (length(keys(scalarData)) > 0) {
    const updates = toPairs(scalarData);
    const columns = map(u => {
      const field = find(propEq('name', u[0]), type.fields);
      return field ? field.columnName : u[0];
    }, updates);
    const values = map(u => {
      // this addresses the issue found here: https://github.com/brianc/node-postgres/issues/1519
      const field = find(propEq('name', u[0]), type.fields);
      return (field && field.sqlType === 'jsonb')
        ? JSON.stringify(u[u.length - 1])
        : u[u.length - 1];
    }, updates);
    const [whereClause, whereVals] = makeWhereClause(type, where, length(values));
    const setStr = join(', ', mapIndexed((c, i) => `${c} = $${i + 1}`, columns));
    // Todo: implement where clause (won't always be 'id')
    const sql = `update ${dbCtx.schemaName}.${type.tableName} set ${setStr} ${whereClause} returning *`;
    const vals: any = concat(values, whereVals);
    const result = await execStmt(dbCtx.client, { sql, values: vals });
    entity = columnNamesToGQLNames(type, head(result.rows));
  } else {
    entity = await findOne(dbCtx, type, where);
  }

  const relationUpdates = await P.all(map(async relationFieldName => {
    const f = findFieldByName(type, relationFieldName);
    const relation = findRelationByFieldName(dbCtx.schema, type, relationFieldName);

    const createRelationData = path([f.name, 'create'], data);
    if (createRelationData) {
      if (isHasOne(relation)) {
        const relationEntity = await insert(dbCtx, relation.targetType, createRelationData);
        await connectRelation(dbCtx, relation, entity.id, relationEntity.id);
        return { [f.name]: relationEntity };
      } else if (relation.relationType === RELATION_TYPE_HAS_MANY) {
        const hasManyCreateData: any[] = path([f.name, 'create'], data) || [];
        const insertedEntities = await P.all(map(async insertData => {
          const relationEntity = await insert(dbCtx, relation.targetType, insertData);
          await connectRelation(dbCtx, relation, entity.id, relationEntity.id);
        }, hasManyCreateData));
        return {
          [f.name]: insertedEntities,
        };
      }
    }

    const connectFilter = path([f.name, 'connect'], data);
    if (connectFilter) {
      if (isHasOne(relation)) {
        const relationEntity = await findOne(dbCtx, relation.targetType, connectFilter);
        await connectRelation(dbCtx, relation, entity.id, relationEntity.id);
        return { [f.name]: relationEntity };
      } else if (isHasMany(relation)) {
        const where = { OR: connectFilter };
        // Todo: guard against empty connectFilter list
        const relationEntities = await findMany(dbCtx, relation.targetType, where);
        const _connect = async (r: any) => connectRelation(dbCtx, relation, entity.id, r.id);
        await P.all(map(_connect, relationEntities));
        return { [f.name]: relationEntities };
      }
    }

    const disconnectFilter = path([f.name, 'disconnect'], data);
    if (disconnectFilter) {
      if (isHasOne(relation)) {
        await disconnectHasOneRelation(dbCtx, relation, entity.id);
        return { [f.name]: null };
      } else if (isHasMany(relation)) {
        await disconnectHasManyRelation(dbCtx, relation, entity.id, disconnectFilter);
        return { [f.name]: [] };
      }
    }

    const deleteFilter = path([f.name, 'delete'], data);
    if (deleteFilter) {
      if (isHasOne(relation)) {
        const findSql = `select ${relation.targetJoinColName} from ${dbCtx.schemaName}.${relation.tableName} where ${relation.srcJoinColName} = $1`;
        const result = await execStmt(dbCtx.client, { sql: findSql, values: [entity.id] });
        const removeJoinSql = `delete from ${dbCtx.schemaName}.${relation.tableName} where ${relation.srcJoinColName} = $1`;
        await execStmt(dbCtx.client, { sql: removeJoinSql, values: [entity.id] });
        const removeEntitySql = `delete from ${dbCtx.schemaName}.${relation.targetType.tableName} where id = $1`;
        await execStmt(dbCtx.client, {
          sql: removeEntitySql,
          values: [result.rows[0][relation.targetJoinColName]],
        });
        return { [f.name]: null };
      } else if (isHasMany(relation)) {
        await disconnectHasManyRelation(dbCtx, relation, entity.id, deleteFilter);
        await del(dbCtx, relation.targetType, { OR: deleteFilter });
        return { [f.name]: [] };
      }
    }

    const updateRelationData = path([f.name, 'update'], data);
    if (updateRelationData) {
      if (isHasOne(relation)) {
        const findSql = `select ${relation.targetJoinColName} from ${dbCtx.schemaName}.${relation.tableName} where ${relation.srcJoinColName} = $1`;
        const result = await execStmt(dbCtx.client, { sql: findSql, values: [entity.id] });
        const relationId = result.rows[0][relation.targetJoinColName];
        const relationEntity = await update(
          dbCtx,
          relation.targetType,
          updateRelationData,
          { id: relationId },
        );
        return { [f.name]: relationEntity };
      } else if (isHasMany(relation)) {
        const hasManyUpdateRelationData: any[] = path([f.name, 'update'], data) || [];
        await P.all(map(updateSpec => {
          const { data, where } = updateSpec;
          return update(dbCtx, relation.targetType, data, where);
        }, hasManyUpdateRelationData));
      }
    }

    const upsertRelationData = path([f.name, 'upsert'], data);
    if (upsertRelationData) {
      if (isHasOne(relation)) {
        const relationEntity = await upsertRelation(
          dbCtx,
          relation,
          where,
          entity,
          upsertRelationData,
        );
        return { [f.name]: relationEntity };
      } else if (isHasMany(relation)) {
        const hasManyUpsertRelationData: any[] = path([f.name, 'upsert'], data) || [];
        const relationEntities = await P.all(map(async upsertSpec => {
          const { where, update, create } = upsertSpec;
          const re = await upsert(dbCtx, relation.targetType, where, create, update);
          try {
            await connectRelation(dbCtx, relation, entity.id, re.id);
          } catch (e) {
            if (!match(/duplicate key/, e.message)) throw e;
          }
          return re;
        }, hasManyUpsertRelationData));
        return { [f.name]: relationEntities };
      }
    }

    return {};
  }, keys(relationData)));

  return mergeAll(prepend(entity, relationUpdates));
};

export const upsert =
  // eslint-disable-next-line
  async (dbCtx: PGDBContext, type: Type, where: any, _create: any, _update: any): Promise < any > => {
    const entity = await findOne(dbCtx, type, where);
    return (entity)
      ? update(dbCtx, type, _update, where)
      : insert(dbCtx, type, _create);
  };

export const del = async (dbCtx: PGDBContext, type: Type, where: any): Promise<any> => {
  const whereAST = fromWhereInput(type, where);
  const [whereSql, whereVals] = formatSqlWithBindParams(whereAST);
  const replaceFn = () => { let i = 0; return () => `$${++i}`; };
  const pgWhereSql = whereSql.replace(/\?/g, replaceFn());
  const whereClause = (isEmpty(pgWhereSql)) ? '' : `where ${pgWhereSql}`;
  const sql = `delete from ${dbCtx.schemaName}.${type.tableName} ${whereClause} returning *`;
  const result = await execStmt(dbCtx.client, { sql, values: whereVals });
  return head(result.rows);
};

// export const updateMany = async (
//   dbCtx: PGDBContext,
//   type: Type,
//   data: any,
//   where: any,
// ): Promise<any> => {
//   const updates = toPairs(data);
//   const columns = map(head, updates);
//   const values = map(last, updates);
//   const [whereClause, whereVals] = makeWhereClause(where, length(values));
//   const setStr = join(', ', mapIndexed((c, i) => `${c} = $${i + 1}`, columns));
//   const sql = `update ${dbCtx.schemaName}.${type.tableName} set ${setStr} ${whereClause} returning *`;
//   const result = await execStmt(dbCtx.client, { sql, values: concat(values, whereVals) });
//   return { count: result.rowCount };
// };

export const delMany = async (dbCtx: PGDBContext, type: Type, where: any): Promise<any> => {
  const whereAST = fromWhereInput(type, where);
  const [whereSql, whereVals] = formatSqlWithBindParams(whereAST);
  const replaceFn = () => { let i = 0; return () => `$${++i}`; };
  const pgWhereSql = whereSql.replace(/\?/g, replaceFn());
  const whereClause = (isEmpty(pgWhereSql)) ? '' : `where ${pgWhereSql}`;
  const sql = `delete from ${dbCtx.schemaName}.${type.tableName} ${whereClause} returning *`;
  const result = await execStmt(dbCtx.client, { sql, values: whereVals });
  return { count: result.rowCount };
};

export const init = (dbCtx: PGDBContext): DB => ({
  parseDBSchema: (): Promise<any> =>
    parseDBSchema(dbCtx),

  tableNames: (): Promise<any> =>
    tableNames(dbCtx),

  dropAllTables: (): Promise<any> =>
    dropAllTables(dbCtx),

  createTables: (schema: Schema): Promise<any> =>
    createTables(dbCtx, schema),

  migrate: (typedefs: string, schema: Schema): FutureInstance<{}, [boolean, string]> =>
    migrate(dbCtx, typedefs, schema),

  sqlType: (gqlType: string): SqlType =>
    sqlType(gqlType),

  findOne: (type: Type, where: any): Promise<any> =>
    findOne(dbCtx, type, where),

  findOneInRelation: (
    srcId: string,
    srcField: Field,
    targetType: Type,
    relation: Relation,
  ): Promise<any> =>
    findOneInRelation(dbCtx, srcId, srcField, targetType, relation),

  findManyInRelation: (
    srcId: string,
    srcField: Field,
    targetType: Type,
    relation: Relation,
  ): Promise<any> =>
    findManyInRelation(dbCtx, srcId, srcField, targetType, relation),

  find: (
    type: Type,
    where: any,
    orderBy: any,
    skip: number,
    after: string,
    before: string,
    first: number,
    last: number,
  ): Promise<any> =>
    findMany(dbCtx, type, where, orderBy, skip, after, before, first, last),

  count: (
    type: Type,
    where: any,
    orderBy: any,
    skip: number,
    after: string,
    before: string,
    first: number,
    last: number,
  ): Promise<any> =>
    count(dbCtx, type, where, orderBy, skip, after, before, first, last),

  findThrough: (
    relation: Relation,
    srcIds: string[],
    where?: any,
    orderBy?: any,
    skip?: number,
    after?: string,
    before?: string,
    first?: number,
    last?: number,
  ): Promise<any> =>
    findThrough(dbCtx, relation, srcIds, where, orderBy, skip, after, before, first, last),

  findOneThrough: (
    relation: Relation,
    srcIds: string[],
  ): Promise<any> =>
    findOneThrough(dbCtx, relation, srcIds),

  insert: (type: Type, data: any): Promise<any> =>
    insert(dbCtx, type, data),

  update: (type: Type, data: any, where: any): Promise<any> =>
    update(dbCtx, type, data, where),

  // eslint-disable-next-line
  upsert: (type: Type, where: any, create: any, update: any): Promise<any> =>
    upsert(dbCtx, type, where, create, update),

  del: (type: Type, where: any): Promise<any> =>
    del(dbCtx, type, where),

  // updateMany: (type: Type, data: any, where: any): Promise<any> =>
  //   updateMany(dbCtx, type, data, where),

  delMany: (type: Type, where: any): Promise<any> =>
    delMany(dbCtx, type, where),

  sql: (sql: string, values: any[]): Promise<any> =>
    dbCtx.client.query(sql, values),
});
