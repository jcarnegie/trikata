// @flow
import P from 'bluebird';
import {
  FutureInstance,
  attempt,
  attemptP,
  chain,
  map,
  mapRej,
  parallel,
  fork,
} from 'fluture';
import {
  append,
  assocPath,
  compose,
  contains,
  curry,
  differenceWith,
  filter,
  find,
  flatten,
  has,
  isNil,
  join,
  length,
  map as rMap,
  not,
  prop,
  propEq,
  reduce,
  unnest,
  values,
} from 'ramda';

import { PGDBContext } from './index';
import { Schema } from '../../schema';
import { Type } from '../../schema/type';
import { Field } from '../../schema/field';
import { Index } from '../../schema/idx';
import { Relation } from '../../schema/relation';

import { stringify } from '../../util/string';
import { concat } from '../../util/list';
import { createSchemaSql } from './sql';
import { execStmt } from './index';
import {
  parseDBSchema,
  createJoinTables,
  createTablesFromTypes,
  addColumns,
  addIndexes,
  updateColumns,
  dropColumns,
  dropTables,
  dropIndexes,
  renameRelationForeignKeys,
  renameTable,
} from './ddl';
import { scalarFields } from '../../schema/field';
import { createRelationType, defaultName } from '../../schema/relation';

const notNil = compose(not, isNil);
const cmpByName = (x: any, y: any) => x.name === y.name;
const cmpTypes = (x: any, y: any) => x.tableName === y.tableName;

const renamableRelations = (
  schema: Schema,
  dbSchema: Schema,
): {old: Relation, new: Relation}[] => {
  // Filter for relations where the relation types have no other relation between them
  const filterSingleTypeRelations = filter((relation: Relation) => {
    const typeACount = reduce((acc, r) => {
      const hasType = r.srcType.name === relation.srcType.name ||
        r.targetType.name === relation.srcType.name;
      return acc + ((hasType) ? 1 : 0);
    }, 0, schema.relations);
    const typeBCount = reduce((acc, r) => {
      const hasType = r.srcType.name === relation.targetType.name ||
        r.targetType.name === relation.targetType.name;
      return acc + ((hasType) ? 1 : 0);
    }, 0, schema.relations);
    return typeACount === 1 && typeBCount === 1;
  });
  const singleTypeRelations = filterSingleTypeRelations(schema.relations);
  const relationMatcher = curry((r1: Relation, r2: Relation): boolean =>
    (
      (r1.srcType.name === r2.types[0].name && r1.targetType.name === r2.types[1].name)
      ||
      (r1.srcType.name === r2.types[1].name && r1.targetType.name === r2.types[0].name)
    ) && (
      r1.name !== r2.name && (
        r1.name === defaultName(r1.srcType, r1.targetType) ||
        r2.name === defaultName(r1.srcType, r1.targetType) ||
        notNil(r1.renameTo)
      )
    ));

  const relationsToRename = reduce((acc: any, r) => {
    const matchingRelation = find(relationMatcher(r), dbSchema.relations);
    if (matchingRelation) {
      return append({ old: matchingRelation, new: r }, acc);
    }
    return acc;
  }, [], singleTypeRelations);

  return relationsToRename;
};

export const validateRelations = (
  context: any,
  schema: Schema,
  dbSchema: Schema,
): any => {
  // 1. validate GQL schema (schema)
  // 1.1. invalid case #1: if Type has > 1 relation, all relations must have @relation w/ name arg
  // 1.1. invalid case #2: 
  // 1.1. invalid case #3: 
  // 2. validate we can migrate from dbSchema to schema

  type TypeRelations = {
    srcType: Type,
    relations: Relation[],
  };

  const relationData = reduce((data: any, r) => {
    const srcType = r.srcField.parentType;

    if (!data[srcType.name]) {
      data[srcType.name] = {
        srcType,
        relations: [],
      };
    }
    const relations = append(r, data[srcType.name].relations);
    return assocPath([srcType.name, 'relations'], relations, data);
  }, {}, schema.relations);

  // console.log(relationData);
  // console.log(values(relationData));

  const hasValidRelationDirectiveName = (r: Relation): boolean => {
    const d = find(propEq('name', 'relation'), r.srcField.directives);
    return notNil(d) && notNil(d.args.name);
  };

  const duplicatedRelations = (rd: TypeRelations): any => {
    const reducerFn = (data: any, r: Relation): any => {
      const key = `${r.srcType.name}:${r.targetType.name}:${r.name}`;
      if (!has(key, data)) data[key] = [];
      data[key] = append(r, data[key]);
      return data;
    };
    const duplicatesData = reduce(reducerFn, {}, rd.relations);
    return filter(data => length(data) > 1, values(duplicatesData));
  };

  const allDuplicatedRelations = unnest(rMap(duplicatedRelations, values(relationData)));
  
  if (allDuplicatedRelations.length > 0) {
    const duplicateErrors = join('\n', rMap((dups: Relation[]) => {
      const msg = (name: string) =>
        `    * The relation field '${name}' must specify a '@relation' directive: '@relation(name: "MyRelation")'`;
      const messages = join('\n', rMap(r => msg(r.srcField.name), dups));
      return `  ${dups[0].srcType.name}\n${messages}`;
    }, allDuplicatedRelations));

    throw new Error(`
Errors:

${duplicateErrors}
`);
  };

  

//   const typeHasInvalidRelations = (rd: TypeRelations): boolean =>
//     length(rd.relations) > 1 &&
//     !all(hasValidRelationDirectiveName, rd.relations);

//   const invalidTypeRelations = filter(typeHasInvalidRelations, values(relationData));

//   if (invalidTypeRelations.length > 0) {
//     const invalidTypeRelation = invalidTypeRelations[0];
//     const invalidType = invalidTypeRelation.srcType;
//     const invalidRelations = filter(
//       r => !hasValidRelationDirectiveName(r),
//       invalidTypeRelation.relations,
//     );
//     throw new Error(`
// Errors:

//   ${invalidType.name}
// ${join('\n', rMap(ir => `    * The relation field '${ir.srcField.name}' must specify a '@relation' directive: '@relation(name: "MyRelation")'`, invalidRelations))}
// `);
//   }
};

export const addNewTables = async (
  dbCtx: PGDBContext,
  context: any,
  schema: Schema,
  dbSchema: Schema,
): Promise<any> => {
  const missingDBTypes = differenceWith(cmpTypes, schema.types, dbSchema.types);
  return createTablesFromTypes(dbCtx, missingDBTypes);
};

/**
 *
 */
export const renameRelationTables = async (
  dbCtx: PGDBContext,
  context: any,
  schema: Schema,
  dbSchema: Schema,
): Promise<any> => {
  const renamePairs = renamableRelations(schema, dbSchema);
  return P.all(rMap(async p => {
    // rename the relation tables
    await renameTable(dbCtx, p.old.tableName, p.new.tableName);
    // rename the relation table foreign key constraints
    await renameRelationForeignKeys(dbCtx, p.old, p.new);
  }, renamePairs));
};

export const addRelationTables = async (
  dbCtx: PGDBContext,
  context: any,
  schema: Schema,
  dbSchema: Schema,
): Promise<any> => {
  const missingDBRelations = differenceWith(cmpByName, schema.relations, dbSchema.relations);
  const renamableNames = rMap(p => p.new.name, renamableRelations(schema, dbSchema));
  const filteredMissingDBRelations = filter(r => !contains(r.name, renamableNames), missingDBRelations);
  await createJoinTables(dbCtx, filteredMissingDBRelations);
  const types = rMap(createRelationType, missingDBRelations);
  const typeIndexes: Index[][] = rMap(prop('indexes'), types);
  const indexes = unnest(typeIndexes);
  return addIndexes(dbCtx, indexes);
};

export const addNewColumns = async (
  dbCtx: PGDBContext,
  context: any,
  schema: Schema,
  dbSchema: Schema,
): Promise<any> => {
  const missing: Field[][] = rMap(type => {
    const dbType = find(propEq('name', type.name), dbSchema.types);
    if (!dbType) return [];
    return filter(f => isNil(find(propEq('columnName', f.columnName), dbType.fields)), scalarFields(type));
  }, schema.types)
  // Todo: fix: casting to any is cheap hack to suppress TS compile error
  const missingDBFields: any = flatten(missing);

  return addColumns(dbCtx, missingDBFields);
};

export const addNewIndexes = async (
  dbCtx: PGDBContext,
  context: any,
  schema: Schema,
  dbSchema: Schema,
): Promise<any> => {
  // Todo: fix: casting to any is cheap hack to suppress TS compile error
  const missingDBIndexes: any = flatten(rMap(type => {
    const dbType = find(propEq('name', type.name), dbSchema.types);
    if (!dbType) return [];
    return filter(idx => isNil(find(propEq('name', idx.name), dbType.indexes)), type.indexes);
  }, schema.types));

  return addIndexes(dbCtx, missingDBIndexes);
};

export const updateExistingColumns = async (
  dbCtx: PGDBContext,
  context: any,
  schema: Schema,
  dbSchema: Schema,
): Promise<any> => {
  // Changes that can be made to a field / dbCtx.client column
  //  1) change in type
  //  2) rename (as opposed to drop existing column, add new column, resulting in potential data loss)
  //  3) set / drop not null
  //  4) set / drop default
  //  5) change type size (i.e. varchar (255) --> varchar (80))
  // Todo: handle rename separately since we won't necessarily find matching DB type if field name is changed
  //       Note: need to handle case where new field name exists in DB
  const fieldUpdates = reduce((updates: any, type: Type) => {
    const dbType = find(propEq('name', type.name), dbSchema.types);
    if (!dbType) return updates;
    return concat(updates, reduce((_updates: any, field: Field) => {
      const dbField = find(propEq('name', field.name), dbType.fields);
      if (!dbField) return _updates;
      let changes: any = [];
      // change in type
      if (field.sqlType !== dbField.sqlType || field.size !== dbField.size) changes = append('type', changes);
      // change in nullability
      if (field.nullable !== dbField.nullable) changes = append('nullability', changes);
      // change in default
      if (field.default !== dbField.default) changes = append('default', changes);
      if (length(changes) === 0) return _updates;

      return append({
        changes,
        field,
        dbField,
      }, _updates);
    }, [], type.fields));
  }, [], schema.types);

  return updateColumns(dbCtx, fieldUpdates);
};

export const removeColumns = async (
  dbCtx: PGDBContext,
  context: any,
  schema: Schema,
  dbSchema: Schema,
): Promise<any> => {
  // Todo: fix: casting to any is cheap hack to suppress TS compile error
  const droppableDBFields: any = flatten(rMap(type => {
    const dbType = find(propEq('name', type.name), dbSchema.types);
    if (!dbType) return [];
    return filter(f => isNil(find(propEq('columnName', f.columnName), type.fields)), dbType.fields);
  }, schema.types));

  return dropColumns(dbCtx, droppableDBFields);
};

export const removeTables = async (
  dbCtx: PGDBContext,
  context: any,
  schema: Schema,
  dbSchema: Schema,
): Promise<any> => {
  const droppableDBTypes = differenceWith(cmpTypes, dbSchema.types, schema.types);
  const renamableNames = rMap(p => p.old.name, renamableRelations(schema, dbSchema));
  const typeIsRelation = curry((t: Type, r: Relation) => t.name === r.name);
  const filteredDroppableDBTypes = filter(t => !contains(t.name, renamableNames) && find(typeIsRelation(t), schema.relations) === undefined, droppableDBTypes);
  return dropTables(dbCtx, filteredDroppableDBTypes);
};

export const removeIndexes = async (
  dbCtx: PGDBContext,
  context: any,
  schema: Schema,
  dbSchema: Schema,
): Promise<any> => {
  // Todo: fix: casting to any is cheap hack to suppress TS compile error
  const droppableDBIndexes: any = flatten(rMap(type => {
    const dbType = find(propEq('name', type.name), dbSchema.types);
    if (!dbType) return [];
    return filter(idx => isNil(find(propEq('name', idx.name), type.indexes)), dbType.indexes);
  }, schema.types));

  await dropIndexes(dbCtx, droppableDBIndexes);
};

export const writeMigrationState = async (
  dbCtx: PGDBContext,
  context: any,
  typedefs: string,
  schema: Schema,
): Promise<any> => {
  try {
    const createSql = `
      create table if not exists ${dbCtx.schemaName}.__migrations (
        id bigserial,
        sdl text,
        schema jsonb,
        created_at timestamp
      )
    `;

    await dbCtx.client.query(createSql);

    const insertSql = `
      insert into ${dbCtx.schemaName}.__migrations
        (id, sdl, schema, created_at)
      values
        (default, $1, $2, now())
    `;

    await dbCtx.client.query(insertSql, [typedefs, stringify(schema)]);
  } catch (e) {
    console.error('Error creating migrations table:', e);
    throw e;
  }
};

export const migrate = (
  dbCtx: PGDBContext,
  typedefs: string,
  schema: Schema,
): FutureInstance<{}, [boolean, string]> => {
  let outBuffer = '';
  const out = (output: string) => outBuffer += output;
  const migrationContext = { out };

  const createSchemaFuture = attemptP(() => execStmt(dbCtx.client, createSchemaSql(dbCtx)));
  const dbSchemaFuture = attemptP(() => parseDBSchema(dbCtx));

  const doMigrate = (dbSchema: Schema) => parallel(1)([
    attempt(() => validateRelations(migrationContext, schema, dbSchema)),
    attemptP(() => addNewTables(dbCtx, migrationContext, schema, dbSchema)),
    attemptP(() => renameRelationTables(dbCtx, migrationContext, schema, dbSchema)),
    attemptP(() => addRelationTables(dbCtx, migrationContext, schema, dbSchema)),
    attemptP(() => addNewColumns(dbCtx, migrationContext, schema, dbSchema)),
    attemptP(() => addNewIndexes(dbCtx, migrationContext, schema, dbSchema)),
    attemptP(() => updateExistingColumns(dbCtx, migrationContext, schema, dbSchema)),
    attemptP(() => removeColumns(dbCtx, migrationContext, schema, dbSchema)),
    attemptP(() => removeTables(dbCtx, migrationContext, schema, dbSchema)),
    attemptP(() => removeIndexes(dbCtx, migrationContext, schema, dbSchema)),
    attemptP(() => writeMigrationState(dbCtx, migrationContext, typedefs, schema)),
  ]);

  const report = () => <[boolean, string]>[true, outBuffer];
  const reportErr = (e: any) => {
    return [false, e.message];
  };

  const parseDBSchemaF = chain(() => dbSchemaFuture)(createSchemaFuture);
  const migrateF = chain(doMigrate)(parseDBSchemaF);
  const successF = map(report)(migrateF);
  return <FutureInstance<{}, any>>mapRej(reportErr)(successF);
};
