// @flow

import { SqlType } from '../index';

export const sqlType = (gqlType: string): SqlType => {
  switch (gqlType) {
    case 'ID': return { sqlType: 'varchar', size: 36 };
    case 'String': return { sqlType: 'varchar', size: 255 };
    case 'Int': return { sqlType: 'integer', size: 0 };
    case 'Float': return { sqlType: 'float', size: 0 };
    case 'Boolean': return { sqlType: 'boolean', size: 0 };
    case 'JSON': return { sqlType: 'jsonb', size: 0 };
    case 'Date': return { sqlType: 'date', size: 0 };
    case 'Time': return { sqlType: 'time with time zone', size: 0 };
    case 'DateTime': return { sqlType: 'timestamp with time zone', size: 0 };
    default:
      return { sqlType: 'varchar', size: 255 };
  }
};
