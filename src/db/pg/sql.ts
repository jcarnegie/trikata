// @flow

import { compose, curry, isNil, join, map, not } from 'ramda';
import { underscore } from 'inflection';

import { PGDBContext } from './index';
import { Type } from '../../schema/type';
import { Field } from '../../schema/field';
import { Index } from '../../schema/idx';
import { Relation } from '../../schema/relation';

import { scalarFields } from '../../schema/field';
import { lowerFirstLetter } from '../../util/string';

const notNil = compose(not, isNil);

export type SQLWithValues = {
  sql: string,
  values: any[],
};

// For join tables:
export const JOIN_TABLE_METADATA_SQL = `
  select
    tc.constraint_name, tc.table_name, kcu.column_name, 
    ccu.table_name as foreign_table_name,
    ccu.column_name as foreign_column_name 
  from 
    information_schema.table_constraints as tc 
    join information_schema.key_column_usage as kcu
      on tc.constraint_name = kcu.constraint_name
    join information_schema.constraint_column_usage as ccu
      on ccu.constraint_name = tc.constraint_name
  where
    constraint_type = 'FOREIGN KEY' and
    tc.constraint_schema = $1
`;

// DDL
export const TABLE_METADATA_SQL = `
  select
    table_catalog,
    table_schema,
    table_name,
    column_name,
    data_type,
    character_maximum_length,
    is_nullable,
    column_default
  from
    information_schema.columns
  where
    table_schema = $1
  order by
    table_name, ordinal_position`;

// DDL
export const TABLE_INDEX_METADATA_SQL = `
  SELECT
    t.relname tablename,
    ix.relname indexname,
    indisunique as unique,
    indisprimary as primary,
    regexp_replace(pg_get_indexdef(indexrelid), '.*\\((.*)\\)', '\\1') as columns,
    am.amname as method
  FROM pg_index i
  JOIN pg_class t ON t.oid = i.indrelid
  JOIN pg_class ix ON ix.oid = i.indexrelid
  JOIN pg_am am ON am.oid = ix.relam
  where
    t.relname = $1
    and t.relnamespace = (select oid from pg_namespace where nspname = $2)
  `;

// DDL
export const colSpec = (f: Field): string => {
  const size = f.size > 0 ? `(${f.size})` : '';
  const nullable = f.nullable ? '' : 'not null';
  const unique = f.unique ? 'unique' : '';
  const pk = f.primaryKey ? 'primary key' : '';
  const defaultValue = notNil(f.default) ? `default ${f.default}` : '';
  return `${f.columnName} ${f.sqlType} ${size} ${nullable} ${unique} ${pk} ${defaultValue}`;
};

// DDL
export const tableMetadataSql = (dbCtx: PGDBContext): SQLWithValues => ({
  sql: TABLE_METADATA_SQL,
  values: [dbCtx.schemaName],
});

// DDL
export const tableIndexMetadataSql = (
  dbCtx: PGDBContext,
  tableName: string,
): SQLWithValues => ({
  sql: TABLE_INDEX_METADATA_SQL,
  values: [tableName, dbCtx.schemaName],
});

// DDL
export const joinTableMetadataSql = (dbCtx: PGDBContext): SQLWithValues => ({
  sql: JOIN_TABLE_METADATA_SQL,
  values: [dbCtx.schemaName],
});

// DDL
export const tableNamesSql = (dbCtx: PGDBContext): SQLWithValues => ({
  sql: 'select tablename from pg_tables where schemaname = $1',
  values: [dbCtx.schemaName],
});

// DDL
export const dropAllTablesSql = (dbCtx: PGDBContext): SQLWithValues => ({
  sql: `drop schema ${dbCtx.schemaName} cascade`,
  values: [],
});

// DDL
export const createSchemaSql = (dbCtx: PGDBContext): SQLWithValues => ({
  sql: `create schema if not exists ${dbCtx.schemaName}`,
  values: [],
});

// DDL
export const createTableSql = (dbCtx: PGDBContext, type: Type): SQLWithValues => {
  const tableFields = map(colSpec, scalarFields(type));
  const sql = `
    create table ${dbCtx.schemaName}.${type.tableName} (
      ${join(',\n', tableFields)}
    )
  `;
  return {
    sql,
    values: [],
  };
};

// DDL
export const createJoinTableSql = (
  dbCtx: PGDBContext,
  relation: Relation
): SQLWithValues => {
  const t1 = relation.types[0];
  const t2 = relation.types[1];
  const t1ColName = underscore(`${lowerFirstLetter(t1.name)}Id`);
  const t2ColName = underscore(`${lowerFirstLetter(t2.name)}Id`);
  const sql = `
    create table ${dbCtx.schemaName}.${relation.tableName} (
      id varchar(36) primary key,
      ${t1ColName} varchar(36) not null references ${dbCtx.schemaName}.${t1.tableName}(id) on delete cascade,
      ${t2ColName} varchar(36) not null references ${dbCtx.schemaName}.${t2.tableName}(id) on delete cascade,
      unique (${t1ColName}, ${t2ColName})
    )
  `;
  return {
    sql,
    values: [],
  };
};

// DDL
export const renameTableSql = (
  dbCtx: PGDBContext,
  origName: string,
  newName: string,
): SQLWithValues => {
  // console.log('renameTableSql:', `alter table ${dbCtx.schemaName}.${origName} rename to ${newName}`);
  return {
    sql: `alter table ${dbCtx.schemaName}.${origName} rename to ${newName}`,
    values: [],
  }
};

// DDL
export const renameRelationForeignKeySql = (
  dbCtx: PGDBContext,
  oldRelation: Relation,
  newRelation: Relation,
  type: Type,
): SQLWithValues => {
  const { tableName } = newRelation;
  const colName = underscore(`${lowerFirstLetter(type.name)}Id`);
  const oldName = `${oldRelation.tableName}_${colName}_fkey`;
  const newName = `${newRelation.tableName}_${colName}_fkey`;
  const sql = `alter table ${dbCtx.schemaName}.${tableName} rename constraint "${oldName}" to "${newName}"`;
  // console.log('sql:', sql);
  return {
    sql,
    values: [],
  };
};

// DDL
export const addColumnSql = (dbCtx: PGDBContext, field: Field): SQLWithValues => {
  const { tableName } = field.parentType;
  return {
    sql: `alter table ${dbCtx.schemaName}.${tableName} add column ${colSpec(field)}`,
    values: [],
  };
};

// DDL
export const addIndexSql = curry((dbCtx: PGDBContext, index: Index): SQLWithValues => {
  const { tableName } = index.parentType;
  const uniqueStr = index.unique ? 'unique' : '';
  const sql = `create ${uniqueStr} index ${index.name} on ${dbCtx.schemaName}.${tableName} using ${index.type} (${join(', ', index.columns)})`;
  return {
    sql,
    values: [],
  };
});

// DDL
export const alterColumnTypeSql = (
  dbCtx: PGDBContext,
  field: Field,
): SQLWithValues => {
  const { tableName } = field.parentType;
  const { columnName } = field;
  const size = field.size > 0 ? `(${field.size})` : '';
  return {
    sql: `alter table ${dbCtx.schemaName}.${tableName} alter ${columnName} type ${field.sqlType} ${size}`,
    values: [],
  };
};

// DDL
export const alterColumnDefaultSql = (
  dbCtx: PGDBContext,
  field: Field,
): SQLWithValues => {
  const { tableName } = field.parentType;
  const { columnName } = field;
  const defaultClause = isNil(field.default) ? 'drop default' : `set default ${field.type === 'String' ? `'${field.default}'` : field.default}`;
  return {
    sql: `alter table ${dbCtx.schemaName}.${tableName} alter column ${columnName} ${defaultClause}`,
    values: [],
  };
};

// DDL
export const alterColumnNullabilitySql = (
  dbCtx: PGDBContext,
  field: Field,
): SQLWithValues => {
  const { tableName } = field.parentType;
  const { columnName } = field;
  const nullability = field.nullable ? 'drop' : 'set';
  return {
    sql: `alter table ${dbCtx.schemaName}.${tableName} alter ${columnName} ${nullability} not null`,
    values: [],
  };
};

// DDL
export const dropColumnSql = (dbCtx: PGDBContext, field: Field): SQLWithValues => ({
  sql: `alter table ${dbCtx.schemaName}.${field.parentType.tableName} drop column ${field.columnName}`,
  values: [],
});

// DDL
export const dropTableSql = (dbCtx: PGDBContext, type: Type): SQLWithValues => ({
  sql: `drop table ${dbCtx.schemaName}.${type.tableName} cascade`,
  values: [],
});

// DDL
export const dropIndexSql = (dbCtx: PGDBContext, index: Index): SQLWithValues => ({
  sql: `drop index ${dbCtx.schemaName}.${index.name} cascade`,
  values: [],
});

// DDL
export const dropConstraintSql = (
  dbCtx: PGDBContext,
  type: Type,
  constraintName: string,
): SQLWithValues => ({
  sql: `alter table ${dbCtx.schemaName}.${type.tableName} drop constraint ${constraintName}`,
  values: [],
});
