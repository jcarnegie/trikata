// @flow
import {
  curry,
  find,
  flatten,
  has,
  isEmpty,
  head,
  join,
  keys,
  last,
  map,
  match,
  pathOr,
  propEq,
  reduceWhile,
  toLower,
  toString,
} from 'ramda';

import { Type } from '../schema/type';
import { Field } from '../schema/field';

/*

query {
  posts(where: {
    OR: [{
      AND: [{
        title_in: ["My biggest Adventure", "My latest Hobbies"]
      }, {
        published: true
      }]
    }, {
      id: "cixnen24p33lo0143bexvr52n"
    }]
  }) {
    id
    title
    published
  }
}

--> (title in ('My biggest Adventure', 'My latest Hobbies') AND published = true) OR (id = 'cixnen24p33lo0143bexvr52n')

*/

// type WhereFilterOperator =
//   'eq' |
//   'not' |
//   'lt' |
//   'lte' |
//   'gt' |
//   'gte' |
//   'in' |
//   'not_in' |
//   'contains' |
//   'not_contains' |
//   'starts_with' |
//   'not_starts_with' |
//   'ends_with' |
//   'not_ends_with';

type WhereFilter = {
  column: string,
  operator: string, // WhereFilterOperator
  value: any,
};

type WhereGroupOperator = 'AND' | 'OR' | 'NOT';

type WhereGroup = {
  operator: WhereGroupOperator,
  filters: Array<WhereFilter | WhereGroup>,
}

export type WhereAST = Array<WhereFilter | WhereGroup>;

// const where: WhereAST = [
//   {
//     operator: 'OR',
//     filters: [{
//       operator: 'AND',
//       filters: [
//         { column: 'title', operator: 'in', value: ['My biggest Adventure', 'My latest Hobbies'] },
//         { column: 'published', operator: 'eq', value: true },
//       ],
//     }, {
//       column: 'id',
//       operator: 'eq',
//       value: 'cixnen24p33lo0143bexvr52n',
//     }],
//   },
// ];

// NOTE: the order of the OPERATORS array matters - we have to match the
//       longest string, otherwise we get part of the operator as a part
//       of the column name
const OPERATORS = [
  'not_ends_with',
  'not_starts_with',
  'ends_with',
  'starts_with',
  'not_contains',
  'not_in',
  'contains',
  'lte',
  'gte',
  'not',
  'eq',
  'lt',
  'gt',
  'in',
];

/**
 * Take a where filter such as { id_lt: 3 } and turn it into a WhereAST that
 * looks like this:
 * 
 * 
 * 
 */
export const fromWhereInput = curry((type: Type, where: any): WhereAST => {
  const stringifiedKeys: string[] = map(k => k.toString(), keys(where));

  const matchFn = curry((tableName: string, k: string, acc: any, op: string): [string, string] | [] => {
    const re = new RegExp(`([^\\s]+)_${op}`);
    const result = match(re, k);
    const rawFieldName = result[1];
    // translate the GQL field name to a DB column name
    const field = find(propEq('name', rawFieldName), type.fields);
    const columnName = field ? field.columnName : rawFieldName;
    const column = (tableName) ? `${tableName}.${columnName}` : columnName;
    return isEmpty(result) ? [] : [column, op];
  });

  const mapFn = (k: string): WhereFilter | WhereGroup => {
    const value = pathOr(where[k], [k, 'value'], where);
    const tableName = pathOr('', [k, 'tableName'], where);
    const data = reduceWhile(isEmpty, matchFn(tableName, k), [], OPERATORS);
    let [column, operator] = data;

    // translate the GQL field name to a DB column name
    const field = find(propEq('name', (column || k)), type.fields);
    column = field ? field.columnName : (column || k);

    operator = operator || 'eq';
    
    switch (k) {
      case 'AND':
      case 'OR':
      case 'NOT':
        return { operator: k, filters: flatten(map(fromWhereInput(type), value)) };
      default:
        return {
          column,
          operator,
          value,
        };
    }
  };

  return map(mapFn, stringifiedKeys);
});

export const escape = curry((field: Field, value: any): any =>
  (field.type === 'String' ? `'${value}'` : value));

export const filterToSql = (type: Type, filter: WhereFilter): string => {
  const field = find(propEq('name', filter.column), type.fields);
  if (!field) throw new Error(`Column ${filter.column} not found in typedef ${type.name}`);
  switch (filter.operator) {
    case 'eq': return `${filter.column} = ${escape(field, filter.value)}`;
    case 'not': return `${filter.column} <> ${escape(field, filter.value)}`;
    case 'lt': return `${filter.column} < ${escape(field, filter.value)}`;
    case 'lte': return `${filter.column} <= ${escape(field, filter.value)}`;
    case 'gt': return `${filter.column} > ${escape(field, filter.value)}`;
    case 'gte': return `${filter.column} >= ${escape(field, filter.value)}`;
    case 'in': return `${filter.column} in (${join(', ', map(escape(field), filter.value))})`;
    case 'not_in': return `${filter.column} not in (${join(', ', map(escape(field), filter.value))})`;
    case 'contains': return `${filter.column} like '%${filter.value}%'`;
    case 'not_contains': return `${filter.column} not like '%${filter.value}%'`;
    case 'starts_with': return `${filter.column} like '${filter.value}%'`;
    case 'not_starts_with': return `${filter.column} not like '${filter.value}%'`;
    case 'ends_with': return `${filter.column} like '%${filter.value}'`;
    case 'not_ends_with': return `${filter.column} not like '%${filter.value}'`;
    default:
      throw new Error(`Unknown where clause operator '${filter.operator}'`);
  }
};

export const filterToSqlWithBindParams = (filter: WhereFilter): [string, any[]] => {
  switch (filter.operator) {
    case 'eq': return [`${filter.column} = ?`, [filter.value]];
    case 'not': return [`${filter.column} <> ?`, [filter.value]];
    case 'lt': return [`${filter.column} < ?`, [filter.value]];
    case 'lte': return [`${filter.column} <= ?`, [filter.value]];
    case 'gt': return [`${filter.column} > ?`, [filter.value]];
    case 'gte': return [`${filter.column} >= ?`, [filter.value]];
    case 'in': return [`${filter.column} in (${join(', ', map(() => '?', filter.value))})`, [filter.value]];
    case 'not_in': return [`${filter.column} not in (${join(', ', map(() => '?', filter.value))})`, [filter.value]];
    case 'contains': return [`${filter.column} like ?`, [`%${filter.value}%`]];
    case 'not_contains': return [`${filter.column} not like ?`, [`%${filter.value}%`]];
    case 'starts_with': return [`${filter.column} like ?`, [`${filter.value}%`]];
    case 'not_starts_with': return [`${filter.column} not like ?`, [`${filter.value}%`]];
    case 'ends_with': return [`${filter.column} like ?`, [`%${filter.value}`]];
    case 'not_ends_with': return [`${filter.column} not like ?`, [`%${filter.value}`]];
    default:
      throw new Error(`Unknown where clause operator '${filter.operator}'`);
  }
};

export const isFilter = (data: WhereFilter | WhereGroup) => has('column', data);

export const formatSql = (type: Type, where: WhereAST, joinOp: string = ' and '): string => {
  const filters = map((data: any) => {
    if (isFilter(data)) return `(${filterToSql(type, data)})`;
    return `(${formatSql(type, data.filters, ` ${toLower(data.operator)} `)})`;
  }, where);
  return join(joinOp, filters);
};

export const formatSqlWithBindParams = (where: WhereAST, joinOp: string = ' and '): [string, any[]] => {
  const sqlAndValues = map((data: any) => {
    if (isFilter(data)) {
      const [sql, values] = filterToSqlWithBindParams(data);
      return [`(${sql})`, values];
    }
    const [sql, values] = formatSqlWithBindParams(data.filters, ` ${toLower(data.operator)} `);
    return [`(${sql})`, values];
  }, where);
  const sql = map(sv => sv[0], sqlAndValues);
  const values = map(sv => sv[sv.length - 1], sqlAndValues);
  return [join(joinOp, sql), flatten(values)];
};
